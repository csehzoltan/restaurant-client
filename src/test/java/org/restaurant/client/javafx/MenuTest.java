package org.restaurant.client.javafx;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import junit.framework.Assert;

public class MenuTest {

	@Test
	public void orderingJsonConvertTest() {
		Map<Integer, Integer> orderPrices = new LinkedHashMap<>();
		orderPrices.put(1, 2);
		orderPrices.put(2, 1);
		orderPrices.put(3, 10);
		JsonObject jsMsg = createJsonMsg(orderPrices, "ordering");
		System.out.println(jsMsg.toString());
		
		Gson gson = new Gson();
		String jsonFromMap = gson.toJson(orderPrices);
		System.out.println(jsonFromMap);
		
		Type type = new TypeToken<Map<Integer, Integer>>(){}.getType();
		Map<Integer, Integer> map = gson.fromJson(jsonFromMap, type);
		for(Map.Entry<Integer, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " val: " + entry.getValue());
		}
	}
	
	private JsonObject createJsonMsg(Object data, String actionType) {
		
		Gson gson = new GsonBuilder().create();
        String json = gson.toJson(data);
        JsonParser p = new JsonParser();
        JsonObject obj = p.parse(json).getAsJsonObject();
        
        JsonObject jsonAction = new JsonObject();
        jsonAction.addProperty("type", actionType);
       
        JsonObject sendMsg = new JsonObject();
        sendMsg.add("action", jsonAction);
        sendMsg.add("data", obj);
		return sendMsg;
	}

}
