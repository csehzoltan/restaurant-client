package org.restaurant.client.javafx;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.restaurant.client.gui.logics.Loggin;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit.ApplicationTest;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;

public class HomeTest extends ApplicationTest {
	//links https://github.com/TestFX/TestFX/issues/57
	
	private Parent home;
	@Override
	public void start(Stage stage) {
//		try {
//			home = FXMLLoader.load(Loggin.class.getResource("/org/restaurant/client/gui/temp/common/Home.fxml"));
//			
//			Scene scene = new Scene(home);
//			stage.setScene(scene);
//			stage.show();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	@Test
	public void home_menu_shuld_contain_homeMenuButton() {
		ToggleButton button = from(home).lookup("#homeMenuButton").query();
		assertEquals("Home", button.getText());
	}
	
	@Test
	public void should_click_on_button() {
		FxRobot robot = new FxRobot();
		robot.clickOn("#homeMenuButton");
		//verifyThat(".button", hasText("clicked!"));
		//verifyThat("#homeMenuButton", hasChildren(0, ".file"));
		//rightClickOn("#homeMenuButton");
	}
	
}
