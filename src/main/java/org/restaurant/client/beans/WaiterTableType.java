package org.restaurant.client.beans;

public enum WaiterTableType {
	GREEN(1), YELLOW(2), RED(3);
	
	private int id;
	
	WaiterTableType(int id) {
		this.id = id;
	}
	
	public static WaiterTableType getById(int id) {
		for (WaiterTableType e : values()) {
			if (e.id == id) {
				return e;
			}
		}
		return null;
	}
}
