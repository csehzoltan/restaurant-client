package org.restaurant.client.socket.client;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.restaurant.client.socket.responseactions.ResponseHandler;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.UserType;
import org.restaurant.common.thread.AutoresetEvent;

public class RestaurantMainClient extends AbstractRestaurantClient {
	
	private boolean isConnectedInTime;
	
	public RestaurantMainClient(int port, String host, AutoresetEvent are) {
		super(port, host, are);
	}
	
	@Override
	protected void readerEventsHandle() {
		while(reader.hasNextLine()){
			new ResponseHandler(reader.nextLine()).process();
		}
	}
	
	public void populateUser(UserType userType) {
		getWriter().println(JsonHelper.jsonActonRequest("initUser", userType.name()));
		getWriter().flush();
	}
	
	public void populateProducts(Integer userTypeId) {
		getWriter().println(JsonHelper.jsonActonRequest("getProducts", userTypeId.toString()).toString());
		getWriter().flush();
	}
		
	public void populateUsers() {
//		client.getWriter().println(JsonHelper.jsonActonRequest("getIngredients", null).toString());
//		client.getWriter().flush();
		getWriter().println(JsonHelper.jsonActonRequest("getUsers", null).toString());
		getWriter().flush();
	}

	public void populateTable(Integer tableId) {
		getWriter().println(JsonHelper.jsonActonRequest("getTable", String.valueOf(tableId)).toString());
		getWriter().flush();
	}
	
	public void populateGuestbooks() {
		getWriter().println(JsonHelper.jsonActonRequest("getGuestbooks", null).toString());
		getWriter().flush();
	}
	
	public void populateGuestBookAdministrator(Date date) {
		getWriter().println(JsonHelper.jsonActonRequest("getGuestBookAdministrator", new SimpleDateFormat("yyyymm").format(date)).toString());
		getWriter().flush();
	}
	
	public void populateProductCategoryStatistics(Date date) {
		getWriter().println(JsonHelper.jsonActonRequest("getProductCategoryStatistics", new SimpleDateFormat("yyyymm").format(date)).toString());
		getWriter().flush();
	}
	
	public void populateProductIncomes(Integer year) {
		getWriter().println(JsonHelper.jsonActonRequest("getProductIncomes", String.valueOf(year)).toString());
		getWriter().flush();
	}
	
	public void isConnectedInTime(boolean isConnectedInTime) {
		this.isConnectedInTime = isConnectedInTime;
	}
	
	public boolean isConnectedInTime() {
		return isConnectedInTime;
	}
}
