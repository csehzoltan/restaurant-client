package org.restaurant.client.socket.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import org.restaurant.common.thread.AutoresetEvent;

public abstract class AbstractRestaurantClient extends Thread {

	private Socket socket;
	private PrintWriter writer;
	protected Scanner reader;
	private final int port;
	private final String host;
	
	AutoresetEvent are;
	
	public AbstractRestaurantClient(int port, String host, AutoresetEvent are) {
		this.port = port;
		this.host = host;
		this.are = are;
	}
	
	public PrintWriter getWriter() {
			return writer;
	}
	
	@Override
	public void run() {
		try (Socket s = new Socket(host, port);
				PrintWriter pw = new PrintWriter(s.getOutputStream());
				Scanner sc = new Scanner(s.getInputStream());) {

			this.socket = s;
			this.writer = pw;
			synchronized (this.writer) {
				writer.notifyAll();
			}
			this.reader = sc;
			
			are.set();
			readerEventsHandle();
			
		} catch (Exception e) {
			e.printStackTrace();
			closeClient();
		}
	}

	protected abstract void readerEventsHandle();
	
	/**
	 * Close the socket connection.
	 */
	public void closeClient(){
		try {
			writer.close();
			reader.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
