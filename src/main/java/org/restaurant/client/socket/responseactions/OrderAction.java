package org.restaurant.client.socket.responseactions;

import org.restaurant.client.main.ClientMain;

import com.google.gson.JsonElement;

import javafx.scene.control.Alert.AlertType;

public class OrderAction extends AbstractAction {

	public OrderAction(JsonElement rawData) {
		super(rawData);
	}

	protected void acceptOrder() {
		String errorMsg = 
				rawData.getAsJsonObject().get(ERROR) != null ? rawData.getAsJsonObject().get(ERROR).toString() : null;
		if (errorMsg != null) {
			ClientMain.getHomeController().showAlert(AlertType.WARNING, errorMsg);
		} else {
			String info = rawData.getAsJsonObject().get(INFO) != null ? rawData.getAsJsonObject().get(INFO).toString() : "";
			ClientMain.getHomeController().showAlert(AlertType.INFORMATION, info);
			if(ClientMain.getHomeController().getMenuController() != null) {
				ClientMain.getHomeController().getMenuController().deleteOrders();
			}
			
		}
	}
}
