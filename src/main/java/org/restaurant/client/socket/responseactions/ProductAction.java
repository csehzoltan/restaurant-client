package org.restaurant.client.socket.responseactions;

import java.util.ArrayList;
import java.util.List;

import org.restaurant.client.gui.controllers.IngredientSurfaceController;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.CoordXY;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.Product;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import javafx.scene.control.Alert.AlertType;

public class ProductAction extends AbstractAction {

	public ProductAction(JsonElement data) {
		super(data);
	}
	
	protected void getProducts() {
		Gson gson = new GsonBuilder().create();
		for (JsonElement prod : rawData.getAsJsonObject().get("products").getAsJsonArray()) {
		    Product product = gson.fromJson(prod.getAsJsonObject(), Product.class);
		    ClientMain.getProducts().add(product);
		}
	}
	
	protected void getIngredients() {
		Gson gson = new GsonBuilder().create();
		List<Ingredient> ingredients = new ArrayList<>();
		for(JsonElement elem : rawData.getAsJsonObject().get("ingredients").getAsJsonArray()) {
			Ingredient ingredient = gson.fromJson(elem.getAsJsonObject(), Ingredient.class);
			ingredients.add(ingredient);
		}
		
		try {
			IngredientSurfaceController ingredientSurfaceController = ClientMain.getAdminController().getProductTablePaneController().getIngredientSurfaceController();
				ingredientSurfaceController.setIsIngredientInitalized(true);
				ingredientSurfaceController.loadIngredients(ingredients);
		} catch(NullPointerException e) {
            System.out.print("Ingredient window is not displayed!");
        }
	}
	
	protected void uploadIngredientIds() {
		Gson gson = new GsonBuilder().create();
		List<Ingredient> newIngredients = new ArrayList<>();
		for(JsonElement elem : rawData.getAsJsonObject().get("ingredients").getAsJsonArray()) {
			Ingredient ingredient = gson.fromJson(elem.getAsJsonObject(), Ingredient.class);
			newIngredients.add(ingredient);
		}
		
		try {
			IngredientSurfaceController ingredientSurfaceController = ClientMain.getAdminController().getProductTablePaneController().getIngredientSurfaceController();
			ingredientSurfaceController.uploadIngredientIds(newIngredients);
		} catch(NullPointerException e) {
            System.out.print("Ingredient window is not displayed!");
        }
	}
	
	/**
	 * Betölti limithatár alá eső összetevőket táblát tartalmazó felületet.
	 */
	protected void setIngredientAlerts() {
		Gson gson = new GsonBuilder().create();
		List<Ingredient> ingredients = new ArrayList<>();
		for (JsonElement element : rawData.getAsJsonObject().get(DATA).getAsJsonArray()) {
			Ingredient ingredient = gson.fromJson(element.getAsJsonObject(), Ingredient.class);
			ingredients.add(ingredient);
		}
		if(ClientMain.getAdminController().getIngredientAlertsController() != null) {
			ClientMain.getAdminController().getIngredientAlertsController().loadIngredientAlerts(ingredients);
		}
	}
	
	/**
	 * Betölteti a statisztika vezérlővel a termékek kategóriánkénti statisztikáját.
	 */
	protected void getProductCategoryStatistics() {
		Gson gson = new GsonBuilder().create();
		List<CoordXY> coordsXY = new ArrayList<>();
		for (JsonElement element : rawData.getAsJsonObject().get(DATA).getAsJsonArray()) {
			CoordXY income = gson.fromJson(element.getAsJsonObject(), CoordXY.class);
			coordsXY.add(income);
		}
		if(ClientMain.getAdminController().getStatisticsController() != null) {
			ClientMain.getAdminController().getStatisticsController().loadPieCharts(coordsXY);
		}
	}
	
	/**
	 * Amennyiben meg van nyitva a statisztika felület betölti a megadott évhez tartozó bevételeket egyetlen oszlopdiagramban az összes hónapra.
	 */
	protected void getProductIncomes() {
		Gson gson = new GsonBuilder().create();
		List<CoordXY> coordsXY = new ArrayList<>();
		for (JsonElement element : rawData.getAsJsonObject().get(DATA).getAsJsonArray()) {
			CoordXY income = gson.fromJson(element.getAsJsonObject(), CoordXY.class);
			coordsXY.add(income);
		}
		if(ClientMain.getAdminController().getStatisticsController() != null) {
			ClientMain.getAdminController().getStatisticsController().loadIncomesBarChart(coordsXY);
		}
	}
	
	protected void saveProductByAdmin() {
		String errorMsg = rawData.getAsJsonObject().get(ERROR) != null ? rawData.getAsJsonObject().get(ERROR).toString() : null;
		if (errorMsg != null) {
			ClientMain.getAdminController().showAlert(AlertType.WARNING, errorMsg);
		} else {
			Gson gson = new Gson();
			Product product = gson.fromJson(getAsJsonObject(DATA), Product.class);
			// TODO map is better solution
			if (ClientMain.getAdminController().getProductTablePaneController() != null) {
				String actionType = getInfos().get("type");
				boolean isInsert = "insert".equals(actionType);
				ClientMain.getAdminController().getProductTablePaneController().updateTable(product, isInsert);
			}
			if (product != null && !ClientMain.getProducts().contains(product)) {
				ClientMain.getProducts().add(product);
			} else if (product != null && ClientMain.getProducts().contains(product)) {
				ClientMain.getProducts().remove(product);
				ClientMain.getProducts().add(product);
			}
			
			ClientMain.getAdminController().showAlert(AlertType.WARNING, "Product successfully saved!");
		}
	}
}
