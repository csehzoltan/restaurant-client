 package org.restaurant.client.socket.responseactions;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class AbstractAction {
	protected static final String ACTION = "action";
	protected static final String ERROR = "error";
	protected static final String DATA = "data";
	protected static final String INFO = "info";
	
	protected JsonElement rawData;
	
	public AbstractAction(JsonElement rawData) {
		super();
		this.rawData = rawData;
	}
	
	/**
	 * Json üzenetből kiszedi az info tagot és visszatér a benne szereplő kulcs érték párosokkal.
	 * @return Visszatér az info blokk kulcs érték párosaival.
	 */
	protected Map<String, String> getInfos() {
		Gson gson = new Gson();
		Type typeOfHashMap = new TypeToken<Map<String, String>>() { }.getType();
		Map<String, String> infos = null;
		if(rawData.getAsJsonObject().get("info") != null) {
			infos = gson.fromJson(rawData.getAsJsonObject().get("info"), typeOfHashMap);
		}
		return infos;
	}
	
	protected JsonObject getAsJsonObject(String name) {
		return rawData.getAsJsonObject().getAsJsonObject(name);
	}
	
}