package org.restaurant.client.socket.responseactions;

import java.util.ArrayList;
import java.util.List;

import org.restaurant.client.gui.helper.NotificationHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.common.model.beans.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;

public class UserAction extends AbstractAction {
	
	public UserAction(JsonElement data) {
		super(data);
	}
	
	protected void setParentUsers() {
		Gson gson = new GsonBuilder().create();
		for (JsonElement element : rawData.getAsJsonObject().get("users").getAsJsonArray()) {
			User user = gson.fromJson(element.getAsJsonObject(), User.class);
			ClientMain.getUsers().add(user);
		}
	}
	
	protected void setGuestbooks() {
		Gson gson = new GsonBuilder().create();
		List<Guestbook> guestbooks = new ArrayList<>();
		for (JsonElement element : rawData.getAsJsonObject().get(DATA).getAsJsonArray()) {
			Guestbook guestbook = gson.fromJson(element.getAsJsonObject(), Guestbook.class);
			guestbooks.add(guestbook);
		}
		if(ClientMain.getAdminController().getGuestbookPaneController() != null) {
			ClientMain.getAdminController().getGuestbookPaneController().loadGuestbookItems(guestbooks);
		}
	}
	
	protected void registration() {
		JsonObject userObj = rawData.getAsJsonObject().getAsJsonObject("data");
		Gson gson = new GsonBuilder().create();
		User user = gson.fromJson(userObj, User.class);
		
		String errorMsg = rawData.getAsJsonObject().get(ERROR) != null ? rawData.getAsJsonObject().get(ERROR).toString() : null;
		if (errorMsg != null) {
			ClientMain.getHomeController().showAlert(AlertType.WARNING, errorMsg);
		} else if (ClientMain.getHomeController().getRegistrationController() != null) {
			boolean hasUser = (user == null);
			ClientMain.getHomeController().getRegistrationController().loginUser(user, hasUser);
		}
	}

	protected void registrationByAdmin() {
		String errorMsg = rawData.getAsJsonObject().get(ERROR) != null ? rawData.getAsJsonObject().get(ERROR).toString() : null;
		if (errorMsg != null) {
			ClientMain.getAdminController().showAlert(AlertType.WARNING, errorMsg);
		} else {
			Gson gson = new Gson();
			User user = gson.fromJson(getAsJsonObject(DATA), User.class);
			// TODO map is better solution
			if (ClientMain.getAdminController().getUserPaneController() != null) {

				String actionType = getInfos().get("type");

				boolean isInsert = "insert".equals(actionType);
				ClientMain.getAdminController().getUserPaneController().updateTable(user, isInsert);
			}
			if (user != null && !ClientMain.getUsers().contains(user)) {
				ClientMain.getUsers().add(user);
			} else if (user != null && ClientMain.getUsers().contains(user)) {
				ClientMain.getUsers().remove(user);
				ClientMain.getUsers().add(user);
			}
			ClientMain.getAdminController().showAlert(AlertType.WARNING, "User successfully saved!");
		}
	}
	
	protected void saveGuestbook() {
		String errorMsg = rawData.getAsJsonObject().get(ERROR) != null ? rawData.getAsJsonObject().get(ERROR).toString() : null;
		if (errorMsg != null) {
			new NotificationHelper().showNotification(errorMsg, Pos.BOTTOM_RIGHT, "Error");
		} else {
			new NotificationHelper().showNotification("Guestbook successfully saved!", Pos.BOTTOM_RIGHT, "Message");
			if(ClientMain.getHomeController().getGuestbookController() != null) {
				ClientMain.getHomeController().getGuestbookController().disableAllFields(true);
			}
		}
	}
	
}
