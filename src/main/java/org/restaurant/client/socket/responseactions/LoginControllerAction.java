package org.restaurant.client.socket.responseactions;

import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.User;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class LoginControllerAction extends AbstractAction {

	public LoginControllerAction(JsonElement data) {
		super(data);
	}
	
	/**
	 * Bejelentkezik a felhasználó, amennyiben sikeres volt a bejelentkezés. Vendég esetén a regisztráció és a login gomb megváltozik egy felhasználó
	 * ikonra, melynek segítségével ki tudunk jelentkezni. Adminisztrátor esetén elérhetővé teszi a funkciókat.
	 */
	public void logIn() {
		String action = rawData.getAsJsonObject().get(ACTION).getAsString();
		Gson gson = new Gson();
		if ("logIn".equals(action)) {
			boolean hasError = rawData.getAsJsonObject().has(ERROR);
			JsonObject userObj = rawData.getAsJsonObject().getAsJsonObject(DATA);
			User user = gson.fromJson(userObj, User.class);
			if(ClientMain.getAdminController() != null) {
				ClientMain.getAdminController().loginUser(user, hasError);
			} else if (ClientMain.getHomeController() != null && ClientMain.getHomeController().getLoginController() != null) {
				ClientMain.getHomeController().getLoginController().loginUser(user, hasError);
			}
		}
	}

}
