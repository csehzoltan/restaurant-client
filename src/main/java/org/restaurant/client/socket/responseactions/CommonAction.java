package org.restaurant.client.socket.responseactions;

import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.Table;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CommonAction extends AbstractAction {

	public CommonAction(JsonElement rawData) {
		super(rawData);
	}

	protected void getTable() {
		JsonObject tableObj = rawData.getAsJsonObject().getAsJsonObject(DATA);
		Gson gson = new Gson();
		Table table = gson.fromJson(tableObj, Table.class);
		ClientMain.setTable(table);
	}

}
