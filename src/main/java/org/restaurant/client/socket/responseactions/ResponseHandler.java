package org.restaurant.client.socket.responseactions;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class ResponseHandler {
	
	private String action;
	private JsonElement data;
	
	/**
	 * Kiszedi a művelet nevét és a JSON adatot eltárolja osztályváltozóba.
	 * @param requestMsg szervertől jövő JSON üzenet, amit fel kell dolgozni.
	 */
	public ResponseHandler(String requestMsg) {	
		JsonElement jelement = new JsonParser().parse(requestMsg.trim());
		this.action = jelement.getAsJsonObject().get("action").getAsString();
		this.data = jelement;
	}

	/**
	 * A bejövő kérés művelet nevének megfelelően kiválasztja a hozzá tartozó
	 * műveletet és végrehajtja.
	 */
	public void process() {
		try {
			if ("getProducts".equals(action)) {
				new ProductAction(data).getProducts();
			} else if ("getIngredients".equals(action)) {
				new ProductAction(data).getIngredients();
			} else if("uploadIngredients".equals(action)) {
				new ProductAction(data).uploadIngredientIds();
			} else if ("getProductCategoryStatistics".equals(action)) {
				new ProductAction(data).getProductCategoryStatistics();
			} else if("getProductIncomes".equals(action)){
				new ProductAction(data).getProductIncomes();
			} else if("saveProductByAdmin".equals(action)) {
				new ProductAction(data).saveProductByAdmin();
			} else if("setIngredientAlerts".equals(action)) {
				new ProductAction(data).setIngredientAlerts();
			} else if ("logIn".equals(action)) {
				new LoginControllerAction(data).logIn();
			} else if ("getUsers".equals(action)) {
				new UserAction(data).setParentUsers();
			} else if ("getGuestbooks".equals(action)) {
				new UserAction(data).setGuestbooks();
			} else if ("saveGuestbook".equals(action)) {
				new UserAction(data).saveGuestbook();
			} else if("getGuestBookAdministrator".equals(action)) {
				new UserAction(data).setGuestbooks();
			} else if ("registrationByAdmin".equals(action)) {
				new UserAction(data).registrationByAdmin();
			} else if("registration".equals(action)) {
				new UserAction(data).registration();
			} else if ("getTable".equals(action)) {
				new CommonAction(data).getTable();
			} else if ("acceptOrder".equals(action)) {
				new OrderAction(data).acceptOrder();
			} else if ("acceptNewOrder".equals(action)) {
				new WaiterAction(data).acceptNewOrder();
			} else if ("getPayForWaiter".equals(action)) {
				new WaiterAction(data).getPayForWaiter();
			}
		} catch (Exception e) {
			System.out.println("Action error!");
		}
	}
}
