package org.restaurant.client.socket.responseactions;

import java.util.List;

import org.restaurant.client.beans.WaiterTableType;
import org.restaurant.client.gui.controllers.WaiterProfileController;
import org.restaurant.client.gui.controllers.WaiterTableController;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.ProductRequest;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class WaiterAction extends AbstractAction {

	public WaiterAction(JsonElement rawData) {
		super(rawData);
	}
	
	/**
	 * Az asztalhoz hozzáadja az új rendeléseket, átszínezi az asztalt sárgára amennyiben meglévő rendeléshez kerülnek újak,
	 * ezzel jelezzük a felszolgálónak, hogy új rendelés érkezett. Amennyiben attól az asztaltól nem volt még megrendelés hozzáadja az
	 * asztalokat tartalmazó listába.
	 */
	protected void acceptNewOrder() {		
		JsonObject productRequestJsonObj = rawData.getAsJsonObject().getAsJsonObject("productRequest");
		Gson gson = new Gson();
		ProductRequest productRequest = gson.fromJson(productRequestJsonObj, ProductRequest.class);
		
		for(WaiterProfileController waiterProfileController : ClientMain.getHomeController().getWaiterSurfaceController().getWaiterProfiles()) {
			if(waiterProfileController.getWaiter().getId().equals(productRequest.getWaiter().getId())) {
				List<WaiterTableController> tables = waiterProfileController.getTables();
				boolean isCreateNewTable = true;
				if(tables != null && !tables.isEmpty()) {
					for(WaiterTableController waiterTableController : waiterProfileController.getTables()) {
						if(productRequest.getTable().getId().equals(waiterTableController.getTable().getId())) {
							waiterTableController.setRequests(productRequest);
							waiterTableController.reInitalize(WaiterTableType.YELLOW);
							isCreateNewTable = false;
						}
					}
				}
				if(isCreateNewTable) {
					waiterProfileController.addTable(productRequest.getTable(), productRequest, WaiterTableType.YELLOW);
				}
				
			}
		}
	}
	
	/**
	 * Kiszínezi az asztalhoz tartozó boxot pirosra jezelve a felszolgálónak, hogy fizetni kíván a vendég.
	 * Egy felhasználót ábrázoló ikont jelenít meg a boxban, ha a fizetést egyvalaki teljesíti az asztalnál, különben több felhasználót ábrázoló ikont jelenít meg.
	 */
	protected void getPayForWaiter() {
		JsonObject productRequestJsonObj = rawData.getAsJsonObject().getAsJsonObject(DATA);
		Gson gson = new Gson();
		ProductRequest productRequest = gson.fromJson(productRequestJsonObj, ProductRequest.class);
		
		for(WaiterProfileController waiterProfileController : ClientMain.getHomeController().getWaiterSurfaceController().getWaiterProfiles()) {
			if(waiterProfileController.getWaiter().getId().equals(productRequest.getWaiter().getId())) {
				changeWaiterTableToRedAndShowPayTypeIcon(productRequest, waiterProfileController);
			}
		}
	}

	private void changeWaiterTableToRedAndShowPayTypeIcon(ProductRequest productRequest,
			WaiterProfileController waiterProfileController) {
		for(WaiterTableController waiterTableController : waiterProfileController.getTables()) {
			if(productRequest.getTable().getId().equals(waiterTableController.getTable().getId())) {

				if(productRequest.getPayType().equals(PayType.ONEPAYS)) {
					waiterTableController.showOnePayIcon();
				} else {
					waiterTableController.showSeparatePayIcon();
				}
				
				waiterTableController.reInitalize(WaiterTableType.RED);
			}
		}
	}
}
