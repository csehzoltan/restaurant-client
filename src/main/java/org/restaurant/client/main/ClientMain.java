package org.restaurant.client.main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.restaurant.client.gui.controllers.AdministratorSurfaceController;
import org.restaurant.client.gui.controllers.HomeController;
import org.restaurant.client.properties.ClientConfig;
import org.restaurant.client.properties.PropertyException;
import org.restaurant.client.socket.client.RestaurantMainClient;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.Table;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;
import org.restaurant.common.thread.AutoresetEvent;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Elindítja a kliens oldali programot, elvégzi a felhasználó típus felismerését és aszerint kerül konfigurálásra a program.
 * Elindítja a kommunikációt a szerver és a kliens között.
 * @author Zoltán
 *
 */
public class ClientMain extends Application {
	private static RestaurantMainClient client;
	private static User user;
	private static SortedSet<Product> products = Collections.synchronizedSortedSet(new TreeSet<>((o1, o2) -> (o1.getCategory().getNo() + o1.getName()).compareTo(o2.getCategory().getNo() + o2.getName())));
	private static List<User> users = Collections.synchronizedList(new ArrayList<User>());
	private static HomeController homeController;
	private static AdministratorSurfaceController adminController;
	private static ClientConfig clientProperty;
	private static Table table;
	static AutoresetEvent areWaitForAnswer;

	static {
		try {
			clientProperty = new ClientConfig();
			areWaitForAnswer = new AutoresetEvent(false);
			client = new RestaurantMainClient(clientProperty.getPort(), clientProperty.getHost(), areWaitForAnswer);
		} catch (PropertyException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws InterruptedException {
		startClient(primaryStage);
		initData(primaryStage);
		addGUI(primaryStage);
	}

	private void startClient(Stage primaryStage) {
		client.start();
		primaryStage.setOnCloseRequest(e -> {
	        Platform.exit();
	        client.closeClient();
	    });
	}

	private void initData(Stage primaryStage) {
		//TODO creat a thread solution, add timer

			try {
				ClientMain.getClient().isConnectedInTime(areWaitForAnswer.waitOne(6000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		if (client.isConnectedInTime()) {
			client.populateUser(clientProperty.getUserType());
			client.populateProducts(clientProperty.getUserType().getId());
			client.populateUsers();
			client.populateTable(clientProperty.getTableId());
		}
		
		Runnable guiRunnable = () -> { 
			addGUI(primaryStage);
		};
		Platform.runLater(guiRunnable);
	}

	/**
	 * Grafikus felület betöltéséért felelős metódus, a beállításokból eldönti, hogy az adminisztrátor vagy az 
	 * általános felhasználói felületet töltse be.
	 * @param primaryStage Tároló amin megjeleníteni szeretnénk.
	 * @return Helyszín, ami a grafikus felületet a képernyőre rendereli.
	 */
	private Scene addGUI(Stage primaryStage) {
		try {
			boolean isAdmin = UserType.ADMINISTRATOR.equals(clientProperty.getUserType());
			FXMLLoader loader = new FXMLLoader(isAdmin ?
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/AdministratorSurface.fxml")
					: getClass().getResource("/org/restaurant/client/gui/temp/common/Home.fxml"));
			if(isAdmin) {
				Control homePane = loader.load();
				setAdminController(loader.getController());
				primaryStage.setScene(new Scene(homePane));
			} else {
				loader.setControllerFactory(c ->  new HomeController(clientProperty.getUserType()));
				Pane homePane = loader.load();
				setHomeController(loader.getController());
				primaryStage.setScene(new Scene(homePane));
			}
			
			primaryStage.setTitle("Restaurant");
			primaryStage.setFullScreen(clientProperty.isFullScrean());
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return primaryStage.getScene();
	}
	
	@Override
	public void stop() {
		client.closeClient();
	}
	
	public static User getUser() {
		return user;
	}

	public static void setUser(User user) {
		ClientMain.user = user;
	}

	public static SortedSet<Product> getProducts() {
		return products;
	}

	public static List<User> getUsers() {
		return users;
	}

	public static HomeController getHomeController() {
		return homeController;
	}

	public static void setHomeController(HomeController homeController) {
		ClientMain.homeController = homeController;
	}

	public static RestaurantMainClient getClient() {
		return client;
	}

	public static AdministratorSurfaceController getAdminController() {
		return adminController;
	}

	public static void setAdminController(AdministratorSurfaceController adminController) {
		ClientMain.adminController = adminController;
	}

	public static Table getTable() {
		return table;
	}

	public static void setTable(Table table) {
		ClientMain.table = table;
	}

	public static ClientConfig getClientProperty() {
		return clientProperty;
	}
}