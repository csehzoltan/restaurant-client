package org.restaurant.client.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.restaurant.common.model.beans.UserType;

/**
 * A szerver konfigurációs információt kezelő osztály, itt lehet beállítani, hogy a kliens melyik porton keresztül próbáljon kommunikálni,
 * a felhasználó típusát, az asztal típusát és hogy a diagramoknál mely évtől legyenek lekérdezhetők az adatok.
 * @author cseh.zoltan
 *
 */
public class ClientConfig {
	
	private Integer port;
	private UserType userType;
	private Integer tableId;
	private boolean isFullScrean;
	private Integer fromYear;
	private String host;

	/**
	 * Konfigurációs fájlból beolvasott adatokat kezeli, tárolja, melyek a következők lehetnek: port, tableId,
	 * fromYear.
	 * @throws PropertyException
	 */
	public ClientConfig() throws PropertyException {
		process();
	}
	
	private void process() throws PropertyException {
		Properties prop = new Properties();
		//File jarDir = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
		//System.getenv("APPDATA")
		System.out.println(System.getenv("APPDATA") + "/thesis/client/resources/config.properties");
		try (FileInputStream is = new FileInputStream(new File(System.getenv("APPDATA") + "/thesis/client/resources/config.properties"))) {
			prop.load(is);
			
			port = prop.getProperty("port") != null ? Integer.parseInt(prop.getProperty("port")) : null;
			userType = UserType.getByValue(prop.getProperty("userType"));
			tableId = prop.getProperty("tableId") != null ? Integer.parseInt(prop.getProperty("tableId")) : null;
			isFullScrean = Boolean.valueOf(prop.getProperty("isFullScrean"));
			fromYear = prop.getProperty("fromYear") != null ? Integer.parseInt(prop.getProperty("fromYear")) : null;
			host = prop.getProperty("host") != null ? prop.getProperty("host") : "localhost";
		} catch (IOException e) {
			throw new PropertyException("Error in loading config properties...", e);			
		}
	}
	
	/**
	 * Visszatér a port számmal.
	 * @return port szám
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * Visszatér a felhasználó típusával.
	 * @return felhasználó típusa.
	 */
	public UserType getUserType() {
		return userType;
	}

	/**
	 * Visszatér az asztal id-val.
	 * @return asztal id-ja.
	 */
	public Integer getTableId() {
		return tableId;
	}

	/**
	 * Teljes képernyőn fusson-e a kliens program.
	 * @return true, ha teljes képernyős módot szeretnénk, különben false.
	 */
	public boolean isFullScrean() {
		return isFullScrean;
	}
	
	/**
	 * Az év melytől a statisztikai adatok lekérdezhetők legyenek.
	 * @return év, melytől a statisztikai adatok lekérdezhetők.
	 */
	public Integer getFromYear() {
		return fromYear;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}


}
