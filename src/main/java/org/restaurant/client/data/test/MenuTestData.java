package org.restaurant.client.data.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductCategory;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.Size;
import org.restaurant.common.model.beans.Unit;

public class MenuTestData {
	
	List<Product> products;
	
	public List<Product> getProducts() {
//		ProductCategory appetizerCategory = new ProductCategory();
//		appetizerCategory.setId(1);
//		appetizerCategory.setName("Appetizer");
//		
//		ProductCategory drinkCategory = new ProductCategory();
//		drinkCategory.setId(2);
//		drinkCategory.setName("Drink");
		
		
		Product basmatiRice = new Product();
		basmatiRice.setId(1);
		basmatiRice.setName("Basmati Rice");
		basmatiRice.setDescription("Our famous basmati with the added touch of spinach.");
		basmatiRice.setCategory(ProductCategory.APPETIZER);
		List<ProductPrice> prices1 = new ArrayList<>();
		ProductPrice price1 = new ProductPrice();
		price1.setId(1);
		price1.setProduct(basmatiRice);
		price1.setPrice(12.3);
		price1.setSize(Size.SMALL);
		
		ProductPrice price2 = new ProductPrice();
		price2.setId(2);
		price2.setProduct(basmatiRice);
		price2.setPrice(14.2);
		price2.setSize(Size.MEDIUM);
		
		ProductPrice price3 = new ProductPrice();
		price3.setId(3);
		price3.setProduct(basmatiRice);
		price3.setPrice(16.2);
		price3.setSize(Size.LARGE);
		
		prices1.add(price1);
		prices1.add(price2);
		prices1.add(price3);
		basmatiRice.setPrices(prices1);
		
		try {
			basmatiRice.setImg(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("gui.img/rice1.jpg")));
			System.out.println("Image loaded successfully!");
			System.out.print(basmatiRice.getImg().toString());
		} catch (IOException e) {
			System.out.println("Image load failed!");
			e.printStackTrace();
		}
		
		Product vegetableRice = new Product();
		vegetableRice.setId(2);
		vegetableRice.setName("Vegetable Rice");
		vegetableRice.setDescription("Flavorful basmati rice mixed with an assortment of vegetables and spices.");
		vegetableRice.setCategory(ProductCategory.APPETIZER);
		List<ProductPrice> prices2 = new ArrayList<>();
		ProductPrice price4 = new ProductPrice();
		price4.setId(4);
		price4.setProduct(vegetableRice);
		price4.setPrice(12.3);
		price4.setSize(Size.SMALL);
		
		ProductPrice price5 = new ProductPrice();
		price5.setId(5);
		price5.setProduct(vegetableRice);
		price5.setPrice(14.2);
		price5.setSize(Size.MEDIUM);
		
		ProductPrice price6 = new ProductPrice();
		price6.setId(6);
		price6.setProduct(vegetableRice);
		price6.setPrice(16.2);
		price6.setSize(Size.LARGE);
		
		prices2.add(price4);
		prices2.add(price5);
		prices2.add(price6);
		vegetableRice.setPrices(prices2);
		try {
			vegetableRice.setImg(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("gui.img/rice3.jpg")));
			System.out.println("Image loaded successfully!");
			System.out.print(basmatiRice.getImg().toString());
		} catch (IOException e) {
			System.out.println("Image load failed!");
			e.printStackTrace();
		}
		
		
		Product product3 = new Product();
		product3.setId(3);
		product3.setName("Copy Product");
		product3.setDescription("Flavorful basmati rice mixed with an assortment of vegetables and spices.");
		product3.setCategory(ProductCategory.APPETIZER);
		List<ProductPrice> prices3 = new ArrayList<>();
		ProductPrice price7 = new ProductPrice();
		price7.setId(7);
		price7.setProduct(product3);
		price7.setPrice(12.3);
		price7.setSize(Size.SMALL);
		
		ProductPrice price8 = new ProductPrice();
		price8.setId(8);
		price8.setProduct(product3);
		price8.setPrice(14.2);
		price8.setSize(Size.MEDIUM);
		
		ProductPrice price9 = new ProductPrice();
		price9.setId(9);
		price9.setProduct(product3);
		price9.setPrice(16.2);
		price9.setSize(Size.LARGE);
		
		prices3.add(price7);
		prices3.add(price8);
		prices3.add(price9);
		product3.setPrices(prices3);
		try {
			product3.setImg(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("gui.img/tofu.jpg")));
			System.out.println("Image loaded successfully!");
			System.out.print(product3.getImg().toString());
		} catch (IOException e) {
			System.out.println("Image load failed!");
			e.printStackTrace();
		}
		
		Product product4 = new Product();
		product4.setId(4);
		product4.setName("Copy Product2");
		product4.setDescription("Flavorful basmati rice mixed with an assortment of vegetables and spices.");
		product4.setCategory(ProductCategory.APPETIZER);
		List<ProductPrice> prices4 = new ArrayList<>();
		ProductPrice price10 = new ProductPrice();
		price10.setId(10);
		price10.setProduct(product4);
		price10.setPrice(12.3);
		price10.setSize(Size.SMALL);
		
		ProductPrice price11 = new ProductPrice();
		price11.setId(11);
		price11.setProduct(product4);
		price11.setPrice(14.2);
		price11.setSize(Size.MEDIUM);
		
		ProductPrice price12 = new ProductPrice();
		price12.setId(12);
		price12.setProduct(product4);
		price12.setPrice(16.2);
		price12.setSize(Size.LARGE);
		
		prices4.add(price10);
		prices4.add(price11);
		prices4.add(price12);
		product4.setPrices(prices4);
		try {
			product4.setImg(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("gui.img/beef.jpg")));
			System.out.println("Image loaded successfully!");
			System.out.print(product4.getImg().toString());
		} catch (IOException e) {
			System.out.println("Image load failed!");
			e.printStackTrace();
		}
		
		List<ProductPrice> prices5 = new ArrayList<>();
		ProductPrice price13 = new ProductPrice();
		price13.setId(13);
		price13.setPrice(3.2);
		price13.setUnit(Unit.DL);
		price13.setAmount(2.0);
		
		ProductPrice price14 = new ProductPrice();
		price14.setId(14);
		price14.setPrice(4.0);
		price14.setUnit(Unit.DL);
		price14.setAmount(3.0);
		
		prices5.add(price13);
		prices5.add(price14);
		
		Product cola = new Product();
		cola.setId(5);
		cola.setName("Coca Cola");
		cola.setCategory(ProductCategory.DRINK);
		cola.setDescription("Coca-cola is a carbonated soft dringk produced by the Coca-cola Company.");
		cola.setPrices(prices5);
		try {
			cola.setImg(IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("gui.img/cola.jpg")));
			System.out.println("Image loaded successfully!");
			System.out.print(basmatiRice.getImg().toString());
		} catch (IOException e) {
			System.out.println("Image load failed!");
			e.printStackTrace();
		}	
		
		List<Product> productList = new ArrayList<>();
		productList.add(basmatiRice);
		productList.add(vegetableRice);
		productList.add(product3);
		productList.add(product4);
		productList.add(cola);
		
		products = productList;
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products=products;
	}
}
