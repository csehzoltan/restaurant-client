package org.restaurant.client.gui.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.restaurant.common.model.beans.Month;

import com.jfoenix.controls.JFXComboBox;

public class DateHelper {

	private DateHelper() {
	    throw new IllegalStateException("Utility class");
	}
	
	public static void fillDateCheckBoxes(JFXComboBox<Integer> yearsCB, Integer fromYear) {
		Integer actYear = Calendar.getInstance().get(Calendar.YEAR);
		if (fromYear != null && fromYear < actYear) {
			while (fromYear <= actYear) {
				yearsCB.getItems().add(actYear--);
			}
		} else {
			yearsCB.getItems().add(actYear);
		}

		yearsCB.getSelectionModel().selectFirst();
	}
	
	public static void fillMonthCheckBox(JFXComboBox<String> monthsCB) {
		for (Month month : Month.values()) {
			monthsCB.getItems().add(month.ordinal(), month.getName());
		}
		
		monthsCB.getSelectionModel().select(Month.getByNo(Calendar.getInstance().get(Calendar.MONTH) + 1).ordinal());
	}
	
	public static Date getDateBySearchFields(JFXComboBox<Integer> yearsCB, JFXComboBox<String> monthsCB) {
		Date searchDate = null;
		try {
			searchDate = new SimpleDateFormat("yyyymm").parse(String.valueOf(yearsCB.getValue()) 
					+ String.format("%02d", monthsCB!= null ? Month.getByName(monthsCB.getValue()).getNo() : 1));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return searchDate;
	}
}
