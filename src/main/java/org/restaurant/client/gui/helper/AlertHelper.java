package org.restaurant.client.gui.helper;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * Figyelmeztető üzenetek kezelésére készült osztály.
 * @author cseh.zoltan
 *
 */
public class AlertHelper {
	
	private AlertHelper() {}
	
	/**
	 * Figyelmeztetőüzenetet dob, a megadott alert típusnak megfelelően, címnek beírja az alert típus neve.
	 * @param alertType alert típusa.
	 * @param msg megjelenítendő üzenet.
	 */
	public static void callAlert(AlertType alertType, String msg) {
		Alert alert = new Alert(alertType);
		alert.setTitle(alertType.name());
		alert.setHeaderText(null);
		alert.setContentText(msg);
		alert.showAndWait();
	}
	
}
