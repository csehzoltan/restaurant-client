package org.restaurant.client.gui.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.CoordXY;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.chart.PieChart;

/**
 * JavaFX diagramok egyszerűbb létrehozásáért felelős.
 * @author cseh.zoltan
 *
 */
public class ChartHelper {
	
	private ChartHelper() {
		super();
	}
	
	/**
	 * A coordsYX objektum lista segítségével létrehoz kördiagrammokat tartalmazó
	 * mappet.
	 * 
	 * @param coordsXY
	 *            Ezen objektum x koordinátához tartozó értéke szerint csoportosítja
	 *            a kördiagramokat, a kördiagram címének ezt az értéket állítja be.
	 *            y-hoz tartozó érték a kördiagram szövege, szögletes zárójelbe a
	 *            hozzá tartozó értéket is beírja. A value paramétere határozza meg
	 *            a kördiagram adott cikkekhez tartozó értékeket.
	 * @return Mappel tér vissza, melynek kulcsa tartalmazza a kördiagram címét
	 *         értéke pedig magát a kördiagram objektumot.
	 */
	public static Map<String, PieChart> getPieCharts(List<CoordXY> coordsXY) {
		Map<String, PieChart> pieCharts = new HashMap<>();
		for (CoordXY coordxy : coordsXY) {
			if (pieCharts.get(coordxy.getxText()) == null) {
				PieChart chart = new PieChart();
				chart.setTitle(coordxy.getxText().toUpperCase());
				chart.setLegendSide(Side.BOTTOM);
				ObservableList<PieChart.Data> pcData = FXCollections.observableArrayList();
				pcData.add(new PieChart.Data(coordxy.getyText() + " [" + Math.round(coordxy.getValue()) + "]",
						coordxy.getValue()));
				chart.setData(pcData);
				pieCharts.put(coordxy.getxText(), chart);
			} else {
				pieCharts.get(coordxy.getxText()).getData().add(new PieChart.Data(
						coordxy.getyText() + " [" + Math.round(coordxy.getValue()) + "]", coordxy.getValue()));
			}
		}
		return pieCharts;
	}
}
