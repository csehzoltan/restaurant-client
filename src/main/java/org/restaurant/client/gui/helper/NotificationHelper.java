package org.restaurant.client.gui.helper;

import org.controlsfx.control.Notifications;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.util.Duration;

public class NotificationHelper {
	
	/**
	 * Notifikációt létrehozó metódus, meghívásával megjelenik egy értesítő ablak a képernyő megadott pozícióján
	 * 5 másodpercig egy értesítő üzenettel.
	 * @param msg notifikációs üzenet.
	 * @param pos notifikáció pozíciója.
	 * @param title notifikáció címe.
	 */
	public void showNotification(String msg, Pos pos, String title) {
		Platform.runLater(() -> {
			Notifications requestNotification = Notifications.create()
	    			.title(title)
	    			.text(msg)
	    			.graphic(null)
	    			.hideAfter(Duration.seconds(5))
	    			.position(pos);
			requestNotification.show();
		});
	}
}
