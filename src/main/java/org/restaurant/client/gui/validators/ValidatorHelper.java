package org.restaurant.client.gui.validators;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.validation.RequiredFieldValidator;

import de.jensd.fx.glyphs.GlyphsBuilder;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * Beviteli mezők validálására készült osztály (java fx). A kötelező mezők kitöltésére vannak csak metódusok megvalósítva,
 * ezen osztály a JFoenix által nyújtott validálási lehetőségek kiegészítésére jött létre, azonban a JFoenixnek kiderült,
 * hogy ezen verziójában csak a RequiredFieldValidator osztály működik hibátlanul, ezért ezt a megoldást csak a bejelentkezési
 * és a regisztrációs oldalnál használom. Későbbiekben esetleg ki lehet próbálni más verziókat, hogy javították-e a hibákat.
 * @author cseh.zoltan
 *
 */
public class ValidatorHelper {
	
	/**
	 * Szövegmezőre rárak, egy kötelező mező validátort.
	 * @param textField Szöveges mező amihez kötelező mező validálást szeretnék hozzáadni.
	 */
	public static void addRequiredValidator(JFXTextField textField) {
		RequiredFieldValidator validator = createRequiredValidator();
		textField.getValidators().add(validator);
		textField.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
            	textField.validate();
            }
        });
	}
	
	/**
	 * Jelszó típusú mezőhöz ad kötelező mező kitöltése validátort.
	 * @param pwField jelszó mező, melyre a validátort szeretnénk rakni.
	 */
	public static void addRequiredValidator(JFXPasswordField pwField) {
		RequiredFieldValidator validator = createRequiredValidator();
		pwField.getValidators().add(validator);
		pwField.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
            	pwField.validate();
            }
        });
	}
	
	private static RequiredFieldValidator createRequiredValidator() {
		RequiredFieldValidator validator = new RequiredFieldValidator();
		validator.setMessage("Required field!");
		validator.setIcon(GlyphsBuilder.create(FontAwesomeIconView.class).glyph(FontAwesomeIcon.WARNING).size("15").build());
		return validator;
	}
	
	/**
	 * Végigmegy a megadott pane gyerekein és kötelező mező validátort rak rájuk.
	 * @param pane gyökér, melynek gyerekeire a kötelező mező validátort szeretnénk rakni.
	 */
	public static void addValidatorToPaneFields(Pane pane) {
		for(Node childField : pane.getChildren()) {
			if(childField instanceof JFXTextField) {
				addRequiredValidator((JFXTextField) childField);
			} else if (childField instanceof JFXPasswordField) {
				addRequiredValidator((JFXPasswordField) childField);
			}
		}
	}
	
	/**
	 * A megadott pane gyökér összes gyerekén végigmegy és ellenőrzi a validságukat.
	 * @param pane gyökér pane, melynek a gyerekeire szeretnénk a validálás ellenőrzést rakni.
	 * @return true ha nincs validálási hiba, külben false.
	 */
	public static boolean hasValidationError(Pane pane) {
		boolean hasValidationError=false;
		boolean actIsValid = true;
		for(Node childField : pane.getChildren()) {
    		if(childField instanceof JFXTextField) {
    			actIsValid = ((JFXTextField) childField).validate();
    		} else if (childField instanceof JFXPasswordField) {
    			actIsValid = ((JFXPasswordField) childField).validate();
    		}
    		if(!actIsValid && !hasValidationError) {
    			hasValidationError = true;
    		}
    	}
		return hasValidationError;
	}
	
}
