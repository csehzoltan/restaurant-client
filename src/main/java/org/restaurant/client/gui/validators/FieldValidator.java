package org.restaurant.client.gui.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class FieldValidator {

	public static final String EMAILREGEXP = "[a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ._%+-]+@[a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ.-]+\\.[a-zA-Z]{2,10}$";
	public static final String NAMEREGEXP = "[a-zA-ZáéíóöőúüűÁÉÍÓÖŐÚÜŰ ]*";
	public static final String ACCOUNTREGEXP = "[a-zA-Z0-9._-]*";
	public static final String PASSREGEXP = "[a-zA-Z0-9áéíóöőúüűÁÉÍÓÖŐÚÜŰ@#$%*&<>+-_?!,.;()\\[\\]{}]*";
	public static final String PHONENUMBERREGEXP = "((\\+?3|0)6)(-|\\()?(\\d{1,2})(-|\\))?(\\d{3})-?(\\d{3,4})";
	public static final String ANYCHARACTERREGEXP = ".*";
	public static final String DECIMALREGEXP = "(\\d+\\.)?\\d+$";

	public static final String MSGACCOUNTRULE = "Only letters, numbers, and (._-) symbols are allowed.";
	public static final String MSGPASSRULE = "Only letters, numbers and (@#$%&+=) symbols are allowed.";
	public static final String MSGNAMERULE = "Only, letters and space symbol allowed.";
	public static final String DECIMALRULE = "Number format is invalid! (Examples: 0, 0.0, 1.22)";
	
	/**
	 * Létrehozza az figyelmeztető ablakot, a megfelelő címkével és szöveggel.
	 * @param promptText a mező segédszövege (placeholder, prompt text), 
	 * ezen érték alapján jelezzük vissza a felhasználónak, hogy melyik mező hibás.
	 * @param text szöveg hosszúság ellenőrzéséhez a küldendő szöveg.
	 * @param empty üres-e a mező.
	 * @param isEmptyValidation elfogadhat-e üres szöveget.
	 * @param minChar minimum hossza a szövegnek.
	 * @param maxChar milyen hosszúságú lehet a megadott szöveg (max limit).
	 * @param msg egyéb hibaüzenet szövege.
	 */
	private void validationAlert(String promptText, String text, boolean empty, boolean isEmptyValidation, Integer minChar, Integer maxChar, String msg) {
		Alert alert = createWarningAlert();
		if(msg == null) {
			msg = "";
		}
		String maxCharMsg = "";
		if(maxChar != null && text.length() > maxChar) {
			maxCharMsg = " Please correct it [max length: " + maxChar + "]! ";
		}
		
		String minCharMsg = "";
		if(minChar != null && text.length() < minChar) {
			minCharMsg = " Please correct it [min length: " + minChar + "]! ";
		}
		
		if (empty && isEmptyValidation) {
			alert.setContentText("Please fill the " + "\"" + promptText + "\"" + " field!");
		} else {
			alert.setContentText(promptText + " field is not valid! " + maxCharMsg + minCharMsg + msg);
		}

		alert.showAndWait();
	}
	
	private Alert createWarningAlert() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		return alert;
	}
	
	/**
	 * Létrehozza az figyelmeztető ablakot, a megfelelő címkével és szöveggel.
	 * @param promptText a mező segédszövege (placeholder, prompt text), 
	 * ezen érték alapján jelezzük vissza a felhasználónak, hogy melyik mező hibás.
	 * @param text szöveg hosszúság ellenőrzéséhez a küldendő szöveg.
	 * @param empty üres-e a mező.
	 * @param isEmptyValidation elfogadhat-e üres szöveget.
	 * @param minChar minimum hossza a szövegnek.
	 * @param maxChar milyen hosszúságú lehet a megadott szöveg (max limit).
	 * @param msg egyéb hibaüzenet szövege.
	 */
	public boolean validateTf(JFXTextField field, String pattern, Boolean isEmptyValidation, Integer minChar,
			Integer maxChar, String msg) {
		String fieldText = field.getText();
		return validateText(fieldText, field.getPromptText(), pattern, isEmptyValidation, minChar, maxChar, msg);
	}

	private boolean validateText(String fieldText, String promptText, String pattern, Boolean isEmptyValidation, Integer minChar,
			Integer maxChar, String msg) {
		if (fieldText != null && !fieldText.isEmpty()) {
			Pattern pat = Pattern.compile(pattern);
			Matcher matcher = pat.matcher(fieldText);
			if (matcher.matches() 
					&& ((maxChar == null) || (fieldText.length() <= maxChar))
					&& ((minChar == null) || (fieldText.length() >= minChar))) {
				return true;
			} else {
				validationAlert(promptText, fieldText, false, isEmptyValidation, minChar, maxChar,
						msg);
				return false;
			}
		} else if (isEmptyValidation && (fieldText == null || fieldText.isEmpty())) {
			validationAlert(promptText, fieldText, true, isEmptyValidation, minChar, maxChar, msg);
			return false;
		}
		return true;
	}
	
	/**
	 * Validálja a dátum bekérő mezőt, amennyiben üres, akkor hibaüzenetet dob.
	 * @param datePicker dátum beviteli mező.
	 * @return igaz, ha ki van töltve, különben hamis.
	 */
	public boolean validateDatePickerRequired(JFXDatePicker datePicker) {
		if (datePicker.getValue() == null) {
			Alert alert = createWarningAlert();
			alert.setContentText("Please fill the " + "\"" + datePicker.getPromptText() + "\"" + " field!");
			alert.showAndWait();
			return false;
		}
		return true;
	}
	
	/**
	 * Kombinált listát leellenőrzi, hogy üres-e, amennyiben igen hibaüzenetet dob.
	 * @param combobox kombinált lista.
	 * @return igaz, ha valid különben hamis.
	 */
	public boolean validateComboBoxRequired(JFXComboBox<String> combobox) {
		if (combobox.getValue() == null) {
			Alert alert = createWarningAlert();
			alert.setContentText("Please fill the " + "\"" + combobox.getPromptText() + "\"" + " field!");
			alert.showAndWait();
			return false;
		}
		return true;
	}
	
	/**
	 * Létrehozza az figyelmeztető ablakot, a megfelelő címkével és szöveggel.
	 * @param promptText a mező segédszövege (placeholder, prompt text), 
	 * ezen érték alapján jelezzük vissza a felhasználónak, hogy melyik mező hibás.
	 * @param text szöveg hosszúság ellenőrzéséhez a küldendő szöveg.
	 * @param empty üres-e a mező.
	 * @param isEmptyValidation elfogadhat-e üres szöveget.
	 * @param minChar minimum hossza a szövegnek.
	 * @param maxChar milyen hosszúságú lehet a megadott szöveg (max limit).
	 * @param msg egyéb hibaüzenet szövege.
	 */
	public boolean validatePwf(JFXPasswordField pwField, String pattern, Boolean isEmptyValidation, Integer minChar,
			Integer maxChar, String msg) {
		return validateText(pwField.getText(), pwField.getPromptText(), pattern, isEmptyValidation, minChar, maxChar, msg);
	}

}
