package org.restaurant.client.gui.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang.SerializationUtils;
import org.controlsfx.control.Notifications;
import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.helpers.NumberHelper;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductOrderState;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.Table;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jfoenix.controls.JFXButton;

import eu.hansolo.tilesfx.Tile;
import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.IntegerStringConverter;

public class TableRequestDialogController {
	
    @FXML
    private TableView<ProductOrder> orderTable;
    
    @FXML
    private TableColumn<ProductOrder, String> colChair;

    @FXML
    private TableColumn<ProductOrder, String> colProduct;
    
    @FXML
    private TableColumn<ProductOrder, String> colProductCategory;
    
    @FXML
    private TableColumn<ProductOrder, String> colSize;

    @FXML
    private TableColumn<ProductOrder, Integer> colQuantity;
    
    @FXML
    private TableColumn<ProductOrder, Integer> colRejectedQty;

    @FXML
    private TableColumn<ProductOrder, String> colStatus;

    @FXML
    private HBox chairDataHBox;
    
    @FXML
    private Label totalPriceLabel;
    
    @FXML
    private Pane sampleChairPane;

    @FXML
    private ScrollPane chairScrollPane;

    @FXML
    private Pane progressIndicator;

    @FXML
    private Label userNameLabel;

    @FXML
    private Label tableNameLabel;

    @FXML
    private Label noOrderLabel;
    
    @FXML
    private JFXButton payButton;
    
    @FXML
    private JFXButton saveBtn;
   
    private Table table;
    
    private Tile circularProgressTile;

	private ProductRequest request;
    
	private WaiterTableController parentController;
	
	private Set<Integer> modifiedOrderIds;
	
    private List<ChairController> chairControllers;
	
    /**
     * A felszolgáló által látott rendelések felületnek a szükséges háttér adatát tölti fel a megadott értékekkel.
     * @param table asztal ahova a rendelés tartozik.
     * @param request kérés, ami összefoglalja a rendelés listát.
     */
    public TableRequestDialogController(Table table, ProductRequest request) {
    	this.table = table;
    	this.request = request;
    }
	
	/**
	 * Inicializálja a felületet, beállítja a táblanevet, felhasználónevet, feltölti adatokkal a rendelések táblát és kiszámolja a székekhez a kifizetendő összeget.
	 * @throws IOException
	 */
	@FXML
	public void initialize() throws IOException {
		modifiedOrderIds = new HashSet<>();
		hideSampleChair();
		addSuccessOrderCircularTile();
		tableNameLabel.setText(table.getName());
		userNameLabel.setText(request.getWaiter().getName() != null ? request.getWaiter().getName()
				: request.getWaiter().getAccountName());
		noOrderLabel.setText(String.valueOf(request.getProductOrders().size()));
		initOrderTable();
		addChairs();
	}

	/**
	 * Megrendelések segítségével kiszedi a rendelést érintő székeket külön eltárolja, ahol nincs szék kitöltve annak -1-es id-t ad
	 * és üresen létrehozza, kiszámolja minden egyes székre az általa leadott rendeléseinek az árát és megjeleníti a felületen.
	 * @throws IOException
	 */
	private void addChairs() throws IOException {
		HashMap<Chair, Double> chairPrices = new HashMap<>();
		Chair emptyChair = new Chair();
		emptyChair.setId(-1);
		emptyChair.setName("");

		for (ProductOrder order : request.getProductOrders()) {
			if (order.getChair() != null) {
				addChairPrice(chairPrices, order, order.getChair());
			} else {
				addChairPrice(chairPrices, order, emptyChair);
			}
		}
		chairControllers = new ArrayList<>();
		for (Entry<Chair, Double> chairPrice : chairPrices.entrySet()) {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/waiter/Chair.fxml"));
			loader.setControllerFactory(c -> new ChairController(chairPrice.getKey(), chairPrice.getValue()));
			Parent chair = loader.load();
			ChairController chairController = loader.getController();
			chairControllers.add(chairController);
			
			chairDataHBox.getChildren().add(chair);
		}
	}

	/**
	 * Az adott székhez hozzáadja a megrendelt termék árát a megrendelt mennyiség és az elutasított mennyiség alapján.
	 * @param chairPrices székek eddig kiszámolt összegeivel.
	 * @param order rendelés, ami a megrendelt terméket és a rendelt mennyiséget tartalmazza.
	 * @param chair szék, ahova a rendelés tartozik.
	 */
	private void addChairPrice(HashMap<Chair, Double> chairPrices, ProductOrder order, Chair chair) {
		Double price = 0.0;
		if(order.getProductPrice() != null) {
			double rejectedQuantity = order.getRejectedQuantity() * NumberHelper.scaleDouble(order.getProductPrice().getPrice(), 2);
			
			price = order.getQuantity() * NumberHelper.scaleDouble(order.getProductPrice().getPrice(), 2) - rejectedQuantity;
		}
		
		chairPrices.merge(chair, price, Double::sum);
	}

	/**
	 * Elrejti a felületen mintaként berakott széket (scenebuildernél mintaként van elhelyezve).
	 */
	private void hideSampleChair() {
		sampleChairPane.setVisible(false);
		sampleChairPane.setManaged(false);
	}

	/**
	 * Rendelés tábla értékeinek feltöltése, megjelenítése, események kezelése.
	 */
	private void initOrderTable() {
		colChair.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getChair() != null ? cell.getValue().getChair().getName() : ""));
		colProduct.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getProductPrice().getProduct().getName()));
		colProductCategory.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getProductPrice().getProduct().getCategory().getName()));
		colQuantity.setCellValueFactory(new PropertyValueFactory<ProductOrder, Integer>("quantity"));
		
		colSize.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getProductPrice().getAmountDisplay()));
		colStatus.setCellValueFactory(cell -> new SimpleStringProperty(cell.getValue().getState().getDisplay()));
		
		colRejectedQty.setEditable(true);
		colRejectedQty.setCellValueFactory(new PropertyValueFactory<ProductOrder, Integer>("rejectedQuantity"));
		colRejectedQty.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter() {
			@Override
			public Integer fromString(String value) {
				try {
					return super.fromString(value);
				} catch(NumberFormatException e) {
					Notifications requestNotification = Notifications.create().title("Field validation error!")
							.text("The rejected quantity value must be a number!").graphic(null).hideAfter(Duration.seconds(5))
							.position(Pos.BOTTOM_RIGHT);
					requestNotification.show();
					return 0;
				}
				
			}
		}));
		colRejectedQty.setOnEditCommit(event -> {
			modifiedOrderIds.add(event.getRowValue().getId());
			if (event.getNewValue() != null && (event.getNewValue() > event.getRowValue().getQuantity() || event.getNewValue() < 0)) {
				int oldValue = event.getRowValue().getRejectedQuantity();
				double itemPrice = event.getRowValue().getProductPrice().getPrice();
				
				recalculateCharisByOldValue(event, oldValue, itemPrice);
				
				Notifications requestNotification = Notifications.create().title("Field validation error!")
						.text("The given quantity should be between 0 and " + event.getRowValue().getQuantity() + "!").graphic(null)
						.hideAfter(Duration.seconds(5)).position(Pos.BOTTOM_RIGHT);
				requestNotification.show();
				
			} else {
				int newValue = event.getNewValue() == null ? 0 : event.getNewValue();
				int oldValue = event.getRowValue().getRejectedQuantity();
				double itemPrice = event.getRowValue().getProductPrice().getPrice();
				
				setChairPricesAndCircularProgressTile(event, newValue, oldValue, itemPrice);
				event.getRowValue().setRejectedQuantity(event.getNewValue());
				if(event.getRowValue().getQuantity() == newValue) {
					event.getRowValue().setState(ProductOrderState.DONE);
				}
				event.getTableView().refresh();
			}

		});
		
		ObservableList<String> orderStates = FXCollections.observableArrayList();
		orderStates.add(ProductOrderState.INPROGRESS.getDisplay());
		orderStates.add(ProductOrderState.DONE.getDisplay());
		
		colStatus.setCellFactory(ComboBoxTableCell.forTableColumn(new DefaultStringConverter(), orderStates));
		colStatus.setOnEditCommit(event -> {
			modifiedOrderIds.add(event.getRowValue().getId());
			event.getRowValue().setState(ProductOrderState.getByValue(event.getNewValue()));
			circularProgressTile.setValue(((double) getCompletedOrderNo(event) / event.getTableView().getItems().size()) * 100);
		});

		ObservableList<ProductOrder> orders;
		synchronized (ClientMain.getProducts()) {
			orders = FXCollections.observableArrayList();
			initOrderData(orders);
		}
		if (orders != null) {
			orderTable.setItems(orders);
		}
	}

	private void setChairPricesAndCircularProgressTile(CellEditEvent<ProductOrder, Integer> event, int newValue,
			int oldValue, double itemPrice) {
		for (ChairController chair : getChairControllers()) {
			if (chair.getChair().getName().equals(event.getRowValue().getChair().getName())) {
				double result = (chair.getPrice() + oldValue * itemPrice) - newValue * itemPrice;
				if(newValue == event.getRowValue().getQuantity()) {
					event.getRowValue().setState(ProductOrderState.DONE);
//							event.getTableView().refresh();
					int completedOrder = 0;
					for (ProductOrder row : event.getTableView().getItems()) {
						if (row.getState().getDisplay().equals(ProductOrderState.DONE.getDisplay())) {
							completedOrder++;
						}
					}
					circularProgressTile.setValue(((double)completedOrder / event.getTableView().getItems().size()) * 100);
				}
				
				chair.setPrice(NumberHelper.scaleDouble(result, 2));
				chair.getPriceLabel().setText(String.valueOf(NumberHelper.scaleDouble(result, 2) + "$"));
			}
		}
	}

	/**
	 * Újraszámolja az asztalokhoz tartozó árakat. (Azért szükséges, hogyha szöveget írunk be, akkor csak így tudjuk visszaállítani a régi értékeket).
	 * @param event esemény.
	 * @param oldValue régi elutasított mennyiség.
	 * @param itemPrice termék ára.
	 */
	private void recalculateCharisByOldValue(CellEditEvent<ProductOrder, Integer> event, int oldValue,
			double itemPrice) {
		for (ChairController chair : getChairControllers()) {
			double result = (chair.getPrice() + oldValue * itemPrice);
			chair.setPrice(NumberHelper.scaleDouble(result, 2));
			chair.getPriceLabel().setText(String.valueOf(NumberHelper.scaleDouble(result, 2) + "$"));
			
			event.getRowValue().setRejectedQuantity(oldValue);
			event.getTableView().refresh();
		}
	}

	private int getCompletedOrderNo(CellEditEvent<ProductOrder, String> event) {
		int completedOrder = 0;
		for (ProductOrder row : event.getTableView().getItems()) {
			if (row.getState().getDisplay().equals(ProductOrderState.DONE.getDisplay())) {
				completedOrder++;
			}
		}
		return completedOrder;
	}

	/**
	 * Kiszámolja a teljes fizetendő összeget az adott kéréshez és hozzáadja a 
	 * rendelés adataihoz a termék adatait main osztályban tárolt termékelből.
	 * @param orders kéréshez tartozó rendelések.
	 */
	private void initOrderData(ObservableList<ProductOrder> orders) {
		double priceSum = 0.0;
		
		if(request != null) {
			ProductRequest tempProductRequest = (ProductRequest) SerializationUtils.clone(request);
			for (ProductOrder order : tempProductRequest.getProductOrders()) {
				for (Product product : ClientMain.getProducts()) {
					for (ProductPrice price : product.getPrices()) {
						if (order.getProductPrice().getId().equals(price.getId())) {
							order.getProductPrice().setProduct(new Product(product.getId(), product.getName(), product.getCategory(), product.getDescription(), new byte[]{}, null, null));
							orders.add(order);
							double rejectedPrice = order.getRejectedQuantity() * price.getPrice();
							priceSum += (order.getQuantity() * price.getPrice()) - rejectedPrice;
						}
					}
				}
			}
			
		}
		totalPriceLabel.setText(NumberHelper.scaleDouble(priceSum, 2) + " $");
	}

	/**
	 * Rendelés teljesítését százalékosan megjelenítő kör ábra kirajzolása.
	 */
	private void addSuccessOrderCircularTile() {
		circularProgressTile = TileBuilder.create()
                .skinType(SkinType.CIRCULAR_PROGRESS)
                .prefSize(150, 150)
                .title("Completed")
                .backgroundColor(Color.web("#f4f4f4", 1.0))
                .foregroundBaseColor(Color.web("#3f4652", 1.0))
                .barBackgroundColor(Color.web("#bababa", 0.5))
                .barColor(Color.web("#36cbd0", 1.0))
                .unit("\u0025")
                .animated(true)
                .build();
		circularProgressTile.setValue(getDoneOrderPercent());
		progressIndicator.getChildren().add(circularProgressTile);
	}

	/**
	 * @return Lezárt (elutasított, sikeresen kiküldött) rendelések százalékos értékét adja vissza.
	 */
	private double getDoneOrderPercent() {
		double doneOrderNo = 0.0;
		for (ProductOrder order : getRequest().getProductOrders()) {
			if (order.getState().equals(ProductOrderState.DONE)) {
				doneOrderNo++;
			}
		}
		if (!getRequest().getProductOrders().isEmpty()) {
			return (doneOrderNo / getRequest().getProductOrders().size()) * 100;
		} else return 0;
	}
	
    /**
     * A felszolgáló elfogadja a fizetést és jelzi ezt a szervernek (ahol mentésre kerül a váltás), majd kitörli az asztalt a listájából.
     * @param event
     */
    @FXML
    void pay(ActionEvent event) {
    	if(getParentController().isVisibleOneOfPayIcon()) {
    		
    		Alert alert = new Alert(AlertType.CONFIRMATION);
    		alert.setTitle("Message");
    		alert.setHeaderText("Close order");
    		alert.setContentText("Cannot edit the data after payment (The status of all orders will be done). Are you ok with this?");

    		Optional<ButtonType> result = alert.showAndWait();
    		if (result.isPresent() && result.get() == ButtonType.OK){
    			JsonObject jsMsg = createJsonMsg("acceptPayByWaiter", getRequest().getId());
        		PrintWriter clientWriter = ClientMain.getClient().getWriter();
        		clientWriter.println(jsMsg.toString());
        		clientWriter.flush();
        		
        		delteTable();
        		
        		payButton.setDisable(true);
        		saveBtn.setDisable(true);
        		orderTable.setDisable(true);
    		}
    	} else {
    		AlertHelper.callAlert(AlertType.WARNING, "The guest did not indicate his payment intent!");
    	}
    }

	/**
	 * Szülőből kitörli az asztalt (megrendelést).
	 */
	private void delteTable() {
		getParentController().getParentPane().getChildren().remove(getParentController().getTablePane());
		getParentController().getParentConroller().getTables().remove(getParentController());
	}
    
    /**
     * Elküldi a módosításokat a szervernek és a szülőben újraszámolja a teljesített kéréseket.
     * @param event
     */
	@FXML
	void save(ActionEvent event) {
		List<ProductOrder> modifiedProductOrders = new ArrayList<>();
		for (ProductOrder rowOrder : orderTable.getItems()) {
			if (modifiedOrderIds.contains(rowOrder.getId())) {
				for(ProductOrder order : request.getProductOrders()) {
					if(order.getId().equals(rowOrder.getId())) {
						order.setState(rowOrder.getState());
						order.setRejectedQuantity(rowOrder.getRejectedQuantity());
					}
				}
				modifiedProductOrders.add(rowOrder);
			}
		}
		PrintWriter clientWriter = ClientMain.getClient().getWriter();
		clientWriter.println(JsonHelper
				.createJsonMsgList("saveRequestOrdersByWaiter", modifiedProductOrders, ProductOrder.class).toString());
		clientWriter.flush();
		if (!modifiedProductOrders.isEmpty()) {
			parentController.reInitalize(null);
		}
	}
    
	//TODO json helper class
	private JsonObject createJsonMsg(String actionType, Object data) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(data);
		//JsonObject obj = new JsonParser().parse(json).getAsJsonObject();

		JsonElement actionNameJsonElement = new JsonParser().parse(actionType);
		JsonObject sendMsg = new JsonObject();

		sendMsg.add("action", actionNameJsonElement);
		sendMsg.add("data", new JsonParser().parse(json));
		return sendMsg;
	}
	
	public WaiterTableController getParentController() {
		return parentController;
	}

	public void setParentController(WaiterTableController parentController) {
		this.parentController = parentController;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public ProductRequest getRequest() {
		return request;
	}

	public void setRequest(ProductRequest request) {
		this.request = request;
	}

	public List<ChairController> getChairControllers() {
		return chairControllers;
	}

	public void setChairControllers(List<ChairController> chairControllers) {
		this.chairControllers = chairControllers;
	}
}
