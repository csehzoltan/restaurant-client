package org.restaurant.client.gui.controllers;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.List;

import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Ingredient;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class IngredientAlertsController {
	
    @FXML
    private TableView<Ingredient> alertTable;

    @FXML
    private TableColumn<Ingredient, String> colIngredient;

    @FXML
    private TableColumn<Ingredient, Double> colActualNo;

    @FXML
    private TableColumn<Ingredient, Double> colLimit;
	
	/**
	 * Inicializálja a tábla oszlopait, majd betölti a táblázatot a fogyóban lévő összetevőkről.
	 */
	@FXML
	public void initialize() {
		initAlertTable();
		
		if(ClientMain.getClient() != null) {
			ClientMain.getClient().getWriter().println(JsonHelper.jsonActonRequest("setIngredientAlerts", ""));
			ClientMain.getClient().getWriter().flush();
		}
		
	}

	private void initAlertTable() {
		alertTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		colIngredient.setCellValueFactory(new PropertyValueFactory<Ingredient, String>("name"));
		colActualNo.setCellValueFactory(new PropertyValueFactory<Ingredient, Double>("quantity"));
		colLimit.setCellValueFactory(new PropertyValueFactory<Ingredient, Double>("alertLimit"));
	}
	
	 /**
     * A vendégkönyv alapra betölti a vendégkönyveket.
     * @param guestbookItems Vendégkönyvek listája.
     */
	public void loadIngredientAlerts(List<Ingredient> ingredients) {
		Platform.runLater(() -> {
			ObservableList<Ingredient> ingredientTableItems = FXCollections.observableArrayList();
			for (Ingredient ingredient : ingredients) {
				ingredientTableItems.add(ingredient);
			}

			if (ingredientTableItems != null) {
				alertTable.setItems(ingredientTableItems);
			}
		});
	}
}
