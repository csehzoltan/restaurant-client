package org.restaurant.client.gui.controllers;

import java.io.IOException;

import org.controlsfx.control.Notifications;
import org.restaurant.client.gui.logics.Loggin;
import org.restaurant.client.gui.validators.ValidatorHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LoginController {

    @FXML
    private Pane loginPane;
	
    @FXML
    private ImageView userIcon;

    @FXML
    private JFXTextField userNameField;
    
    @FXML
    private HBox signUpHBox;
    
    @FXML
    private VBox vboxLogin;

    @FXML
    private JFXPasswordField passwordField;
    
    @FXML
    private Label helperTextLbl;
    
    @FXML
    private Label messageLabel;

    @FXML
    private JFXButton logInBtn;

    @FXML
    private Hyperlink signUpLink;

    @FXML
    private Hyperlink forgottPassLink;
    
    private HomeController homeController;
    
    private AdministratorSurfaceController adminController;

	private UserType userType;
    
    public LoginController(UserType userType) {
    	this.userType = userType;
    }
    
	/**
	 * Ha pincérként jelentkezünk be, akkor 
	 * eltűnteti a főmenüből a regisztrációs gombot, adminisztrátorként sem érhető el a regisztráció, egyébként 
	 * megjelenít a regisztrációs oldalhoz egy linket.
	 */
	public void initialize() {
		//RestaurantMain.class.getResource("/gui.css/main.css").toExternalForm()
		userIcon.setImage(new Image(getClass().getResource("/gui.img/user-icon.png").toExternalForm()));
		
		forgottPassLink.setVisible(false);
		forgottPassLink.setManaged(false);
		if(UserType.WAITER.equals(userType)) {
			signUpLink.setVisible(false);
			signUpLink.setManaged(false);
			helperTextLbl.setMinHeight(70.0);
			helperTextLbl.setText("If you don't have an account contact an administrator!");
		} else if (UserType.ADMINISTRATOR.equals(userType)) {
			
			userNameField.setStyle("-jfx-focus-color: #247291; -jfx-unfocus-color: #247291");
			passwordField.setStyle("-jfx-focus-color: #247291; -jfx-unfocus-color: #247291");
			logInBtn.setStyle("-fx-background-color: #247291");
			
			signUpLink.setVisible(false);
			signUpLink.setManaged(false);
			helperTextLbl.setMinHeight(50.0);
			helperTextLbl.setText("If you don't have an account contact an admin!");
		}
		ValidatorHelper.addValidatorToPaneFields(vboxLogin);
	}
    
    @FXML
    void forgottPass(ActionEvent event) {

    }

    @FXML
    void logIn(ActionEvent event) {
    	boolean isValidFields = ValidatorHelper.hasValidationError(vboxLogin);
    	if(!isValidFields) {
        	User user = new User();
    		user.setAccountName(userNameField.getText());
    		user.setPassword(passwordField.getText());
    		user.setType(ClientMain.getClientProperty().getUserType());
    		
        	ClientMain.getClient().getWriter().println(JsonHelper.createJsonMsg("logIn", user, null));
        	ClientMain.getClient().getWriter().flush();
    	} else {
    		Notifications requestNotification = Notifications.create()
        			.title("Field validation error!")
        			.text("Please check that the fields have been correctly filled!")
        			.graphic(null)
        			.hideAfter(Duration.seconds(5))
        			.position(Pos.BOTTOM_RIGHT);
    		requestNotification.show();
    	}
    }
    
	/**
	 * Felhasználó bejelentkezésének eseményeit kezeő metódus, sikeres bejelentekzéskor elrejti a regsztráció és bejelentkezés menüpontokat a főmenüből
	 * és helyette egy profil ikont tesz ki, melynek segítségével a profilhoz tartozó műveleteket láthatjuk, mint például a kijelentkezés. Hibás bejelentkezéskor hibaüzenetet dob.
	 * @param user bejelentekzett felhasználó
	 * @param hasError	boolean értéket tartalmaz, igaz akkor, ha minden rendben történt a bejelentkezéskor, hamis ha nem, amennyiben hibás a bejelentkezés kidob egy hibaüzenetet.
	 */
	public void loginUser(User user, boolean hasError) {
		Platform.runLater(() -> {
			if (hasError || user == null || user.isDeleted()) {
				messageLabel.setTextFill(Color.web("#d62d20"));
				messageLabel.setFont(new Font("Arial", 11));
				messageLabel.setWrapText(true);
				messageLabel.setText("Wrong password or username. Try again!");
			} else if (UserType.GUEST.equals(user.getType())) {
				getHomeController().setVisibilityOfProfileControllers(false, true);
				getHomeController().setLoginController(null);
				ClientMain.setUser(user);
				Stage loginDialog = (Stage) loginPane.getScene().getWindow();
				loginDialog.close();
			} else if (UserType.WAITER.equals(user.getType()) && getHomeController().getWaiterSurfaceController() != null) {
				try {
					getHomeController().getWaiterSurfaceController().addWaiter(user);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}

    /**
     * Átmegy a regisztrációs oldalra és bezárja a bejelentkezés felületet.
     * @param event
     * @throws IOException 
     */
    @FXML
    void signUp(ActionEvent event) throws IOException {
    	Stage stage = (Stage) signUpLink.getScene().getWindow();
        stage.close();
    	
		Pane root = FXMLLoader.load(Loggin.class.getResource("/org/restaurant/client/gui/temp/common/Registration.fxml"));
		Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(loginPane.getScene().getWindow());
		//dialog.initOwner(parentStage);
		dialog.setTitle("Registration");
		dialog.setScene(new Scene(root));
		dialog.show();
    }
    
	public HomeController getHomeController() {
		return homeController;
	}

	public void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}

	public AdministratorSurfaceController getAdminController() {
		return adminController;
	}

	public void setAdminController(AdministratorSurfaceController adminController) {
		this.adminController = adminController;
	}
}
