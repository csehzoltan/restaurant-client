package org.restaurant.client.gui.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.gui.validators.FieldValidator;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.Unit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

public class IngredientSurfaceController {
	
	//Left content
    @FXML
    private BorderPane ingredientBP;
	
    @FXML
    private FlowPane vegetarianFlowPane;

    @FXML
    private FlowPane fruitsFlowPane;
    
    @FXML
    private FlowPane othersFlowPane;
    
    @FXML
    private JFXButton uploadIngredientImgBtn;
    
    @FXML
    private ImageView ingredientImageView;

	@FXML
    private JFXTextField idFld;

    @FXML
    private JFXTextField nameFld;

    @FXML
    private JFXTextField quantityFld;

    @FXML
    private JFXTextField alertLimitFld;

	@FXML
    private JFXComboBox<String> unitCB;
    
    @FXML
    private JFXTextField prodQuantityFld;

    @FXML
    private JFXComboBox<String> prodUnitCB;
    
    //Right content
    @FXML
    private ScrollPane productIngredientSP;

    @FXML
    private Label productIngredientTitle;

    @FXML
    private TableView<Ingredient> productIngredientTV;

    @FXML
    private TableColumn<Ingredient, String> ingredientNameCol;

    @FXML
    private TableColumn<Ingredient, Double> ingredientQuantityCol;

    @FXML
    private TableColumn<Ingredient, Unit> ingredientUnitCol;

    @FXML
    private JFXButton uploadProductIngredientBtn;

    @FXML
    private JFXButton deleteIngredientProductBtn;
    
    @FXML
    private JFXButton saveButton;
    
    private ProductTablePaneController parentController;

	private List<Ingredient> ingredients;
    
    private Set<Ingredient> uploadedIngredients;

	private List<IngredientItemController> ingredientItemControllers;
	
	private FieldValidator validator;
    
    private FileChooser fileChooser;
    
    private File file;
    
    private Product product;
    
    private Boolean isIngredientInitalized = false;

	public IngredientSurfaceController(Product product) {
    	this.product = product;
    }
    
	/**
	 * Kikéri a szervertől az összes feljegyzett összetevőt.
	 */
	public void initialize() {
		validator = new FieldValidator();
		initFields();
		fileChooser = new FileChooser();
		ingredientItemControllers = Collections.synchronizedList(new ArrayList<IngredientItemController>());
		ingredients = Collections.synchronizedList(new ArrayList<Ingredient>());
		uploadedIngredients = new HashSet<>();
		ObservableList<Ingredient> productIngredientItems = FXCollections.observableArrayList();
		productIngredientTV.setItems(productIngredientItems);
		getIngredientsFromServer();
	}

	private void getIngredientsFromServer() {
		if (ClientMain.getClient().getWriter() != null) {
			ClientMain.getClient().getWriter().println(JsonHelper.jsonActonRequest("getIngredients", null).toString());
			ClientMain.getClient().getWriter().flush();
		}
	}
	
	/**
	 * Baloldali blokkba betölti az összes összetevőt.
	 * Amennyiben van a termékhez összetevő betölti azokat egy jobboldalon elhelyezkedő táblázatban ábécé sorrendben.
	 */
	public void loadIngredients(List<Ingredient> ingredients) {
		Platform.runLater(() -> {
			try {
				setIngredients(ingredients);
				ingredients.sort(Comparator.comparing(Ingredient::getName));
				for (Ingredient ingredient : getIngredients()) {
					FXMLLoader loader = new FXMLLoader(getClass()
							.getResource("/org/restaurant/client/gui/temp/administrator/IngredientItem.fxml"));
					loader.setControllerFactory(c -> new IngredientItemController(ingredient));

					VBox ingredientItem = loader.load();
					IngredientItemController ingredientController = loader.getController();
					ingredientController.setParentController(this);
					ingredientItemControllers.add(ingredientController);
					
					
					vegetarianFlowPane.getChildren().add(ingredientItem);
				}

				if (product != null && product.getName() != null) {
					productIngredientTitle.setText(product.getName());

					ingredientNameCol.setCellValueFactory(new PropertyValueFactory<Ingredient, String>("name"));
					ingredientQuantityCol
							.setCellValueFactory(new PropertyValueFactory<Ingredient, Double>("quantity"));
					ingredientUnitCol.setCellValueFactory(new PropertyValueFactory<Ingredient, Unit>("unit"));

					ObservableList<Ingredient> productIngredientItems = FXCollections.observableArrayList();
					if(product.getIngredients() != null) {
						for (Ingredient ingredient : product.getIngredients()) {
							productIngredientItems.add(ingredient);
						}
					}

					if (productIngredientItems != null) {
						FXCollections.sort(productIngredientItems, (i1, i2) -> i1.getName().compareTo(i2.getName()));
						productIngredientTV.setItems(productIngredientItems);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	private void initFields() {
		idFld.setDisable(true);
		
		ObservableList<String> units = FXCollections.observableArrayList();
		for (Unit unit : Unit.values()) {
			units.add(unit.getValue());
		}
		unitCB.getItems().addAll(units);
		prodUnitCB.getItems().addAll(units);
	}
	
    @FXML
    void uploadIngredientImg(ActionEvent event) {
    	file = fileChooser.showOpenDialog(ingredientBP.getScene().getWindow());
		if(file != null) {
			ingredientImageView.setImage(new Image(file.toURI().toString()));
			ingredientImageView.setFitWidth(50);
			ingredientImageView.setFitHeight(50);
			ingredientImageView.setPreserveRatio(true);
		}
    }
    
    @FXML
    void uploadIngredient(ActionEvent event) {
 
		boolean isValid = false;
		if (validator.validateTf(nameFld, FieldValidator.ANYCHARACTERREGEXP, true, 3, 50, FieldValidator.MSGNAMERULE)
				&& validator.validateTf(quantityFld, FieldValidator.DECIMALREGEXP, true, 1, 10,
						FieldValidator.DECIMALRULE)
				&& validator.validateTf(alertLimitFld, FieldValidator.DECIMALREGEXP, false, 1, 10,
						FieldValidator.DECIMALRULE)) {
			isValid = true;
		}
		
		if (isValid && (ingredientImageView.getImage() == null || ingredientImageView.getImage().getWidth() > 200)) {
				AlertHelper.callAlert(AlertType.ERROR, "Image is missing or too big data! Width is bigger then 200");
				isValid = false;
		}
		boolean hasNameAlread = false;
		if(isValid) {
			hasNameAlread = checkNameNotExists();
		}
    	
    	if(!hasNameAlread && isValid && idFld.getText() != null && !idFld.getText().isEmpty()) {
    		for(IngredientItemController itemController : ingredientItemControllers) {
    			if(itemController.getIngredient().getId().equals(Integer.parseInt(idFld.getText()))) {
    				Ingredient ingredientToUpload = itemController.getIngredient();
    				ingredientToUpload.setName(nameFld.getText());
    				ingredientToUpload.setQuantity(quantityFld.getText() != null && !quantityFld.getText().isEmpty() ? Double.valueOf(quantityFld.getText().replaceAll(",", ".")) : null);
    				ingredientToUpload.setAlertLimit(alertLimitFld.getText() != null && !alertLimitFld.getText().isEmpty() ? Double.valueOf(alertLimitFld.getText().replaceAll(",", ".")) : null);
    				ingredientToUpload.setUnit(unitCB.getValue() != null && !unitCB.getValue().isEmpty() ? Unit.getByValue(unitCB.getValue()) : null);
    				BufferedImage bImage = SwingFXUtils.fromFXImage(ingredientImageView.getImage(), null);
    				ByteArrayOutputStream s = new ByteArrayOutputStream();
 
    				try {
						ImageIO.write(bImage, "png", s);
						byte[] res = s.toByteArray();
	    				s.close();
	    				
	    				ingredientToUpload.setIngredientImg(res);
    				} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				itemController.redraw();
    				uploadedIngredients.add(itemController.getIngredient());
    			}
    		}
		} else if (!hasNameAlread && isValid) {
			try {
					BufferedImage bImage = SwingFXUtils.fromFXImage(ingredientImageView.getImage(), null);
					ByteArrayOutputStream outStream = new ByteArrayOutputStream();
					ImageIO.write(bImage, "png", outStream);
					byte[] res = outStream.toByteArray();
	    			outStream.close();
	    			Ingredient ingredient = new Ingredient();
	    			ingredient.setIngredientImg(res);
    				ingredient.setName(nameFld.getText());
    				ingredient.setQuantity(Double.valueOf(quantityFld.getText()));
    				ingredient.setAlertLimit((alertLimitFld.getText() != null && !alertLimitFld.getText().isEmpty())
    						? Double.valueOf(alertLimitFld.getText().replaceAll(",", ".")) : null);
    				ingredient.setUnit((unitCB.getValue() != null && !unitCB.getValue().isEmpty()) ? Unit.getByValue(unitCB.getValue()) : null);
    				FXMLLoader loader = new FXMLLoader(
    						getClass().getResource("/org/restaurant/client/gui/temp/administrator/IngredientItem.fxml"));
    				loader.setControllerFactory(c -> new IngredientItemController(ingredient));
    				VBox ingredientItem;

    				ingredientItem = loader.load();

    				IngredientItemController ingredientController = loader.getController();
    				ingredientController.setParentController(this);
    				ingredientItemControllers.add(ingredientController);
    				vegetarianFlowPane.getChildren().add(ingredientItem);
    				uploadedIngredients.add(ingredient);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean checkNameNotExists() {
		boolean hasNameAlready = false;
		Iterator<IngredientItemController> crunchifyIterator = ingredientItemControllers.iterator();
		if(ingredientItemControllers != null && !ingredientItemControllers.isEmpty()) {
			while(crunchifyIterator.hasNext() && !hasNameAlready) {
				IngredientItemController itemController = crunchifyIterator.next();
				if ((idFld.getText()!=null && !idFld.getText().isEmpty())
						&& Integer.parseInt(idFld.getText()) != itemController.getIngredient().getId()
						&& nameFld.getText().trim().equalsIgnoreCase(itemController.getIngredient().getName().trim())) {
					AlertHelper.callAlert(AlertType.ERROR, "Ingredient name already exists!");
					hasNameAlready = true;
				}
				if ((idFld.getText() == null || idFld.getText().isEmpty()) && nameFld.getText().trim().equalsIgnoreCase(itemController.getIngredient().getName().trim())) {
					AlertHelper.callAlert(AlertType.ERROR, "Ingredient name already exists!");
					hasNameAlready = true;
				}
			}
		}
		return hasNameAlready;
	}
	
	public void uploadIngredientIds(List<Ingredient> newIngredients) {
		Platform.runLater(() -> {
			for(Ingredient uploadIngredient : newIngredients) {
				for(IngredientItemController itemController : ingredientItemControllers) {
					addIngredientIds(uploadIngredient, itemController);
				}
			}
		});
	}

	private void addIngredientIds(Ingredient uploadIngredient, IngredientItemController itemController) {
		if(itemController.getIngredient().getId() == null 
				&& itemController.getIngredient().getName().equalsIgnoreCase(uploadIngredient.getName())) {
			itemController.getIngredient().setId(uploadIngredient.getId());
			if(idFld.getText() != null && idFld.getText().isEmpty() && nameFld.getText().equals(uploadIngredient.getName())) {
				idFld.setText(String.valueOf(uploadIngredient.getId()));
			}
		}
	}
	

    /**
     * A termékhez hozzáadja a kiválasztott terméket. Sikertelen hozzáadás, módosítás esetén hibaüzenetet dob.
     * @param event
     */
    @FXML
    void uploadProductIngredient(ActionEvent event) {
    	boolean hasError = false;
    	Integer ingredientId = (idFld.getText() != null && !idFld.getText().isEmpty()) ? Integer.parseInt(idFld.getText()) : null;
    	
		boolean isValid = false;
		if (validator.validateTf(prodQuantityFld, FieldValidator.DECIMALREGEXP, true, 1, 10, FieldValidator.DECIMALRULE)) {
			isValid = true;
		}
    	
    	if(isValid && (prodQuantityFld.getText() == null || prodQuantityFld.getText().isEmpty())) {
    		AlertHelper.callAlert(AlertType.ERROR, "Quantity value missing!");
    		hasError = true;
    	} else if (ingredientId == null) {
    		AlertHelper.callAlert(AlertType.ERROR, "Select a saved ingredient!");
    		hasError = true;
    	}
    	
    	if(isValid && !hasError) {
    		uploadProductIngredientTable(ingredientId);
    	}
    }

	private void uploadProductIngredientTable(Integer ingredientId) {
		boolean isFoundIngredient = false;
		for(int i = 0; i < productIngredientTV.getItems().size(); i++) {
			if(productIngredientTV.getItems().get(i).getId().equals(ingredientId)) {
				setIngredientValuesFromLeftIngredientData(productIngredientTV.getItems().get(i));
				isFoundIngredient = true;
				break;
			}
		}
		if(!isFoundIngredient) {
			Ingredient ingredient = new Ingredient();
			setIngredientValuesFromLeftIngredientData(ingredient);
			productIngredientTV.getItems().addAll(ingredient);
		}	
		productIngredientTV.refresh();
	}

	/**
	 * Megadott összetevőnek beállítja az adatait az összetevők tábla alatt lévő mezőkből hivatkozva.
	 * @param ingredient Összetevő aminek az értékeit be szeretnénk állítani.
	 */
	private void setIngredientValuesFromLeftIngredientData(Ingredient ingredient) {
		ingredient.setId((idFld.getText() != null && !idFld.getText().isEmpty()) ? Integer.parseInt(idFld.getText()) : null);
		ingredient.setName(nameFld.getText());
		ingredient.setQuantity(prodQuantityFld.getText() != null ? Double.valueOf(prodQuantityFld.getText().replaceAll(",", ".")) : null);
		ingredient.setAlertLimit((alertLimitFld.getText() != null && !alertLimitFld.getText().isEmpty()) ?
				Double.valueOf(alertLimitFld.getText().replaceAll(",", ".")) : null);
		ingredient.setUnit(prodUnitCB.getValue() != null ? Unit.getByValue(prodUnitCB.getValue()) : null);
		ingredient.setDeleted(false);
	}
    
    /**
     * Elküldi az összes összetevőt, melyen módosítás történt.
     * @param event
     */
    @FXML
    void saveChanges(ActionEvent event) {
    	List<Ingredient> productIngredients = new ArrayList<>(productIngredientTV.getItems());
    	parentController.drawIngredients(productIngredients);
    	product.setIngredients(productIngredients);
    	List<Ingredient> modifiedIngredients = new ArrayList<>(uploadedIngredients);
    	JsonObject jsMsg = createJsonMsg(modifiedIngredients, "uploadIngredients");
    	if(ClientMain.getClient() != null) {
    		ClientMain.getClient().getWriter().println(jsMsg.toString());
    		ClientMain.getClient().getWriter().flush();
    	}
    }
    
    @FXML
    void eraseIngredientData(ActionEvent event) {
    	idFld.clear();
    	nameFld.clear();
    	quantityFld.clear();
    	unitCB.setValue(null);
    	ingredientImageView.setImage(null);
    }
    
    /**
     * Törli a hozzávalót, amennyiben az nincs más termék árában használatban.
     * @param event
     */
    @FXML
    void deleteIngredient(ActionEvent event) {
    	if(idFld.getText() != null && !idFld.getText().isEmpty()) {
    		Integer ingredientId = Integer.parseInt(idFld.getText());
    		boolean isUsedIngredient = false;
    		String usedInProduct = null;
    	    saveButton.setDisable(true);
    	    uploadProductIngredientBtn.setDisable(true);
    	    deleteIngredientProductBtn.setDisable(true);
    		for(Product prodElement : ClientMain.getProducts()) {
    			if(prodElement.getIngredients() != null) {
    				for(Ingredient prodIngredient : prodElement.getIngredients()) {
        				if(prodIngredient.getId() != null && prodIngredient.getId().equals(ingredientId)) {
        					isUsedIngredient = true;
        					usedInProduct = (prodElement.getName() != null) ? prodElement.getName() : "";
        					break;
        				}
        			}
    				if (isUsedIngredient) {
    					break;
    				}
    			}
    		}
    		
    		if(isUsedIngredient) {
    			AlertHelper.callAlert(AlertType.ERROR, "Already used in the " + usedInProduct + " product");
    		} else {
    			if(ingredientItemControllers != null) {
    				deleteIngredientFromIngredientsTable();
    			}
    			
    			removeIngredientFromProdIngredTable();
    			
    		}
    		saveButton.setDisable(false);
    		uploadProductIngredientBtn.setDisable(false);
    		deleteIngredientProductBtn.setDisable(false);
    	} else {
    		AlertHelper.callAlert(AlertType.ERROR, "Not found selected (saved) ingredient!");
    	}
    }

	private void deleteIngredientFromIngredientsTable() {
		for(IngredientItemController itemController : ingredientItemControllers) {
			if(itemController.getIngredient().getId().equals(Integer.parseInt(idFld.getText()))) {
				itemController.getIngredient().setDeleted(true);
				uploadedIngredients.add(itemController.getIngredient());
				vegetarianFlowPane.getChildren().remove(itemController.getIngredientItem());
			}
		}
	}

	private void removeIngredientFromProdIngredTable() {
		if(productIngredientTV.getItems() != null) {
			for(int i = 0; i < productIngredientTV.getItems().size(); i++) {
				if(productIngredientTV.getItems().get(i).getId().equals(Integer.parseInt(idFld.getText()))) {
					productIngredientTV.getItems().remove(i);
				}
			}
		}
	}

    @FXML
    void deleteIngredientFromProduct(ActionEvent event) {
    	if(productIngredientTV.getSelectionModel() != null && productIngredientTV.getSelectionModel().getSelectedItem() != null) {
    		productIngredientTV.getItems().remove(productIngredientTV.getSelectionModel().getSelectedItem());
        	productIngredientTV.refresh();
    	} else {
    		AlertHelper.callAlert(AlertType.ERROR, "Select item first from the table!");
    	}
    	
    }
    
    public ImageView getIngredientImageView() {
 		return ingredientImageView;
 	}

 	public JFXTextField getIdFld() {
 		return idFld;
 	}

 	public JFXTextField getNameFld() {
 		return nameFld;
 	}

 	public JFXTextField getQuantityFld() {
 		return quantityFld;
 	}

 	public JFXComboBox<String> getUnitCB() {
 		return unitCB;
 	}
 	
    public JFXTextField getAlertLimitFld() {
		return alertLimitFld;
	}

	public void setAlertLimitFld(JFXTextField alertLimitFld) {
		this.alertLimitFld = alertLimitFld;
	}
 
    public Boolean getIsIngredientInitalized() {
		return isIngredientInitalized;
	}

	public void setIsIngredientInitalized(Boolean isIngredientInitalized) {
		this.isIngredientInitalized = isIngredientInitalized;
	}
	
    public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setParentController(ProductTablePaneController parentController) {
		this.parentController = parentController;
	}
	
	//TODO create separate class for json msg create
	private JsonObject createJsonMsg(Object data, String actionType) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(data);
		JsonElement obj = new JsonParser().parse(json);
		
		JsonElement actionNameJsonElement = new JsonParser().parse(actionType);
		JsonObject sendMsg = new JsonObject();

		sendMsg.add("action", actionNameJsonElement);
		sendMsg.add("data", obj);
		
		return sendMsg;
		
	}
}
