package org.restaurant.client.gui.controllers;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restaurant.common.model.beans.User;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class WaiterSurfaceController {
    @FXML
    private VBox waitersVBox;

    @FXML
    private FlowPane waiterProfilesFlowPane;
    
    private List<WaiterProfileController> waiterProfiles;
    
    public void initialize() {
    	waiterProfiles = new ArrayList<>();
    	double paneWidth = 400.0;
    	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
    	int screenWidth = gd.getDisplayMode().getWidth();
    	
    	waiterProfilesFlowPane.setPrefWidth((double)screenWidth-200);
    	waiterProfilesFlowPane.setPrefWrapLength((screenWidth/paneWidth)-50);
    	waiterProfilesFlowPane.setHgap(20);
    	waiterProfilesFlowPane.setVgap(20);
    }
    
    public void addWaiter(User user) throws IOException {
    	
    	FXMLLoader loader = new FXMLLoader(
				getClass().getResource("/org/restaurant/client/gui/temp/waiter/WaiterProfile.fxml"));
    	loader.setControllerFactory(c ->  new WaiterProfileController(user));
		Pane waiterProfilePane = loader.load();
		WaiterProfileController waiterProfileController = loader.getController();
		waiterProfileController.setParentPane(waiterProfilesFlowPane);
		//waiterProfileController.setUser(user);
		waiterProfiles.add(waiterProfileController);
    	waiterProfilesFlowPane.getChildren().add(waiterProfilePane);
    }
    
    public List<WaiterProfileController> getWaiterProfiles() {
    	return waiterProfiles;
    }
}
