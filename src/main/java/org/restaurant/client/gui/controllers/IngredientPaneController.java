package org.restaurant.client.gui.controllers;

import org.restaurant.common.model.beans.Ingredient;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class IngredientPaneController {

    @FXML
    private Label ingredientLabel;
    
    @FXML
    private HBox ingredientHBox;

    @FXML
    private JFXButton deleteIngredientBtn;

    @FXML
    private FontAwesomeIconView deleteIngredient;
    
    private ProductTablePaneController parentController;
    
    private Ingredient ingredient;
    
    public IngredientPaneController(Ingredient ingredient) {
    	this.ingredient = ingredient;
    }
    
	/**
	 * Összetevők objektumhoz (this) segéd osztály hozzáadása, hogy a szülő tudjon hivatkozni erre az objektumra.
	 * (Például frissítéskor ki lehet törölni egyszerűen ezeket az elemeket és újrainicializálni)
	 */
	public void initialize() {
		ingredientLabel.setText(ingredient.getName());
		ingredientHBox.setId("ingredient_" + ingredient.getId());
		ingredientHBox.getStyleClass().add("ingredientClass");
    }

	
    /**
     * Szülő törlés metódusát hívja meg. (Törli magát a listából és a termék segéd objektumból)
     * @param event
     */
    @FXML
    void deleteIngredient(ActionEvent event) {
    	if(parentController != null) {
    		parentController.deleteIngredient(ingredient);
    	}
    }
    
	/**
	 * Szülő vezérlő beállítása.
	 * @param parentController szülő vezérlő.
	 */
	public void setParentController(ProductTablePaneController parentController) {
		this.parentController = parentController;
	}
}
