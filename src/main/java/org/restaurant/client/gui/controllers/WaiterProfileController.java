package org.restaurant.client.gui.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.restaurant.client.beans.WaiterTableType;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.Table;
import org.restaurant.common.model.beans.User;

import com.jfoenix.controls.JFXButton;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;

public class WaiterProfileController {
    @FXML
    private AnchorPane waiterProfile;

    @FXML
    private Label waiterNameLabel;

    @FXML
    private Label waiterContactLabel;

    @FXML
    private JFXButton waiterProfileButton;

    @FXML
    private FlowPane waiterFlowPane;
    
    private Pane parentPane;

	private User waiter;
    private List<WaiterTableController> tables = new ArrayList<>();
    
    public WaiterProfileController(User waiter) {
    	this.waiter = waiter;
    }
    
	public void initialize() throws IOException {
		if(getWaiter() != null) {
			waiterNameLabel.setText(getWaiter().getName());
	    	waiterContactLabel.setText(getWaiter().getPhoneNumber());
	    	addTables();
		}
    }
	
	/**
	 * Betölti a felszolgálóhoz tartozó asztalokat.
	 * @throws IOException
	 */
	private void addTables() throws IOException {
		Map<Table, ProductRequest> tableRequests = new LinkedHashMap<>();
		for(ProductRequest request : getWaiter().getProductRequests()) {
			tableRequests.put(request.getTable(), request);
		}
		
		for(Map.Entry<Table, ProductRequest> entry : tableRequests.entrySet()) {
			loadTable(entry.getKey(), entry.getValue(), WaiterTableType.GREEN);
		}
	}
	
	public void addTable(Table table, ProductRequest request, WaiterTableType tableType) {
		try {
			loadTable(table, request, tableType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadTable(Table table, ProductRequest request, WaiterTableType tableType) throws IOException {
		Platform.runLater(() -> {
			try {
				FXMLLoader loader = new FXMLLoader(
						getClass().getResource("/org/restaurant/client/gui/temp/waiter/WaiterTable.fxml"));

				loader.setControllerFactory(c -> new WaiterTableController(table, request));
				Parent newTable = loader.load();

				waiterFlowPane.getChildren().add(newTable);
				tables.add(loader.getController());

				WaiterTableController waiterTableController = loader.getController();
				waiterTableController.changeTableType(tableType);
				waiterTableController.setParentPane(waiterFlowPane);
				waiterTableController.setParentConroller(this);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	void closeWaiter(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Look, a Confirmation Dialog");
		alert.setContentText("Are you sure you want to exit?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			Map<String, String> infos = new HashMap<>();
			infos.put("waiterId", String.valueOf(waiter.getId()));
			if (ClientMain.getClient() != null) {
				ClientMain.getClient().getWriter()
						.println(JsonHelper.createJsonMsg("closeWaiter", null, infos));
				ClientMain.getClient().getWriter().flush();
			}
			parentPane.getChildren().remove(waiterProfile);
		}
	}

	public User getWaiter() {
		return waiter;
	}

	public void setWaiter(User waiter) {
		this.waiter = waiter;
	}

	public List<WaiterTableController> getTables() {
		return tables;
	}
	
    public Pane getParentPane() {
		return parentPane;
	}

	public void setParentPane(FlowPane parentPane) {
		this.parentPane = parentPane;
	}
}
