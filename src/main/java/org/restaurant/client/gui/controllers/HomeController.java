package org.restaurant.client.gui.controllers;

import java.io.IOException;

import org.controlsfx.control.Notifications;
import org.restaurant.client.gui.helper.NotificationHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.UserType;

import com.jfoenix.controls.JFXButton;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class HomeController {

    @FXML
    private Button homeButton;
    
    @FXML
    private ToggleButton homeMenuButton;

    @FXML
    private ToggleButton menuBtn;

    @FXML
    private ToggleButton guestbookBtn;
    
    @FXML
    private ToggleButton waiterOrderMenu;
	
    @FXML
    private BorderPane pane;
    
    @FXML
    private AnchorPane centerPane;
	
    @FXML
    private Hyperlink registrationLink;

    @FXML
    private JFXButton loginButton;
    
    @FXML
    private ImageView backgroundView;

    @FXML
    private MenuBar userMenuBar;
    
    private WaiterSurfaceController waiterSurfaceController;
    private LoginController loginController;
    private RegistrationController registrationController;
    private MenuController menuController;
    private GuestbookController guestbookController;

	private ClientMain clientMain;
    private UserType userType;
	
	public HomeController(UserType userType) {
		this.userType = userType;
	}
	
	/**
	 * A főmenüből leveszi a felhasználó típus szerinti szükségtelen menüket.
	 */
	@FXML
    public void initialize() {
		
		//stylesheet borderpane: gui.css/main.css
		pane.getStylesheets().add("gui.css/main.css");
		if(UserType.GUEST.equals(userType)) {
			waiterOrderMenu.setManaged(false);
			waiterOrderMenu.setVisible(false);
		} else if (UserType.WAITER.equals(userType)) {
			registrationLink.setManaged(false);
			registrationLink.setVisible(false);
			guestbookBtn.setManaged(false);
			guestbookBtn.setVisible(false);
			menuBtn.setManaged(false);
			menuBtn.setVisible(false);
		}
    	pane.getStylesheets().add("gui.css/main.css");
    	userMenuBar.setVisible(false);
    	
    	if(!ClientMain.getClient().isConnectedInTime()) {
			NotificationHelper nh = new NotificationHelper();
			nh.showNotification("Client connection time out!", Pos.BOTTOM_RIGHT, "Info");
		}
    }
    
	@FXML
	void login(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/common/Login.fxml"));
			loader.setControllerFactory(c ->  new LoginController(userType));
			Pane loginPane = loader.load();
			loginController = loader.getController();
			loginController.setHomeController(this);

			Stage dialog = new Stage();
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(pane.getScene().getWindow());
			dialog.setTitle("Login");
			dialog.setScene(new Scene(loginPane));
			dialog.centerOnScreen();
			dialog.setResizable(false);
			dialog.show();
		} catch (IOException e) {
			System.out.print("Registration problem: " + e + e.getMessage());
			e.printStackTrace();
		}
	}
    
    @FXML
    void logout(ActionEvent event) {
    	ClientMain.setUser(null);
    	registrationLink.setVisible(true);
    	registrationLink.setManaged(true);
    	loginButton.setVisible(true);
    	loginButton.setManaged(true);
		userMenuBar.setVisible(false);
		userMenuBar.setManaged(false);
		
		Notifications requestNotification = Notifications.create()
    			.title("Log out!")
    			.text("Successful logout!")
    			.graphic(null)
    			.hideAfter(Duration.seconds(5))
    			.position(Pos.BOTTOM_RIGHT);
		requestNotification.show();
    }

    @FXML
    void registration(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/common/Registration.fxml"));
			Pane root = loader.load();
			registrationController = loader.getController();
			registrationController.setHomeController(this);

			Stage dialog = new Stage();
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(pane.getScene().getWindow());
			dialog.setTitle("Registration");
			dialog.setScene(new Scene(root));
			dialog.centerOnScreen();
			dialog.setResizable(false);
			dialog.show();
		} catch (IOException e) {
			System.out.print("Registration problem: " + e);
			e.printStackTrace();
		}
	}
    
    @FXML
    void showMenu(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/guest/Menu.fxml"));
			Pane menuBox = loader.load();
			setMenuController(loader.getController());
			//Pane menuBox = FXMLLoader.load(getClass().getResource("/org/restaurant/client/gui/temp/guest/Menu.fxml"));
			pane.setCenter(menuBox);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void showWaiterOrder(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/waiter/WaiterSurface.fxml"));
    		Pane waitersBox = loader.load();
			setWaiterSurfaceController(loader.getController());
			pane.setCenter(waitersBox);
			waiterOrderMenu.setManaged(false);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    void showGuestbook(ActionEvent event) {
    	try {
    		FXMLLoader loader = new FXMLLoader(
    				getClass().getResource("/org/restaurant/client/gui/temp/guest/Guestbook.fxml"));
			Pane guestbook = loader.load();
			guestbookController = loader.getController();
			pane.setCenter(guestbook);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void setVisibilityOfProfileControllers(boolean showRegLogControlls, boolean showProfileButton) {
    	registrationLink.setVisible(showRegLogControlls);
    	registrationLink.setManaged(showRegLogControlls);
		loginButton.setVisible(showRegLogControlls);
		loginButton.setManaged(showRegLogControlls);
		userMenuBar.setVisible(showProfileButton);
		userMenuBar.setManaged(showProfileButton);
    }
    
    /**
     * Created to get the main pane for the dialogs.
     * @return Main parent pane.
     */
    public BorderPane getMainPane() {
    	return pane;
    }
    
    public WaiterSurfaceController getWaiterSurfaceController() {
		return waiterSurfaceController;
	}

	public void setWaiterSurfaceController(WaiterSurfaceController waiterSurfaceController) {
		this.waiterSurfaceController = waiterSurfaceController;
	}
	
    public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}
	
	public RegistrationController getRegistrationController() {
		return registrationController;
	}
	
	public void setRegistrationController(RegistrationController registrationController) {
		this.registrationController = registrationController;
	}
	
	public MenuController getMenuController() {
		return menuController;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}
	
	public GuestbookController getGuestbookController() {
		return guestbookController;
	}
	
	public final ClientMain getClientMain() {
		return clientMain;
	}

	public final void setClientMain(ClientMain clientMain) {
		this.clientMain = clientMain;
	}
	
	//TODO admin controller also contains this (duplication)
	public void showAlert(AlertType alertType, String msg) {
		Platform.runLater(() -> {
			Alert alert = new Alert(alertType);
			alert.setTitle("Message");
			alert.setHeaderText(null);
			alert.setContentText(msg);
			alert.showAndWait();
		});
	}
	
	public void showNotification(String msg) {
		Notifications requestNotification = Notifications.create()
    			.title("Message")
    			.text(msg)
    			.graphic(null)
    			.hideAfter(Duration.seconds(5))
    			.position(Pos.BOTTOM_RIGHT);
		requestNotification.show();
	}

}