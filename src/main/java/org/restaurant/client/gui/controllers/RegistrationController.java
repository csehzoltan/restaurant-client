package org.restaurant.client.gui.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.gui.helper.NotificationHelper;
import org.restaurant.client.gui.validators.FieldValidator;
import org.restaurant.client.gui.validators.ValidatorHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class RegistrationController {

    @FXML
    private Pane registrationPane;
	
    @FXML
    private VBox inputFieldsVBox;
	
    @FXML
    private JFXTextField userNameField;

    @FXML
    private JFXTextField emailField;

    @FXML
    private JFXPasswordField pwField;

    @FXML
    private JFXPasswordField retypePwField;

    @FXML
    private JFXDatePicker dateOfBirthPicker;
    
    @FXML
    private HBox birthPickerHBox;

    @FXML
    private JFXButton signUpBtn;

    @FXML
    private Hyperlink logInLink;
    
    @FXML
    private ImageView userIcon;
    
    private HomeController homeController;

	@FXML
	public void initialize() {
		inputFieldsVBox.getStylesheets().add("gui.css/main.css");
		
		initImages();
		
		ValidatorHelper.addValidatorToPaneFields(inputFieldsVBox);
		dateOfBirthPicker.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					if (dateOfBirthPicker.getEditor().getText() == null
							|| dateOfBirthPicker.getEditor().getText().isEmpty()
							|| dateOfBirthPicker.getEditor().getText() != null
									&& dateOfBirthPicker.getValue() == null) {
						birthPickerHBox.setVisible(true);
					} else {
						birthPickerHBox.setVisible(false);
					}
				}
			}
		});
		birthPickerHBox.setVisible(false);
	}

	private void initImages() {
		userIcon.setImage(new Image(getClass().getResource("/gui.img/user-icon.png").toExternalForm()));
	}
    
    /**
     * Betölti a bejelentkezési felületet.
     * @param event
     * @throws IOException
     */
    @FXML
    void logIn(ActionEvent event) throws IOException {
    	Stage stage = (Stage) logInLink.getScene().getWindow();
        stage.close();
    	
		Pane root = FXMLLoader.load(RegistrationController.class.getResource("/org/restaurant/client/gui/temp/common/Login.fxml"));
		Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		//dialog.initOwner(parentStage);
		dialog.setTitle("Registration");
		dialog.setScene(new Scene(root));
		dialog.show();
    }

    /**
     * Elküldi a regisztrációs szándékot a szervernek, amennyiben nem talál helytelenül kitöltött mezőt, melyeket a validálási
     * feltételben megadtunk.
     * @param event
     */
    @FXML
	void signUp(ActionEvent event) {

		boolean hasValidationError = ValidatorHelper.hasValidationError(inputFieldsVBox);

		if (dateOfBirthPicker.getValue() == null || dateOfBirthPicker.getValue().toString().isEmpty()) {
			hasValidationError = true;
			if (!birthPickerHBox.isVisible()) {
				birthPickerHBox.setVisible(true);
			}
		}
		
		if(!hasValidationError) {
			hasValidationError = validateSaveFields();
		}
		
		hasValidationError = pwEqualsCheck(hasValidationError);

		if (hasValidationError) {
			Notifications requestNotification = Notifications.create().title("Field validation error!")
					.text("Please check that the fields have been correctly filled!").graphic(null)
					.hideAfter(Duration.seconds(5)).position(Pos.BOTTOM_RIGHT);
			requestNotification.show();
		} else {
			User user = new User();
			user.setAccountName(userNameField.getText());
			user.setPassword(pwField.getText());

			LocalDate localDate = dateOfBirthPicker.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date dateOfBirth = Date.from(instant);
			//user.setBirthDay(date);
			user.setEmail(emailField.getText());
			user.setType(UserType.GUEST);
			
			if(ClientMain.getClient() != null) {
				PrintWriter clientWriter = ClientMain.getClient().getWriter();
				Map<String, String> infos = new HashMap<>();
				infos.put("dateOfBirth", new SimpleDateFormat("yyyymmdd").format(dateOfBirth));
				clientWriter.println(JsonHelper.createJsonMsg("registration", user, infos));
				clientWriter.flush();
			}

			new NotificationHelper().showNotification("Request registration successfully send!", Pos.BOTTOM_RIGHT, "Registration");
		}
	}

	private boolean pwEqualsCheck(boolean hasValidationError) {
		if(!hasValidationError && pwField.getText()!=null && retypePwField.getText()!=null
				 && !pwField.getText().equals(retypePwField.getText())) {
				hasValidationError = true;
				AlertHelper.callAlert(AlertType.INFORMATION, "Password not same!");
		}
		return hasValidationError;
	}
    
	private boolean validateSaveFields() {
		boolean isValid = true;
		FieldValidator validator = new FieldValidator();
		if (validator.validateTf(userNameField, FieldValidator.NAMEREGEXP, false, 5, 50, FieldValidator.MSGNAMERULE)
				&& validator.validateTf(emailField, FieldValidator.EMAILREGEXP, true, 3, 50, "")
				&& validator.validatePwf(pwField, FieldValidator.PASSREGEXP, true, 3, 50, FieldValidator.MSGPASSRULE)) {
			isValid = false;
		}
		return isValid;
	}
    
	/**
	 * Felhasználó bejelentkezésének eseményeit kezeő metódus, sikeres bejelentekzéskor elrejti a regsztráció és bejelentkezés menüpontokat a főmenüből
	 * és helyette egy profil ikont tesz ki, melynek segítségével a profilhoz tartozó műveleteket láthatjuk, mint például a kijelentkezés. Hibás bejelentkezéskor hibaüzenetet dob.
	 * @param user bejelentekzett felhasználó
	 * @param hasError	boolean értéket tartalmaz, igaz akkor, ha minden rendben történt a bejelentkezéskor, hamis ha nem, amennyiben hibás a bejelentkezés kidob egy hibaüzenetet.
	 */
	public void loginUser(User user, boolean hasError) {
		Platform.runLater(() -> {
			 if (UserType.GUEST.equals(user.getType())) {
				getHomeController().setVisibilityOfProfileControllers(false, true);
				getHomeController().setRegistrationController(null);
				ClientMain.setUser(user);
				Stage registrationDialog = (Stage) registrationPane.getScene().getWindow();
				registrationDialog.close();
				
				new NotificationHelper().showNotification("Successful registration", Pos.BOTTOM_RIGHT, "Registration");
			}
		});
	}

	/**
	 * Visszatér a gyökér kontrollerrel.
	 * @return visszatér a gyökér kontrollerrel.
	 */
	public final HomeController getHomeController() {
		return homeController;
	}

	/**
	 * Beállítja a gyökér kontrollert.
	 * @param homeController beállítja a gyökér kontrollert.
	 */
	public final void setHomeController(HomeController homeController) {
		this.homeController = homeController;
	}
}
