package org.restaurant.client.gui.controllers;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.Optional;

import org.restaurant.client.gui.helper.NotificationHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdministratorSurfaceController {
	
    @FXML
    private FontAwesomeIconView logGlyphy;
	
    @FXML
    private AnchorPane mainWindow;
    
    @FXML
    private JFXButton alertMenu;
    
    @FXML
    private ScrollPane scrollWindow;

	@FXML
	private AnchorPane mainContentPane;
	
	private UserTablePaneController userPaneController;
	
	private ProductTablePaneController productTablePaneController;
	
	private GuestbookPaneController guestbookPaneController;
	
	private StatisticsController statisticsController;
	
	private IngredientAlertsController ingredientAlertsController;

	private LoginController loginController;
	
	private Stage loginStage;
	
    @FXML
    private Label userNameLbl;
	
    @FXML
    private JFXButton userMenu;

    @FXML
    private JFXButton productMenu;

    @FXML
    private JFXButton satisticsMenu;

    @FXML
    private JFXButton guestbookMenu;
	
	private boolean loggedIn;

	/**
	 * Menüket inaktávlja, és beállítja a képernyő szélességét, amennyiben szükséges görgethetővé teszi az ablakot.
	 */
	@FXML
	public void initialize() {
		logoutSettings();
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		mainWindow.setPrefWidth(gd.getDisplayMode().getWidth()-20);
		scrollWindow.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scrollWindow.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		//scrollWindow.setHmax(0);
		scrollWindow.setFitToHeight(true);
		scrollWindow.setFitToWidth(true);
		
		if(!ClientMain.getClient().isConnectedInTime()) {
			NotificationHelper nh = new NotificationHelper();
			nh.showNotification("Client connection time out!", Pos.BOTTOM_RIGHT, "Info");
		}
		
	}
	
    @FXML
    void logAction(ActionEvent event) {
		if (!loggedIn) {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/common/Login.fxml"));
				
				loader.setControllerFactory(c -> new LoginController(UserType.ADMINISTRATOR));
				Pane loginPane = loader.load();
				loginController = loader.getController();
				loginController.setAdminController(this);

				loginStage = new Stage();
				loginStage.initModality(Modality.APPLICATION_MODAL);
				loginStage.initOwner(scrollWindow.getScene().getWindow());
				loginStage.setTitle("Login");
				loginStage.setScene(new Scene(loginPane));
				loginStage.centerOnScreen();
				loginStage.setResizable(false);
				loginStage.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Log out");
			alert.setHeaderText("Log out");
			alert.setContentText("Are you sure you want to logout?");

			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK){
				logoutSettings();
				mainContentPane.getChildren().clear();
			}
		}
    }

	private void logoutSettings() {
		loggedIn = false;
		userMenu.setDisable(true);
		alertMenu.setDisable(true);
		productMenu.setDisable(true);
		satisticsMenu.setDisable(true);
		guestbookMenu.setDisable(true);
		userNameLbl.setText("Log In");
		logGlyphy.setGlyphName("SIGN_IN");
	}
    
	/**
	 * Felhasználó bejelentkezésének eseményeit kezeő metódus, sikeres bejelentekzéskor elrejti a regsztráció és bejelentkezés menüpontokat a főmenüből
	 * és helyette egy profil ikont tesz ki, melynek segítségével a profilhoz tartozó műveleteket láthatjuk, mint például a kijelentkezés. Hibás bejelentkezéskor hibaüzenetet dob.
	 * @param user bejelentekzett felhasználó
	 * @param hasError	boolean értéket tartalmaz, igaz akkor, ha minden rendben történt a bejelentkezéskor, hamis ha nem, amennyiben hibás a bejelentkezés kidob egy hibaüzenetet.
	 */
	public void loginUser(User user, boolean hasError) {
		Platform.runLater(() -> {
			if (!hasError && user != null && user.getId() != null && UserType.ADMINISTRATOR.equals(user.getType())) {
				loggedIn = true;
				userMenu.setDisable(false);
				alertMenu.setDisable(false);
				productMenu.setDisable(false);
				satisticsMenu.setDisable(false);
				guestbookMenu.setDisable(false);
				if(loginStage != null) {
					loginStage.close();
				}
				ClientMain.setUser(user);
				logGlyphy.setGlyphName("SIGN_OUT");
				userNameLbl.setText(user.getName() != null ? user.getName() : "");
			} else {
				new NotificationHelper().showNotification("Wrong login data!", Pos.BOTTOM_RIGHT, "ERROR");
			}

		});
	}

	@FXML
	void loadProducts(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/ProductTablePane.fxml"));

			loader.setControllerFactory(c -> new ProductTablePaneController(ClientMain.getProducts()));
			BorderPane productBp = loader.load();
			productTablePaneController = loader.getController();

			if (!mainContentPane.getChildren().isEmpty()) {
				mainContentPane.getChildren().clear();
			}
			mainContentPane.getChildren().add(productBp);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	void loadGuestBooks(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/GuestbookPane.fxml"));
			
			loader.setControllerFactory(c ->  new GuestbookPaneController());
			Pane guestbookPane = loader.load();
			guestbookPaneController = loader.getController();
			if(!mainContentPane.getChildren().isEmpty()) {
				mainContentPane.getChildren().clear();
			}
			mainContentPane.getChildren().add(guestbookPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Betölti a főfelületre a fogyó összetevőket tartalmazó felületet.
	 * @param event
	 */
	@FXML
	void loadAlert(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/IngredientLimit.fxml"));
			
			loader.setControllerFactory(c ->  new IngredientAlertsController());
			Pane alertPane = loader.load();
			ingredientAlertsController = loader.getController();
			if(!mainContentPane.getChildren().isEmpty()) {
				mainContentPane.getChildren().clear();
			}

			mainContentPane.getChildren().add(alertPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	void loadStatistics(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/Statistics.fxml"));
			
			Pane statisticsPane = loader.load();
			this.statisticsController = loader.getController();
			if(!mainContentPane.getChildren().isEmpty()) {
				mainContentPane.getChildren().clear();
			}

			mainContentPane.getChildren().add(statisticsPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Betölti a felhasználók felületet.
	 * @param event
	 */
	@FXML
	void loadUsers(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/org/restaurant/client/gui/temp/administrator/UserTablePane.fxml"));
			loader.setControllerFactory(c ->  new UserTablePaneController(ClientMain.getUsers()));
			BorderPane userBp = loader.load();
			this.userPaneController = loader.getController();
			
			if(!mainContentPane.getChildren().isEmpty()) {
				mainContentPane.getChildren().clear();
			}
			mainContentPane.getChildren().add(userBp);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showAlert(AlertType alertType, String msg) {
		Platform.runLater(() -> {
			Alert alert = new Alert(alertType);
			alert.setTitle("Validation Error");
			alert.setHeaderText(null);
			alert.setContentText(msg);
			alert.showAndWait();
		});
	}
	
	public GuestbookPaneController getGuestbookPaneController() {
		return guestbookPaneController;
	}

	public UserTablePaneController getUserPaneController() {
		return userPaneController;
	}

	public void setUserPaneController(UserTablePaneController userPaneController) {
		this.userPaneController = userPaneController;
	}
	
	public ProductTablePaneController getProductTablePaneController() {
		return productTablePaneController;
	}

	public void setProductTablePaneController(ProductTablePaneController productTablePaneController) {
		this.productTablePaneController = productTablePaneController;
	}
	
	public StatisticsController getStatisticsController() {
		return statisticsController;
	}
	
	public IngredientAlertsController getIngredientAlertsController() {
		return ingredientAlertsController;
	}

	public void setIngredientAlertsController(IngredientAlertsController ingredientAlertsController) {
		this.ingredientAlertsController = ingredientAlertsController;
	}
}
