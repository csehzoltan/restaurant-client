package org.restaurant.client.gui.controllers;

import java.text.ParseException;
import java.util.Locale;

import org.controlsfx.control.Rating;
import org.restaurant.common.helpers.DateHelper;
import org.restaurant.common.helpers.NumberHelper;
import org.restaurant.common.model.beans.Guestbook;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class GuestbookItemController {

    @FXML
    private Label nameLbl;

    @FXML
    private TextArea remarkTA;

    @FXML
    private Rating foodRating;

    @FXML
    private Rating priceRating;

    @FXML
    private Rating staffRating;

    @FXML
    private Label dateLbl;
    
    private Guestbook guestbook;
    
    public GuestbookItemController(Guestbook guestbook) {
    	this.guestbook = guestbook;
    }
    
    public void initialize() {
    	nameLbl.setText((guestbook.getUser() != null && guestbook.getUser().getAccountName() != null) ? guestbook.getUser().getAccountName() : "Anonymous");
		remarkTA.setText(guestbook.getRemark());
		
		foodRating.setRating(NumberHelper.parseDoubleParam(guestbook.getFoodRating(), 0.0));
		priceRating.setRating(NumberHelper.parseDoubleParam(guestbook.getPriceRating(), 0.0));
		staffRating.setRating(NumberHelper.parseDoubleParam(guestbook.getStaffRating(), 0.0));
		
		
		try {
			dateLbl.setText(DateHelper.format(guestbook.getArrivedAt().toString(), "EEE MMM dd HH:mm:ss zzz yyyy", "yyyy-MM-dd HH:mm:ss", Locale.US));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 

}
