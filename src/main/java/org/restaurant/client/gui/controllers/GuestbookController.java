package org.restaurant.client.gui.controllers;

import org.controlsfx.control.Notifications;
import org.controlsfx.control.Rating;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Guestbook;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class GuestbookController {

    @FXML
    private Label characterNumberLbl;
	
    @FXML
    private TextArea remarkTA;
    
    @FXML
    private StackPane guestbookSP;

    @FXML
    private Rating foodRating;

    @FXML
    private Rating pricesRating;

    @FXML
    private Rating staffRating;
    
    @FXML
    private JFXButton sendButton;
    
    @FXML
    private FontAwesomeIconView foodFAIV;

    @FXML
    private FontAwesomeIconView priceFAIV;

    @FXML
    private FontAwesomeIconView staffFAIV;
    
    @FXML
    public void initialize() {
    	remarkTA.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> 
    	characterNumberLbl.setText(remarkTA.getText() != null ? String.valueOf(remarkTA.getText().length()) : "0"));
    }
    
    @FXML
    void sendGuestbookData(ActionEvent event) {
    	if(remarkTA.getText().isEmpty() && foodRating.getRating() == 0 && pricesRating.getRating() == 0 && staffRating.getRating() == 0) {			
			Notifications requestNotification = Notifications.create()
        			.title("Warning")
        			.text("At least one field must be filled out!")
        			.graphic(null)
        			.hideAfter(Duration.seconds(5))
        			.position(Pos.BOTTOM_RIGHT);
    		requestNotification.show();
    	} else {
    		Guestbook guestbook = new Guestbook();
    		guestbook.setRemark(remarkTA.getText());
    		guestbook.setFoodRating((int) foodRating.getRating());
    		guestbook.setPriceRating((int) pricesRating.getRating());
    		guestbook.setStaffRating((int) staffRating.getRating());
    		guestbook.setUser(ClientMain.getUser());
    		
    		if(ClientMain.getClient() != null) {
    			ClientMain.getClient().getWriter().println(JsonHelper.createJsonMsg("saveGuestbook", guestbook, null));
    			ClientMain.getClient().getWriter().flush();
    		}
    	}
    }
    
    /**
     * A vendégkönyv összes mezőjére beállítja, hogy módosítható legyen vagy sem.
     * @param isDisabled Mezők módosításának letiltása, ha true egyébkont szerkeszthetők lesznek.
     */
	public void disableAllFields(boolean isDisabled) {
		guestbookSP.setDisable(isDisabled);
		
		double opacity = isDisabled ? 0.5 : 1.0;
		
		foodFAIV.setOpacity(opacity);
		priceFAIV.setOpacity(opacity);
		staffFAIV.setOpacity(opacity);
	}

}
