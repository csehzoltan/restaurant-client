package org.restaurant.client.gui.controllers;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.gui.validators.FieldValidator;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class UserTablePaneController {

	@FXML
	private TableView<User> userTable;

	@FXML
	private TableColumn<User, Integer> colId;

	@FXML
	private TableColumn<User, String> colName;

	@FXML
	private TableColumn<User, String> colAccount;

	@FXML
	private TableColumn<User, UserType> colType;

	@FXML
	private TableColumn<User, String> colEmail;

	@FXML
	private TableColumn<User, String> colPhone;

	@FXML
	private TableColumn<User, Date> colBirthDate;
	
	@FXML
	private TableColumn<User, Date> colRegDate;
	
    @FXML
    private TableColumn<User, Boolean> colDelUser;

	@FXML
	private JFXTextField idFld;

	@FXML
	private JFXTextField nameFld;

	@FXML
	private JFXTextField accountFld;

	@FXML
	private JFXPasswordField passwordFld;

	@FXML
	private JFXComboBox<String> typeFld;

	@FXML
	private JFXTextField emailFld;

	@FXML
	private JFXTextField phoneNumberFld;

	@FXML
	private JFXDatePicker birthdayPicker;

    @FXML
    private JFXCheckBox deletedUserCB;

	@FXML
	private JFXButton saveButton;

	@FXML
	private JFXButton deleteButton;

	private FieldValidator validator;

	private List<User> users;

	public UserTablePaneController(List<User> users) {
		this.users = users;
	}

	/**
	 * Mezőkre elhelyezi a validátorokat, majd a felhasználók táblát feltölti a felhasználók adataival.
	 * Végül a felhasználó típusának módosításáért felelős legördülő menüt feltölti adatokkal.
	 */
	@FXML
	public void initialize() {
		validator = new FieldValidator();
		initUserTable();
		initFields();
	}

	/**
	 * Initalize table properties, set constrained resize policy for ignore
	 * scrollbar, because it is in a scroll pane initalize columns, load users into
	 * the table.
	 */
	private void initUserTable() {
		userTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		idFld.setDisable(true);
		colId.setCellValueFactory(new PropertyValueFactory<User, Integer>("id"));
		colName.setCellValueFactory(new PropertyValueFactory<User, String>("name"));
		
		colName.setCellFactory(tc -> {
		    TableCell<User, String> cell = new TableCell<>();
		    Text text = new Text();
		    cell.setGraphic(text);
		    cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
		    text.wrappingWidthProperty().bind(colName.widthProperty());
		    text.textProperty().bind(cell.itemProperty());
		    return cell ;
		});
		
		colAccount.setCellValueFactory(new PropertyValueFactory<User, String>("accountName"));
		
		colAccount.setCellFactory(tc -> {
		    TableCell<User, String> cell = new TableCell<>();
		    Text text = new Text();
		    cell.setGraphic(text);
		    cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
		    text.wrappingWidthProperty().bind(colAccount.widthProperty());
		    text.textProperty().bind(cell.itemProperty());
		    return cell ;
		});
		
		colType.setCellValueFactory(new PropertyValueFactory<User, UserType>("type"));
		colEmail.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
		colPhone.setCellValueFactory(new PropertyValueFactory<User, String>("phoneNumber"));
		colBirthDate.setCellValueFactory(new PropertyValueFactory<User, Date>("birthDay"));
		colBirthDateFormating();
		colRegDate.setCellValueFactory(new PropertyValueFactory<User, Date>("registrationDate"));
		colRegDateFormating();
		colDelUser.setCellValueFactory(new PropertyValueFactory<User, Boolean>("deleted"));

		TableColumn<User, Boolean> colEdit = new TableColumn<>("Act.");
		colEdit.setMinWidth(55.0);
		colEdit.setPrefWidth(55.0);
		colEdit.setMaxWidth(55.0);
		userTable.getColumns().add(colEdit);
		colEdit.setCellValueFactory(p -> new SimpleBooleanProperty(p.getValue() != null));
		colEdit.setCellFactory(p -> new ButtonCell());

		ObservableList<User> userTableItems = FXCollections.observableArrayList();
		for (User user : ClientMain.getUsers()) {
			userTableItems.add(user);
		}

		if (userTableItems != null) {
			userTable.setItems(userTableItems);
		}
	}

	/**
	 * Felhasználók típusai módosító mezőt feltölti adattal.
	 */
	private void initFields() {
		ObservableList<String> userTypes = FXCollections.observableArrayList();
		for (UserType type : UserType.values()) {
			userTypes.add(type.getDisplay() != null ? type.getDisplay().toUpperCase() : null);
		}
		typeFld.getItems().addAll(userTypes);
	}

	/**
	 * Elküldi a szervernek a regisztrálandó felhasználót.
	 * @param event
	 */
	@FXML
	void saveUser(ActionEvent event) {
		if (validateSaveFields()) {
			User saveUser = new User();
			saveUser.setId(idFld.getText().isEmpty() ? null : Integer.valueOf(idFld.getText()));
			saveUser.setName((nameFld.getText() == null || nameFld.getText().isEmpty()) ? null : nameFld.getText());
			saveUser.setAccountName(accountFld.getText());
			saveUser.setBirthDay(Date.from(birthdayPicker.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
			saveUser.setEmail(emailFld.getText());
			saveUser.setType(UserType.getByValue(typeFld.getValue()));
			saveUser.setPassword(passwordFld.getText());
			saveUser.setPhoneNumber((phoneNumberFld.getText() == null || phoneNumberFld.getText().isEmpty()) ? null
					: phoneNumberFld.getText());
			saveUser.setDeleted(deletedUserCB.isSelected());

			if (ClientMain.getClient() != null) {
				ClientMain.getClient().getWriter().println(JsonHelper.createJsonMsg("registrationByAdmin", saveUser, null));
				ClientMain.getClient().getWriter().flush();
			}
		}
	}
	
	/**
	 * Frissíti a felhasználók táblát, amennyiben beszúrás van, akkor hozzácsatolja a táblázathoz, különben frissíti a létező rekordot.
	 * @param user felhasználó.
	 * @param isInsert beszúr a táblába új rekordként, vagy frissít meglévő adatot (id alapú felismerés).
	 */
	public void updateTable(User user, boolean isInsert) {
		Platform.runLater(() -> {
			if (isInsert) {
				userTable.getItems().add(user);
			} else {
				for (User userItem : userTable.getItems()) {
					if (userItem.getId().equals(user.getId())) {
						userItem.setId(user.getId());
						userItem.setAccountName(user.getAccountName());
						userItem.setBirthDay(user.getBirthDay());
						userItem.setDeleted(user.isDeleted());
						userItem.setEmail(user.getEmail());
						userItem.setName(user.getName());
						userItem.setPassword(user.getPassword());
						userItem.setPhoneNumber(user.getPhoneNumber());
						userItem.setRegistrationDate(user.getRegistrationDate());
						userItem.setType(user.getType());
						userItem.setDeleted(user.isDeleted());
					}
				}
			}
			userTable.refresh();
		});
	}

	/**
	 * Validálást futtat le a mezőkön, amennyiben hibás adatot talál hibaüzenetet dob.
	 * @return true értékkel tér vissza, ha a validálás sikeres, különben false successful else false.
	 */
	private boolean validateSaveFields() {
		boolean isValid = false;
		if(validator.validateTf(accountFld, FieldValidator.ACCOUNTREGEXP, true, 3, 50, FieldValidator.MSGACCOUNTRULE) && 
				validator.validatePwf(passwordFld, FieldValidator.PASSREGEXP, true, 3, 50, FieldValidator.MSGPASSRULE) &&
				validator.validateComboBoxRequired(typeFld) &&
				validator.validateTf(emailFld, FieldValidator.EMAILREGEXP, true, 3, 50, "") &&
				validator.validateTf(phoneNumberFld, FieldValidator.PHONENUMBERREGEXP, false, null, 30, "") &&
				validator.validateDatePickerRequired(birthdayPicker) &&
				validator.validateTf(nameFld, FieldValidator.NAMEREGEXP, false, 5, 50, FieldValidator.MSGNAMERULE)
				) {
			isValid = true;
		}
		return isValid;
	}
	
	/**
	 * Dátumok a táblában idővel jelennének meg, ezért még kell rájuk formázás, adatbázisban is csak év, hónap, nap szerepel.
	 */
	private void colBirthDateFormating() {
		SimpleDateFormat myDateFormatter = new SimpleDateFormat("yyyy.MM.dd");
		colBirthDate.setCellFactory(column -> {
			return new TableCell<User, Date>() {
				protected void updateItem(Date item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
					} else {
						setText(myDateFormatter.format(item));
					}
				}
			};
		});
	}
	
	/**
	 * Dátumok a táblában idővel jelennének meg, ezért még kell rájuk formázás, adatbázisban is csak év, hónap, nap szerepel.
	 */
	private void colRegDateFormating() {
		SimpleDateFormat myDateFormatter = new SimpleDateFormat("yyyy.MM.dd");
		colRegDate.setCellFactory(column -> {
			return new TableCell<User, Date>() {
				protected void updateItem(Date item, boolean empty) {
					super.updateItem(item, empty);

					if (item == null || empty) {
						setText(null);
					} else {
						setText(myDateFormatter.format(item));
					}
				}
			};
		});
	}

    /**
     * A felhasználói adatok beviteli mezőit kitörli (azonosító, név, felhasználónév, jelszó, típus, email, telefonszám, születésnap).
     */
    @FXML
    void eraseProductData(ActionEvent event) {
    	idFld.clear();
    	nameFld.clear();
    	accountFld.clear();
    	passwordFld.clear();
    	typeFld.setValue(null);
    	emailFld.clear();
    	phoneNumberFld.clear();
    	birthdayPicker.setValue(null);
    	deletedUserCB.setSelected(false);
    }

	public TableView<User> getUserTable() {
		return userTable;
	}

	public void setUserTable(TableView<User> userTable) {
		this.userTable = userTable;
	}

	public TableColumn<User, String> getColName() {
		return colName;
	}

	public void setColName(TableColumn<User, String> colName) {
		this.colName = colName;
	}

	public TableColumn<User, String> getColAccount() {
		return colAccount;
	}

	public void setColAccount(TableColumn<User, String> colAccount) {
		this.colAccount = colAccount;
	}

	public TableColumn<User, UserType> getColType() {
		return colType;
	}

	public void setColType(TableColumn<User, UserType> colType) {
		this.colType = colType;
	}

	public TableColumn<User, String> getColEmail() {
		return colEmail;
	}

	public void setColEmail(TableColumn<User, String> colEmail) {
		this.colEmail = colEmail;
	}

	public TableColumn<User, String> getColPhone() {
		return colPhone;
	}

	public void setColPhone(TableColumn<User, String> colPhone) {
		this.colPhone = colPhone;
	}

	public TableColumn<User, Date> getColBirthDate() {
		return colBirthDate;
	}

	public void setColBirthDate(TableColumn<User, Date> colBirthDate) {
		this.colBirthDate = colBirthDate;
	}

	public JFXTextField getNameField() {
		return nameFld;
	}

	public void setNameField(JFXTextField nameField) {
		this.nameFld = nameField;
	}

	public JFXTextField getAccountField() {
		return accountFld;
	}

	public void setAccountField(JFXTextField accountField) {
		this.accountFld = accountField;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	private class ButtonCell extends TableCell<User, Boolean> {
		Button cellButton = new Button("Edit");

		/**
		 * Button text set null because it add dots if not set it.
		 */
		ButtonCell() {
			
			Image imgEdit = new Image(getClass().getResource("/gui.icons/edit.png").toExternalForm());
			ImageView iv = new ImageView();
			iv.setFitWidth(20);
			iv.setFitHeight(20);
			iv.setImage(imgEdit);
			iv.setPreserveRatio(true);
			iv.setSmooth(true);
			iv.setCache(true);
			cellButton.setText(null);
			cellButton.setStyle("-fx-background-color: transparent;");
			cellButton.setGraphic(iv);
			setAlignment(Pos.CENTER);
			cellButton.setOnAction(t -> {
				Integer editIndex = ButtonCell.this.getIndex();
				userTable.getSelectionModel().select(editIndex);
				User user = ButtonCell.this.getTableView().getItems().get(editIndex);
				idFld.setText(user.getId().toString());
				accountFld.setText(user.getAccountName());
				birthdayPicker.setValue(user.getBirthDay() != null
						? user.getBirthDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
						: null);
				passwordFld.setText(user.getPassword());
				emailFld.setText(user.getEmail());
				nameFld.setText(user.getName());
				phoneNumberFld.setText(user.getPhoneNumber());
				typeFld.getSelectionModel().select(user.getType().getDisplay().toUpperCase());
				deletedUserCB.setSelected(user.isDeleted());
			});
		}

		@Override
		protected void updateItem(Boolean t, boolean empty) {
			super.updateItem(t, empty);
			if (!empty) {
				setGraphic(cellButton);
			}
		}
	}
	

    /**
     * Jelszó emlékeztető információs ablakot jelenít meg a képernyőn.
     * @param event
     */
    @FXML
    void getPassword(ActionEvent event) {
    	String passWrod = (passwordFld.getText() != null && !passwordFld.getText().isEmpty()) ? ("Password: " + passwordFld.getText()) : "No password found!";
    	AlertHelper.callAlert(AlertType.INFORMATION, passWrod);
    }

}
