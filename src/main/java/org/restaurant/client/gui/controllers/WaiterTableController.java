package org.restaurant.client.gui.controllers;

import java.io.IOException;

import org.restaurant.client.beans.WaiterTableType;
import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductOrderState;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.ProductRequestState;
import org.restaurant.common.model.beans.Table;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class WaiterTableController {
	
    @FXML
    private Pane tablePane;

	@FXML
    private Pane tableSidePane;
    
    @FXML
    private MaterialDesignIconView waiterTableView;

    @FXML
    private Label waiterTableTableName;

    @FXML
    private ProgressBar waiterTableLoader;

    @FXML
    private Label waiterTableOrderCounter; 

    @FXML
    private JFXButton orderButton;
    
    @FXML
    private FontAwesomeIconView userIV;

    @FXML
    private FontAwesomeIconView separateUsersIV;
    
    private Pane parentPane;

	private Table table;
    private ProductRequest request;
    private WaiterProfileController parentController;

    /**
     * Request contains all orders of the table.
     * @param table
     * @param request request for the table which contains orders too from this table.
     */
    public WaiterTableController(Table table, ProductRequest request) {
    	this.table = table;
    	this.request = request;
    }
    
	/**
	 * Kiszinízei a kérés státusza alapján az asztalt, ha fizetési módban van akkor piros, különben marad zöld, fizetési módnál berakja az 
	 * egyvalaki fizet, vagy a mindenki külön fizet ikont.
	 */
	public void initialize() {
		initByRequestData();
		waiterTableTableName.setText(getTable().getName());
		waiterTableOrderCounter.setText(getDoneOrderNo() + "/" + request.getProductOrders().size());
		waiterTableLoader.setProgress(getDoneOrderPercent());
    }
	
	//TODO check its right way
	public void reInitalize(WaiterTableType waiterTableType) {
		Platform.runLater(() -> {
			initByRequestData();
			waiterTableOrderCounter.setText(getDoneOrderNo() + "/" + request.getProductOrders().size());
			waiterTableLoader.setProgress(getDoneOrderPercent());
			changeTableType(waiterTableType);
		});
	}
	
	/**
	 * Láthatóvá teszi az egy emberes fizetést jelző ikont.
	 */
	public void showOnePayIcon() {
		userIV.setVisible(true);
	}
	
	/**
	 * Láthatóvá teszi a mindenki külön fizet ikont.
	 */
	public void showSeparatePayIcon() {
		separateUsersIV.setVisible(true);
	}
	
	/**
	 * Visszatár, hogy látható-e valamelyik fizetést jelentő ikon.
	 * @return igaz, ha látható az egy ember fizet vagy a mindenki külön fizet ikon.
	 */
	public boolean isVisibleOneOfPayIcon() {
		return userIV.isVisible() || separateUsersIV.isVisible();
	}
	
	public void changeTableType(WaiterTableType tableType) {
		if(tableType != null) {
			String commonPaneStyle = " -fx-border-radius: 20; -fx-background-radius: 20; -fx-effect: dropshadow(gaussian, rgb(0.0, 0.0, 0.0, 0.15), 6.0, 0.7, 0.0,1.5)";
			if(tableType.equals(WaiterTableType.GREEN)) {
				tablePane.setStyle("-fx-background-color: #00BF9A; " + commonPaneStyle);
				tableSidePane.setStyle("-fx-background-color: #008975");
			} else if (tableType.equals(WaiterTableType.YELLOW)) {
				tablePane.setStyle("-fx-background-color: #FBC02D; " + commonPaneStyle);
				tableSidePane.setStyle("-fx-background-color: #F9A825");
			} else {
				tablePane.setStyle("-fx-background-color: #DD4B39; " + commonPaneStyle);
				tableSidePane.setStyle("-fx-background-color: #B13C2E");
			}
		}
	}

	private int getDoneOrderNo() {
		int doneOrderNo = 0;
		for (ProductOrder order : getRequest().getProductOrders()) {
			if (order.getState().equals(ProductOrderState.DONE)) {
				doneOrderNo++;
			}
		}
		return doneOrderNo;
	}
	
	private double getDoneOrderPercent() {
		double doneOrderNo = 0.0;
		for (ProductOrder order : getRequest().getProductOrders()) {
			if (order.getState().equals(ProductOrderState.DONE)) {
				doneOrderNo++;
			}
		}
		if (!getRequest().getProductOrders().isEmpty()) {
			return (doneOrderNo / getRequest().getProductOrders().size());
		} else
			return 0;
	}
	
	private void initByRequestData() {
		if(getRequest().getState().equals(ProductRequestState.PAYING)) {
			changeTableType(WaiterTableType.RED);
			if(getRequest().getPayType().equals(PayType.ONEPAYS)) {
				userIV.setVisible(true);
			} else if (getRequest().getPayType().equals(PayType.SEPARATE)) {
				separateUsersIV.setVisible(true);
			}
		}
	}
	
    @FXML
    void showRequestOrders(ActionEvent event) throws IOException {
		FXMLLoader loader = new FXMLLoader(
				getClass().getResource("/org/restaurant/client/gui/temp/waiter/TableRequestDialog.fxml"));
		//másolatot csinálni a requestről
		loader.setControllerFactory(c ->  new TableRequestDialogController(table, request));
		Pane loginPane = loader.load();
		TableRequestDialogController controller = loader.getController();
		controller.setParentController(this);
		
		Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(tablePane.getScene().getWindow());
		dialog.setTitle("Orders");
		dialog.setScene(new Scene(loginPane));
		dialog.centerOnScreen();
		dialog.setResizable(false);
		dialog.show();

		changeTableType(WaiterTableType.GREEN);
    }

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public ProductRequest getRequest() {
			return this.request;
	}

	public void setRequests(ProductRequest request) {
			this.request = request;
	}
	
    public Pane getTablePane() {
		return tablePane;
	}

	public void setTablePane(Pane tablePane) {
		this.tablePane = tablePane;
	}
	
    public Pane getParentPane() {
		return parentPane;
	}

	public void setParentPane(Pane parentPane) {
		this.parentPane = parentPane;
	}
	
	public WaiterProfileController getParentConroller() {
		return this.parentController;
	}
	
	public void setParentConroller(WaiterProfileController waiterProfileController) {
		this.parentController = waiterProfileController;
	}

}