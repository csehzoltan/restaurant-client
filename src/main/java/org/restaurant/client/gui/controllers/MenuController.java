package org.restaurant.client.gui.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.helpers.NumberHelper;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductCategory;
import org.restaurant.common.model.beans.ProductPrice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class MenuController {

	@FXML
    private BorderPane menuPane;
	
    @FXML
    private JFXButton searchButton;
    
    @FXML
    private JFXTextField searchFld;

    @FXML
    private ToggleGroup menuGroup;
    
    @FXML
    private VBox menuVBox;
    
    @FXML
    private ImageView soupIV;
    
    @FXML
    private ImageView appetizerIV;
    
    @FXML
    private ImageView dishIV;
    
    @FXML
    private ImageView pastaIV;
    
    @FXML
    private ImageView saladIV;
    
    @FXML
    private ImageView dessertIV;
    
    @FXML
    private ImageView drinkIV;
    
    @FXML
    private Label emptyorderLabel;

    @FXML
    private Label totalprice_label;
    
    @FXML
    private FlowPane flowPChairs;
    
    @FXML
    private VBox orderListVBox;
    
    @FXML
    private JFXButton orderButton;
    

    @FXML
    private JFXButton payButton;
    
    List<FlowPane> prodFlowPanes;
    ObservableList<String> chairs;
	private String selectedCategory;
    //chairName plus productionPriceId, quantity
    private Map<String, Integer> productOrders;
    
    private ClientMain clientMain;

	double totalPrice = 0.0;
    
	/**
	 * Felépíti a grafikus elemeket egy rendelés blokkot, menüblokkot, betölti a székekhez tartozó jelölőnégyzeteket,
	 * beállítja kezdeti állapotra őket. Stíluslapok behivatkozása. Id-k beállítása.
	 * @throws IOException
	 */
	public void initialize() throws IOException {
		
		initImages();
		
		productOrders = new TreeMap<>();
		initChairs();
		menuPane.getStylesheets().add("gui.css/main.css");
		
		ProductCategory prevProductCategory = null;
		prodFlowPanes = new ArrayList<>();
		if(ClientMain.getProducts() != null) {
			for(Product product : ClientMain.getProducts()) {
				if(prevProductCategory!= null && prevProductCategory.getName().equals(product.getCategory().getName())) {
					prodFlowPanes.get(prodFlowPanes.size() - 1); //get last flowpane to insert new product
				} else {

					Label categoryTitle = new Label(product.getCategory().getName().toUpperCase());
					categoryTitle.setId(product.getCategory().getName().toLowerCase() + "_title");
					FlowPane newCategoryFlowPane = new FlowPane();
					newCategoryFlowPane.setId(product.getCategory().getName().toLowerCase() + "_pane");
					newCategoryFlowPane.setColumnHalignment(HPos.LEFT);
					newCategoryFlowPane.setRowValignment(VPos.CENTER);
					newCategoryFlowPane.setPrefWrapLength(400);
					newCategoryFlowPane.setAlignment(Pos.TOP_LEFT);
					newCategoryFlowPane.setHgap(20);
					newCategoryFlowPane.setVgap(20);

					prodFlowPanes.add(newCategoryFlowPane);

					menuVBox.getChildren().add(categoryTitle);
					menuVBox.getChildren().add(newCategoryFlowPane);

					prevProductCategory = product.getCategory();
				}
				
				ImageView productImageView = new ImageView(byteArrayToImage(product.getImg(), 292, 163));
				productImageView.setFitWidth(292);
				productImageView.setFitHeight(163);
				
				final Insets marginTenTopLeft = new Insets(5, 0, 0, 5);
				Label productName = new Label(product.getName());
				productName.setTextFill(Color.web("#363636"));
				productName.setFont(Font.font("System", FontWeight.BOLD, 20));
				productName.setId(product.getId() + "_productName");
				productName.getStyleClass().add("productName");
				VBox.setMargin(productName, marginTenTopLeft);
				
				Label labelDescription = new Label(product.getDescription());
				labelDescription.setTextFill(Color.web("#575757"));
				labelDescription.setFont(Font.font("System", 12));
				final Tooltip tooltip = new Tooltip();
				tooltip.setText(labelDescription.getText());
				labelDescription.setTooltip(tooltip);
				//labelDescription.setWrapText(true);
				VBox.setMargin(labelDescription, marginTenTopLeft);
				
				FlowPane pricePane = new FlowPane();
				pricePane.setPrefWrapLength(300);
				pricePane.setAlignment(Pos.TOP_LEFT);
				pricePane.setHgap(8);
				pricePane.setVgap(8);
				
				if(product.getPrices() != null && !product.getPrices().isEmpty()) {
					addPriceButtons(product, pricePane);
				}
				
				VBox productVBox = new VBox();
				productVBox.setId(product.getId() + "_box");
				productVBox.setMaxWidth(301);
				productVBox.getChildren().add(productImageView);
				productVBox.getChildren().add(productName);
				productVBox.getChildren().add(labelDescription);
				productVBox.getChildren().add(pricePane);		
//				Rating rating = new Rating();
//				productVBox.getChildren().add(rating);
				
				prodFlowPanes.get(prodFlowPanes.size() - 1).getChildren().add(productVBox);		
			}
		}
		

		if (ClientMain.getTable() != null) {
			for (Chair chair : ClientMain.getTable().getChairs()) {
				JFXCheckBox cb = new JFXCheckBox(chair.getName());
				cb.getStyleClass().add("chair-checkbox");
				flowPChairs.getChildren().add(cb);
			}
		}
	}

	/**
	 * Feltölti az ételkategóriákat felsorakoztató menüpontokat ikonokkal.
	 */
	private void initImages() {
		soupIV.setImage(new Image(getClass().getResource("/gui.icons/soup.png").toExternalForm()));
		appetizerIV.setImage(new Image(getClass().getResource("/gui.icons/appetizer.png").toExternalForm()));
		dishIV.setImage(new Image(getClass().getResource("/gui.icons/dish.png").toExternalForm()));
		pastaIV.setImage(new Image(getClass().getResource("/gui.icons/pasta.png").toExternalForm()));
		saladIV.setImage(new Image(getClass().getResource("/gui.icons/salad.png").toExternalForm()));
		dessertIV.setImage(new Image(getClass().getResource("/gui.icons/piece-of-cake.png").toExternalForm()));
		drinkIV.setImage(new Image(getClass().getResource("/gui.icons/orange-juice.png").toExternalForm()));
	}

	private void initChairs() {
		chairs = FXCollections.observableArrayList();
		if(ClientMain.getTable() != null && ClientMain.getTable().getChairs() != null) {
			for (Chair chair : ClientMain.getTable().getChairs()) {
				chairs.add(chair.getName());
			}
		}	
	}

	/**
	 * Termékekhez tartozó árakat tartalmazó gombok betöltése, id-k beállítása.
	 * @param product termék
	 * @param pricePane árakat tartalmazó blokk
	 */
	private void addPriceButtons(Product product, FlowPane pricePane) {
		boolean isFirst = true;
		for(ProductPrice productPrice : product.getPrices()) {
			if(!productPrice.isDeleted()) {
				String priceText = "";
				if(productPrice.getAmount()!=null) {
					priceText += productPrice.getAmount() + productPrice.getUnit().getValue() + " - $" + productPrice.getPrice(); 
				} else {
					priceText += productPrice.getSize().getDisplay() + " - $" + productPrice.getPrice();
				}
				
				JFXButton priceButton = new JFXButton(priceText);
				priceButton.setPrefHeight(30);
				priceButton.setPrefWidth(80);
				priceButton.setWrapText(true);
//				priceButton.setFont(new Font("Arial", 1));
				priceButton.setId(productPrice.getId() + "_priceButton");
				FlowPane.setMargin(priceButton, new Insets(8, 0, 15, isFirst ? 5 : 0));
				priceButton.setStyle("-fx-font-size: 12px;"); 
				priceButton.getStyleClass().add("menu-price-button");
				priceButton.setOnAction(this::orderAction);
				pricePane.getChildren().add(priceButton);
				isFirst = false;
			}
		}
	}
	
	/**
	 * Képet tároló byte tömb javafx képpé konvertálása.
	 * @param raw kép byte-ben
	 * @param width kép szélessége
	 * @param height kép magassága
	 * @return a megadott méretű java fx képpé konvertált képpel
	 * @throws IOException
	 */
	private static Image byteArrayToImage(byte[] raw, final int width, final int height) throws IOException {
		WritableImage image = new WritableImage(width, height);
		if(raw != null) {
			ByteArrayInputStream bis = new ByteArrayInputStream(raw);
			BufferedImage read = ImageIO.read(bis);
			image = SwingFXUtils.toFXImage(read, null);
		}
		return image;
	}

    @FXML
    void searchMenuItem(ActionEvent event) {

    }
    

    
    @FXML
    void searchByName(KeyEvent event) {
    	for(FlowPane pane : prodFlowPanes) {
    		int visibleProdNum = 0;
    		for(Node productBox : pane.getChildren()) {
    			
				Label productName = (Label) productBox.lookup("#" + productBox.getId().split("_")[0] + "_productName");
				if (!productName.getText().toLowerCase().contains(searchFld.getText().toLowerCase())) {
					productBox.setVisible(false);
					productBox.setManaged(false);
				} else {
					++visibleProdNum;
					productBox.setVisible(true);
					productBox.setManaged(true);
				}
    			
				if (visibleProdNum == 0) {
					menuVBox.lookup("#" + pane.getId().split("_")[0] + "_title").setVisible(false);
				} else {
					menuVBox.lookup("#" + pane.getId().split("_")[0] + "_title").setVisible(true);
				}
    		}
    	}
    }

    @FXML
    void typeFilterAction(ActionEvent e) {
    	ToggleButton selBtn = (ToggleButton) e.getSource();
    	String selBtnId = selBtn.getId().toLowerCase();
    	
		for (FlowPane pane : prodFlowPanes) {
			if (!selBtn.isSelected()) {
				pane.setVisible(true);
				pane.setManaged(true);
				menuVBox.lookup("#" + pane.getId().split("_")[0] + "_title").setVisible(true);
				menuVBox.lookup("#" + pane.getId().split("_")[0] + "_title").setManaged(true);
				selectedCategory = null;
			} else {
				selectedCategory = selBtnId.split("_")[0];
				hideOtherCategories(selectedCategory, pane);
			}
		}
    }
    
    /**
     * Amennyiben a rendelési lista üres, figyelmeztető üzenetet dob és nem küld kérést a szervernek,
     * különben elküldi a szervernek a rendelési listát.
     * @param event
     */
    @FXML
    void ordering(ActionEvent event) {
    	if(productOrders.isEmpty()) {
    		AlertHelper.callAlert(AlertType.WARNING, "The order list is empty!");
    	} else {
    		Integer userId = ClientMain.getUser() == null ? null : ClientMain.getUser().getId();
        	JsonObject jsMsg = createJsonMsg(productOrders, "ordering", String.valueOf(ClientMain.getTable().getId()), userId);
        	
    		PrintWriter clientWriter = ClientMain.getClient().getWriter();
    		clientWriter.println(jsMsg.toString());
    		clientWriter.flush();
    	}
    }
    
    /**
     * Leellenőrzi, hogy a rendelés listában szerepel-e elem, mert ha igen, akkor addig nem lehet fizetni, azokat meg kell szüntetni, 
     * vagy le kell adni a rendelést rájuk.
     * @param event
     */
    @FXML
    void payAction(ActionEvent event) {
    	if(!productOrders.isEmpty()) {
    		AlertHelper.callAlert(AlertType.WARNING, "While the order list is not empty you can not pay!\n Please send or delete orders!");
    	} else {
    		Alert alert = new Alert(AlertType.CONFIRMATION);
    		alert.setTitle("Payment");
    		alert.setHeaderText("Payment");
    		alert.setContentText("Choose your payment method");

    		ButtonType onePaymentBtn = new ButtonType("One pays");
    		ButtonType separatePaymentBtn = new ButtonType("Separate payment");
    		ButtonType cancelBtn = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

    		alert.getButtonTypes().setAll(onePaymentBtn, separatePaymentBtn, cancelBtn);

    		Optional<ButtonType> result = alert.showAndWait();
    		if(result.isPresent()) {
    			if (result.get() == onePaymentBtn) {
    				sendPayMsg(PayType.ONEPAYS);
        		} else if (result.get() == separatePaymentBtn) {
        			sendPayMsg(PayType.SEPARATE);
        		}
    		}
    		
    	}
    }

	private void sendPayMsg(PayType payType) {
		Map<String, String> payParams = new HashMap<>();
		payParams.put("tableId", String.valueOf(ClientMain.getTable().getId()));
		payParams.put("payTypeId", String.valueOf(payType.getId()));
		ClientMain.getClient().getWriter().println(JsonHelper.createJsonMsg("payGuest", null, payParams));
		ClientMain.getClient().getWriter().flush();
	}
    
	private JsonObject createJsonMsg(Object data, String actionType, String info, Integer userId) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(data);
		JsonObject obj = new JsonParser().parse(json).getAsJsonObject();

		JsonElement actionNameJsonElement = new JsonParser().parse(actionType);
		JsonObject sendMsg = new JsonObject();

		sendMsg.add("action", actionNameJsonElement);
		sendMsg.add("data", obj);
		if(info!=null) {
			JsonElement infoJsonElement = new JsonParser().parse(info);
			sendMsg.add("info", infoJsonElement);
		}
		if(userId != null) {
			JsonElement infoJsonElement = new JsonParser().parse(String.valueOf(userId));
			sendMsg.add("userId", infoJsonElement);
		}
		return sendMsg;
		
	}

	private void hideOtherCategories(String selectedCategory, FlowPane pane) {
		String paneCategory = pane.getId().split("_")[0];
		if (!paneCategory.equals(selectedCategory)) {
			pane.setVisible(false);
			pane.setManaged(false);
			menuVBox.lookup("#" + paneCategory + "_title").setVisible(false);
			menuVBox.lookup("#" + paneCategory + "_title").setManaged(false);
		} else {
			pane.setManaged(true);
			pane.setVisible(true);
			menuVBox.lookup("#" + paneCategory + "_title").setVisible(true);
			menuVBox.lookup("#" + paneCategory + "_title").setManaged(true);
		}
	}
	
	public void deleteOrders() {
		Platform.runLater(() -> {
			orderListVBox.getChildren().clear();
			productOrders.clear();
			totalPrice = 0.0;
			totalprice_label.setText("0.0");
			
			emptyorderLabel.setVisible(true);
			emptyorderLabel.setManaged(true);
		});
	}
	
	private void orderAction(ActionEvent event) {
		if (!hasSelectedChair()) {
			AlertHelper.callAlert(AlertType.WARNING, "Firstly select the chair(s) where the order belongs."
					+ "\nThe selection can be made with the checkbox below the offers!");
		} else {
			hideEmptyOrderLbl();

			JFXButton priceButton = (JFXButton) event.getSource();
			int productionId = Integer.parseInt(priceButton.getParent().getParent().getId().split("_")[0]);
			int productionPriceId = Integer.parseInt(priceButton.getId().split("_")[0]);

			String prodName = ((Label) menuVBox.lookup("#" + productionId + "_productName")).getText();
			String amount = priceButton.getText().split("-")[0].trim().replace(".", ",");
			double price = Double.parseDouble(priceButton.getText().split("-")[1].trim().substring(1));
			
			for (String chairName : getSelectedChairs()) {
				VBox charOrdersVBox = ((VBox) orderListVBox.lookup("#" + chairName + "_box"));
				boolean isNewCharOrder = false;
				if (charOrdersVBox == null) {
					charOrdersVBox = new VBox();
					charOrdersVBox.setId(chairName + "_box");
					isNewCharOrder = true;
				}
				String orderKey = chairName + "_" + productionPriceId;
				if (productOrders.containsKey(orderKey)) {
					productOrders.put(orderKey, productOrders.get(orderKey) + 1);
					((Text) orderListVBox.lookup("#" + orderKey + "_label"))
							.setText(prodName + " (" + amount + ")" + " (X" + productOrders.get(orderKey) + ")");
					Label priceLabel = ((Label) orderListVBox.lookup("#" + orderKey + "_price"));
					Double newPrice = NumberHelper
							.scaleDouble(Double.parseDouble(priceLabel.getText().replace("$", "").trim()) + price, 2);
					priceLabel.setText(newPrice + " $");
				} else {
					FontAwesomeIconView deleteIcon = new FontAwesomeIconView(FontAwesomeIcon.TIMES);
					deleteIcon.getStyleClass().add("close-button");
					deleteIcon.setGlyphSize(20);

					JFXButton deleteOrderButton = new JFXButton("", deleteIcon);
					deleteOrderButton.getProperties().put("productionName", prodName);
					deleteOrderButton.setPrefSize(20, 20);
					deleteOrderButton.setPadding(Insets.EMPTY);
					deleteOrderButton.setOnAction(this::deleteOrder);
					deleteOrderButton.setId(orderKey + "_delete");

//					FontAwesomeIconView editIcon = new FontAwesomeIconView(FontAwesomeIcon.PENCIL);
//					editIcon.getStyleClass().add("edit-button");
//					editIcon.setGlyphSize(20);
//
//					JFXButton editOrderButton = new JFXButton("", editIcon);
//					editOrderButton.getProperties().put("productionName", prodName);
//					editOrderButton.setPrefSize(20, 20);
//					editOrderButton.setPadding(Insets.EMPTY);
//					editOrderButton.setId(orderKey + "_edit");

					productOrders.put(orderKey, 1);

					Text orderDataText = new Text(
							prodName + " (" + amount + ")" + " (X" + productOrders.get(orderKey) + ")");
					orderDataText.setWrappingWidth(155);
					orderDataText.setId(orderKey + "_label");

					Label priceLabel = new Label(price + "$");
					priceLabel.setPrefWidth(60);
					priceLabel.setWrapText(true);
					priceLabel.setAlignment(Pos.TOP_RIGHT);
					priceLabel.setWrapText(true);
					priceLabel.setId(orderKey + "_price");
					priceLabel.getStyleClass().add("text_MD_dark");

					Text chairText = new Text(chairName);
					chairText.setWrappingWidth(40);
					chairText.setTextAlignment(TextAlignment.CENTER);
					chairText.setId(orderKey + "_chair");
					chairText.getStyleClass().add("text_MD_orange");

					GridPane orderGridPane = new GridPane();
					orderGridPane.getColumnConstraints().add(new ColumnConstraints(20));
					orderGridPane.getColumnConstraints().add(new ColumnConstraints(155));
					orderGridPane.getColumnConstraints().add(new ColumnConstraints(60));
					orderGridPane.getColumnConstraints().add(new ColumnConstraints(50));
//					orderGridPane.getColumnConstraints().add(new ColumnConstraints(20));

					GridPane.setValignment(deleteOrderButton, VPos.TOP);
					GridPane.setValignment(orderDataText, VPos.TOP);
					GridPane.setHalignment(orderDataText, HPos.LEFT);
					GridPane.setValignment(priceLabel, VPos.TOP);
					GridPane.setHalignment(priceLabel, HPos.RIGHT);
//					GridPane.setValignment(editOrderButton, VPos.TOP);
					GridPane.setValignment(chairText, VPos.TOP);

					orderGridPane.add(deleteOrderButton, 0, 0);
					orderGridPane.add(orderDataText, 1, 0);
					orderGridPane.add(priceLabel, 2, 0);
					orderGridPane.add(chairText, 3, 0);
//					orderGridPane.add(editOrderButton, 4, 0);
					orderGridPane.setId(orderKey + "_orderListItem");

					if (isNewCharOrder) {
						orderListVBox.getChildren().add(charOrdersVBox);
						charOrdersVBox.getChildren().add(orderGridPane);
					} else {
						charOrdersVBox.getChildren().add(orderGridPane);
					}
				}
				totalPrice += price;
				totalprice_label.setText(NumberHelper.scaleDouble(totalPrice, 2) + "$");
			}
		}

	}

	private List<String> getSelectedChairs() {
		List<String> selectedChairs = new ArrayList<>();
		for (Node node : flowPChairs.getChildren()) {
			if (((JFXCheckBox) node).isSelected()) {
				selectedChairs.add(((JFXCheckBox) node).getText());
			}
		}
		return selectedChairs;
	}

	private void hideEmptyOrderLbl() {
		if (emptyorderLabel.isVisible()) {
			emptyorderLabel.setVisible(false);
			emptyorderLabel.setManaged(false);
		}
	}

	private boolean hasSelectedChair() {
		boolean hasSelectedChair = false;
		for (Node node : flowPChairs.getChildren()) {
			if (((JFXCheckBox) node).isSelected()) {
				hasSelectedChair = true;
				break;
			}
		}
		return hasSelectedChair;
	}

	private void deleteOrder(ActionEvent e) {
		JFXButton deleteButton = (JFXButton) e.getSource();
		String[] buttonId = deleteButton.getId().split("_");
		String chairName = buttonId[0];
		int productionPriceId = Integer.parseInt(buttonId[1]);
		String orderKey = chairName + "_" + productionPriceId;
		String price = ((JFXButton) menuVBox
				.lookup("#" + productionPriceId + "_priceButton")).getText().split("-")[1].trim().substring(1);
		totalPrice -= NumberHelper
				.scaleDouble(Double.parseDouble(price), 2);
		
		totalprice_label.setText(NumberHelper.scaleDouble(totalPrice, 2) + "$");

		Label priceLabel = ((Label) orderListVBox.lookup("#" + orderKey + "_price"));
		Double newPrice = NumberHelper
				.scaleDouble(Double.parseDouble(priceLabel.getText().replace("$", "").trim()) - Double.parseDouble(price), 2);

		Integer orderedPiece = productOrders.get(orderKey);
		
		if((orderedPiece -1) == 0) {
			VBox chairOrdersVbos = ((VBox)orderListVBox.lookup("#" + chairName + "_box"));
			chairOrdersVbos.getChildren().remove(chairOrdersVbos.lookup("#" + orderKey + "_orderListItem"));
			productOrders.remove(orderKey);
			if(productOrders.isEmpty()) {
				emptyorderLabel.setVisible(true);
				emptyorderLabel.setManaged(true);
			}
		} else {
			productOrders.put(orderKey +"", orderedPiece -1);
			((Text) orderListVBox.lookup("#" + orderKey + "_label")).setText(
					deleteButton.getProperties().get("productionName") + " (" + "SM" + ")" + " (X" + productOrders.get(orderKey) + ")");
		}
		
		priceLabel.setText(newPrice + "$");
	}
	
    public final ClientMain getClientMain() {
		return clientMain;
	}

	public final void setClientMain(ClientMain clientMain) {
		this.clientMain = clientMain;
	}
	
    public ObservableList<String> getChairs() {
		return chairs;
	}

}