package org.restaurant.client.gui.controllers;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.SortedSet;

import javax.imageio.ImageIO;

import org.apache.commons.lang.SerializationUtils;
import org.restaurant.client.gui.validators.FieldValidator;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductCategory;
import org.restaurant.common.model.beans.ProductPrice;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ProductTablePaneController {

    @FXML
    private HBox dataFieldHBox;
	
    @FXML
    private TableView<Product> productTable;
    
    @FXML
    private FlowPane ingredientListPane;
    
    @FXML
    private FlowPane productPriceListPane;

    @FXML
    private TableColumn<Product, Integer> colId;

    @FXML
    private TableColumn<Product, String> colName;

    @FXML
    private TableColumn<Product, ProductCategory> colCategory;
    
    @FXML
    private TableColumn<Product, String> colDescription;
    
    @FXML
    private TableColumn<Product, Boolean> colDeleted;
    
    @FXML
    private ImageView productImage; 
    
    @FXML
    private JFXButton productImgUploadBtn;

    @FXML
    private JFXTextField idFld;

    @FXML
    private JFXTextField nameFld;

    @FXML
    private JFXComboBox<String> categoryFld;

    @FXML
    private JFXTextField descriptionFld;
    
    @FXML
    private JFXCheckBox deletedProdCB;
    
    @FXML
    private HBox ingredientHBox;
    
    private FileChooser fileChooser;
    
    private File file;
    
	private SortedSet<Product> products;
	
	Product temporaryProduct;

	private FieldValidator validator;
	
	IngredientSurfaceController ingredientSurfaceController;

	public ProductTablePaneController(SortedSet<Product> products) {
		this.products = products;
	}
	
	@FXML
	//TODO separate to helper class
	public void initialize() {
//		ingredientsSrcollPane.setVbarPolicy(ScrollBarPolicy.NEVER);
//		ignoreScrollVertical();
		idFld.setDisable(true);
		fileChooser = new FileChooser();
		validator = new FieldValidator();
		initProductTable();
		initFields();
	}
	
	private void initProductTable() {
		productTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		idFld.setVisible(true);
		colId.setCellValueFactory(new PropertyValueFactory<Product, Integer>("id"));
		colName.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
		colCategory.setCellValueFactory(new PropertyValueFactory<Product, ProductCategory>("category"));
		colDescription.setCellValueFactory(new PropertyValueFactory<Product, String>("description"));
		
		colDescription.setCellFactory(tc -> {
		    TableCell<Product, String> cell = new TableCell<>();
		    Text text = new Text();
		    cell.setGraphic(text);
		    cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
		    text.wrappingWidthProperty().bind(colDescription.widthProperty());
		    text.textProperty().bind(cell.itemProperty());
		    return cell ;
		});
		
		colDeleted.setCellValueFactory(new PropertyValueFactory<Product, Boolean>("deleted"));
		
		TableColumn<Product, Boolean> colEdit = new TableColumn<>("Act.");
		colEdit.setMinWidth(55.0);
		colEdit.setPrefWidth(55.0);
		colEdit.setMaxWidth(55.0);
		productTable.getColumns().add(colEdit);
		colEdit.setCellValueFactory(p -> new SimpleBooleanProperty(p.getValue() != null));
		colEdit.setCellFactory(p -> new ButtonCell());
		
		ObservableList<Product> productTableItems = FXCollections.observableArrayList();
		for (Product product : ClientMain.getProducts()) {
			productTableItems.add(product);
		}

		if (productTableItems != null) {
			productTable.setItems(productTableItems);
		}
	}
	
	private void initFields() {
		ObservableList<String> userTypes = FXCollections.observableArrayList();
		for (ProductCategory category : ProductCategory.values()) {
			userTypes.add(category.getName() != null ? category.getName().toUpperCase() : null);
		}
		categoryFld.getItems().addAll(userTypes);
	}
	
    @FXML
    void addIngredient(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/IngredientSurface.fxml"));
			if(temporaryProduct == null) {
				temporaryProduct = new Product();
			}
			loader.setControllerFactory(c -> new IngredientSurfaceController(temporaryProduct));
			Pane ingredientPane = loader.load();
			ingredientSurfaceController = loader.getController();
			ingredientSurfaceController.setParentController(this);

			Stage dialog = new Stage();
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(ingredientListPane.getScene().getWindow());
			dialog.setTitle("Ingredients");
			dialog.setScene(new Scene(ingredientPane));
			dialog.centerOnScreen();
			dialog.setResizable(false);
			dialog.show();
		} catch (IOException e) {
			System.out.print("Ingredient load problem: " + e.getMessage());
			e.printStackTrace();
		}
    }
    

    @FXML
    //TODO
    void modifyPrices(ActionEvent event) {
    	try {
			FXMLLoader loader = new FXMLLoader(
					getClass().getResource("/org/restaurant/client/gui/temp/administrator/ProductPriceDialog.fxml"));

			if(temporaryProduct == null) {
				temporaryProduct = new Product();
			}
			loader.setControllerFactory(c -> new ProductPriceDialogController(temporaryProduct));
			Pane productPricePane = loader.load();
			ProductPriceDialogController productPriceDialogController = loader.getController();
			productPriceDialogController.setParentController(this);

			Stage dialog = new Stage();
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.initOwner(ingredientListPane.getScene().getWindow());
			dialog.setTitle("Prices");
			dialog.setScene(new Scene(productPricePane));
			dialog.centerOnScreen();
			dialog.setResizable(false);
			dialog.show();
		} catch (IOException e) {
			System.out.print("Price load problem: " + e.getMessage());
			e.printStackTrace();
		}
    }
    
    @FXML
    void uploadImg(ActionEvent event) {
			//filechooser add to stage window
			file = fileChooser.showOpenDialog(productImage.getScene().getWindow());
			if(file != null) {
				productImage.setImage(new Image(file.toURI().toString(), 450, 252, true, true));
				productImage.setFitWidth(200);
				productImage.setFitHeight(150);
				productImage.setPreserveRatio(true);
			}
    }
    
    /**
     * A termék általános adataihoz tartozó (id, kép, név, leírás) mezők adatainak törlése.
     * @param event
     */
    @FXML
    void eraseProductData(ActionEvent event) {
    	idFld.clear();
    	nameFld.clear();
    	descriptionFld.clear();
    	categoryFld.setValue(null);
    	productImage.setImage(null);
    	temporaryProduct = null;
    	deletedProdCB.setSelected(false);
    	
		while (ingredientListPane.lookup(".ingredientClass") != null) {
			ingredientListPane.getChildren().removeAll(ingredientListPane.lookup(".ingredientClass"));
		}
		
		while (productPriceListPane.lookup(".productPriceClass") != null) {
			productPriceListPane.getChildren().removeAll(productPriceListPane.lookup(".productPriceClass"));
		}
    }

    //TODO Másik osztályba szervezni, ami kezeli a képeket.
	private static Image byteArrayToImage(byte[] raw, final int width, final int height) throws IOException {
		WritableImage image = new WritableImage(width, height);
		ByteArrayInputStream bis = new ByteArrayInputStream(raw);
		BufferedImage read = ImageIO.read(bis);
		image = SwingFXUtils.toFXImage(read, null);
		return image;
	}
	
    /**
     * A beviteli mezőkből és a kontrollerekből kiszedi a megfelelő értékeket és aszerint létrehoz egy új terméket, majd elküldi a 
     * szervernek mentésre, módosításra.
     * @param event
     */
    @FXML
    void saveProduct(ActionEvent event) {
    	if(validateSaveFields()) {
    			if(temporaryProduct == null) {
    				temporaryProduct = new Product();
    			}
    			temporaryProduct.setId(idFld.getText().isEmpty() ? null : Integer.valueOf(idFld.getText()));
    			temporaryProduct.setName((nameFld.getText() == null || nameFld.getText().isEmpty()) ? null : nameFld.getText());
    			temporaryProduct.setCategory(ProductCategory.getByName(categoryFld.getValue()));
    			saveImageToProduct(temporaryProduct);
    			temporaryProduct.setDeleted(deletedProdCB.isSelected());
    			temporaryProduct.setDescription((descriptionFld.getText() == null || descriptionFld.getText().isEmpty()) ? null : descriptionFld.getText());
    		
    		if (ClientMain.getClient() != null) {
				ClientMain.getClient().getWriter().println(JsonHelper.createJsonMsg("saveProductByAdmin", temporaryProduct, null));
				ClientMain.getClient().getWriter().flush();
			}
    	}
    }

	private void saveImageToProduct(Product newProduct) {
		if(productImage.getImage() != null) {
			BufferedImage bImage = SwingFXUtils.fromFXImage(productImage.getImage(), null);
			ByteArrayOutputStream s = new ByteArrayOutputStream();

			try {
				ImageIO.write(bImage, "png", s);
				byte[] res = s.toByteArray();
				s.close();
				
				newProduct.setImg(res);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
    
	/**
	 * Validálást futtat le a mezőkön, amennyiben hibás adatot talál hibaüzenetet dob.
	 * @return true értékkel tér vissza, ha a validálás sikeres, különben false successful else false.
	 */
	private boolean validateSaveFields() {
		boolean isValid = false;
		if(validator.validateTf(nameFld, FieldValidator.ANYCHARACTERREGEXP, true, 3, 50, FieldValidator.MSGNAMERULE) && 
				validator.validateComboBoxRequired(categoryFld) &&
				validator.validateTf(descriptionFld, FieldValidator.ANYCHARACTERREGEXP, true, 3, 250, "")
				) {
			isValid = true;
		}
		return isValid;
	}
    
	private class ButtonCell extends TableCell<Product, Boolean> {
		Button cellButton = new Button("Edit");

		/**
		 * Button text set null because it add dots if not set it.
		 */
		ButtonCell() {
			Image imgEdit = new Image(getClass().getResource("/gui.icons/edit.png").toExternalForm());
			ImageView iv = new ImageView();
			iv.setFitWidth(20);
			iv.setFitHeight(20);
			iv.setImage(imgEdit);
			iv.setPreserveRatio(true);
			iv.setSmooth(true);
			iv.setCache(true);
			cellButton.setText(null);
			cellButton.setStyle("-fx-background-color: transparent;");
			cellButton.setGraphic(iv);
			setAlignment(Pos.CENTER);
			cellButton.setOnAction(t -> {
				
				Integer editIndex = ButtonCell.this.getIndex();
				productTable.getSelectionModel().select(editIndex);
				Product product = ButtonCell.this.getTableView().getItems().get(editIndex);
				
				temporaryProduct = (Product) SerializationUtils.clone(product);
				idFld.setText(temporaryProduct.getId().toString());
				nameFld.setText(temporaryProduct.getName());
				categoryFld.getSelectionModel().select(temporaryProduct.getCategory().getName().toUpperCase());
				descriptionFld.setText(temporaryProduct.getDescription());
				deletedProdCB.setSelected(temporaryProduct.isDeleted());
				
				drawIngredients(temporaryProduct.getIngredients());
				drawProductPrices(temporaryProduct.getPrices());
				
					try {
						productImage.setImage(null);
						if(temporaryProduct.getImg() != null) {
							productImage.setImage(byteArrayToImage(temporaryProduct.getImg(), 450, 252));
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					productImage.setFitWidth(200);
					productImage.setFitHeight(150);
					productImage.setPreserveRatio(true);
			});
		}

		@Override
		protected void updateItem(Boolean t, boolean empty) {
			super.updateItem(t, empty);
			if (!empty) {
				setGraphic(cellButton);
			}
		}
	}
	
	/**
	 * Kirejzolja a termékek felületen az aktuálisan kiválasztott összetevőket,
	 * kirajzolás előtt törli az összes megjelenítettet amennyiben létezik.
	 * 
	 * @param ingredients Összetevők listája amit ki szeretnénk rajzolni a termékek
	 *            felületen az adott termékhez.
	 */
	protected void drawIngredients(List<Ingredient> ingredients) {
		while (ingredientListPane.lookup(".ingredientClass") != null) {
			ingredientListPane.getChildren().removeAll(ingredientListPane.lookup(".ingredientClass"));
		}
		if(ingredients != null) {
			for (Ingredient ingredient : ingredients) {
				FXMLLoader loader = new FXMLLoader(
						getClass().getResource("/org/restaurant/client/gui/temp/administrator/IngredientPane.fxml"));
	
				loader.setControllerFactory(c -> new IngredientPaneController(ingredient));
				Parent ingredientPane;
				try {
					ingredientPane = loader.load();
					IngredientPaneController ingredientPaneController = loader.getController();
					ingredientPaneController.setParentController(this);
					ingredientListPane.getChildren().add(ingredientPane);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Termék árainak GUI megjelenítése, vezérlők beállítása.
	 * @param productPrices Termék árainak listája.
	 */
	protected void drawProductPrices(List<ProductPrice> productPrices) {
		while (productPriceListPane.lookup(".productPriceClass") != null) {
			productPriceListPane.getChildren().removeAll(productPriceListPane.lookup(".productPriceClass"));
		}
		if(productPrices != null) {	
			for (ProductPrice productPrice : productPrices) {
				FXMLLoader loader = new FXMLLoader(
						getClass().getResource("/org/restaurant/client/gui/temp/administrator/PorductPriceInfoBox.fxml"));
				loader.setControllerFactory(c -> new PorductPriceInfoBoxController(productPrice));
				
				try {
					Parent productPricePane = loader.load();
					PorductPriceInfoBoxController productPriceController = loader.getController();
					productPriceController.setParentController(this);
	
					productPriceListPane.getChildren().add(productPricePane);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Kitörli az segéd termékből a megadott összetevőt és a felületről leszedi az összetevőt.
	 * @param ingredient Törlendő összetevő.
	 */
	protected void deleteIngredient(Ingredient ingredient) {
		if(ingredient != null && ingredient.getId() != null && temporaryProduct != null) {
			ingredientListPane.getChildren().remove(ingredientListPane.lookup("#ingredient_" + ingredient.getId()));
			if(temporaryProduct.getIngredients() != null) {
				for (int i = 0; i < temporaryProduct.getIngredients().size(); i++) {
					if(temporaryProduct.getIngredients() != null && temporaryProduct.getIngredients().get(i).getId().equals(ingredient.getId())) {
						temporaryProduct.getIngredients().remove(i);
					}
				}
			}
		}
	}
	
	/**
	 * Kitörli az ideiglenes termékből a paraméterként megadott árat megjelenítési név alapján.
	 * Id alapján történik a törlés.
	 * @param price Ár ami törlésre kerül a termékből.
	 */
	protected void deleteProductPrice(ProductPrice price) {
		if (price != null && price.getId() != null && temporaryProduct != null && temporaryProduct.getId() != null) {
			productPriceListPane.getChildren().remove(productPriceListPane.lookup("#productPrice_" + price.getId()));

			for (int i = 0; i < temporaryProduct.getPrices().size(); i++) {
				if (temporaryProduct.getPrices() != null && temporaryProduct.getPrices().get(i).getId().equals(price.getId())) {
					//temporaryProduct.getPrices().remove(i);
					temporaryProduct.getPrices().get(i).setDeleted(true);
				}
			}
		}
	}
	
	/**
	 * Frissíti a termékek táblát, amennyiben beszúrás van, akkor hozzácsatolja a táblázathoz, különben frissíti a létező rekordot.
	 * @param product termék.
	 * @param isInsert beszúr a táblába új rekordként, vagy frissít meglévő adatot (id alapú felismerés).
	 */
	public void updateTable(Product product, boolean isInsert) {
		Platform.runLater(() -> {
			if (isInsert) {
				productTable.getItems().add(product);
				if(nameFld.getText() != null && product.getName().equals(nameFld.getText())) {
					idFld.setText(product.getId().toString());
				}
			} else {
				for (Product productItem : productTable.getItems()) {
					if (productItem.getId().equals(product.getId())) {
						productItem.setId(product.getId());
						productItem.setCategory(product.getCategory());
						productItem.setDeleted(product.isDeleted());
						productItem.setDescription(product.getDescription());
						productItem.setImg(product.getImg());
						productItem.setIngredients(product.getIngredients());
						productItem.setPrices(product.getPrices());
					}
				}
			}
			productTable.refresh();
		});
	}
	
	/**
	 * Összetevő vezérlőjének kikérése.
	 * @return Összetevő vezérlője.
	 */
	public IngredientSurfaceController getIngredientSurfaceController() {
		return ingredientSurfaceController;
	}
	
	/**
	 * Termék másolatának kikérése (deep copy változat), ezen történnek a termék módosításai.
	 * @return Termék másolata, melyen a módosítások végbemennek.
	 */
	public Product getTemporaryProduct() {
		return temporaryProduct;
	}

	/**
	 * Termék másolatának (deep copy) beállítása, ezen történnek a termék módosításai.
	 * @param temporaryProduct Termék másolatának beállítása, melyen a módosítások fognak végbemenni.
	 */
	public void setTemporaryProduct(Product temporaryProduct) {
		this.temporaryProduct = temporaryProduct;
	}

}