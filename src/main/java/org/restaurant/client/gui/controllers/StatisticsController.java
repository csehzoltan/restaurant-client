package org.restaurant.client.gui.controllers;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.restaurant.client.gui.helper.ChartHelper;
import org.restaurant.client.gui.helper.DateHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.CoordXY;
import org.restaurant.common.model.beans.CoordXYYNumberComparator;
import org.restaurant.common.model.beans.Month;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXRadioButton;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class StatisticsController {
	
    @FXML
    private HBox searchFieldsHBox;

    @FXML
    private JFXComboBox<Integer> yearsCB;

    @FXML
    private JFXComboBox<String> monthsCB;

    @FXML
    private JFXRadioButton incomeId;
    
    @FXML
    private JFXRadioButton prodOrdersRB;

    @FXML
    private ToggleGroup statisticsGroup;

    @FXML
    private FlowPane diagramFP;
    
    @FXML
    private BarChart<String, Number> statisticsBarChart;

	/**
	 * Beállításra kerülnek a kereső mezők alapértelemezett értékei, az isLoadedIncomes mezőt hamisra állítja, miután betöltődött a bevételhez
	 * szükséges adat igazra állítódik és innentől kezdve meghívható a statisztika.
	 * Statisztikák váltásának eseményeinek kezelését beállítja.
	 */
	@FXML
	public void initialize() {
		initSearchFields();
		statisticsGroupSetChangeListener();
	}

	/**
	 * Csoportnak beállítja a statisztikák közti váltásának eseményeit, mire melyik statisztika jelenjen meg.
	 * Minden keresési mezőt láthatóvá tesz.
	 */
	private void statisticsGroupSetChangeListener() {
		statisticsGroup.selectedToggleProperty().addListener((ChangeListener<Toggle>) (ov, oldToggle, newToggle) -> {
			setVisibleAllSearchFields();
			sendStatisticsGroupAction();
		});
	}

	private void sendStatisticsGroupAction() {
		if (statisticsGroup.getSelectedToggle() != null) {
			if (incomeId.isSelected()) {
				ClientMain.getClient().populateProductIncomes(yearsCB.getValue());
			} else if (prodOrdersRB.isSelected()) {
				ClientMain.getClient().populateProductCategoryStatistics(DateHelper.getDateBySearchFields(yearsCB, monthsCB));
			}
		}
	}

	/**
	 * CoordXY listát kiegészíti azon hónapokkal (ezen objektumokhoz 0 értéket párosít), melyeket nem tartalmaz a lista.
	 * @param incomes CoordXY objektum lista mely tartalmazza az oszlopdiagram oszlopainak kirajzolásához szükséges adatokat.
	 * @return CoordXY listával tér vissza.
	 */
	private List<CoordXY> complementsIncomeMonths(List<CoordXY> incomes) {
		for (Month month : Month.values()) {
			boolean isInIncomes = false;
			for(CoordXY coordXY : incomes) {
				if(Integer.parseInt(coordXY.getyText()) == month.getNo()) {
					isInIncomes = true;
					break;
				}
			}
			if(!isInIncomes) {
				CoordXY newCoordXY = new CoordXY();
				newCoordXY.setValue(0.0);
				newCoordXY.setxText(String.valueOf(yearsCB.getValue()));
				newCoordXY.setyText(String.valueOf(month.getNo()));
				incomes.add(newCoordXY);
			}
		}
		CoordXYYNumberComparator coordXYComparator = new CoordXYYNumberComparator();
		Collections.sort(incomes, coordXYComparator);
		
		for(CoordXY coordXY : incomes) {
			coordXY.setyText(Month.getByNo(Integer.parseInt(coordXY.getyText())).getName());
		}
		return incomes;
	}

	private void setVisibleAllSearchFields() {
		for(Node searchField : searchFieldsHBox.getChildren()) {
			searchField.setVisible(true);
			searchField.setManaged(true);
		}
	}

	/**
	 * Feltölti az év és a hónap kereső mező kiválasztható értékeit és kiválasztja az aktuális évet és hónapot.
	 */
	private void initSearchFields() {
		for (Month month : Month.values()) {
			monthsCB.getItems().add(month.ordinal(), month.getName());
		}

		Integer fromYear = ClientMain.getClientProperty().getFromYear();
		Integer actYear = Calendar.getInstance().get(Calendar.YEAR);
		if (fromYear != null && fromYear < actYear) {
			while (fromYear <= actYear) {
				yearsCB.getItems().add(actYear--);
			}
		} else {
			yearsCB.getItems().add(actYear);
		}

		monthsCB.getSelectionModel().select(Month.getByNo(Calendar.getInstance().get(Calendar.MONTH) + 1).ordinal());
		yearsCB.getSelectionModel().selectFirst();
	}
    
	/**
	 * Kördiagramok kirajzolásáért felelős a megadott koordináta lista alapján. Minden egyes lista elem egy kördiagramnak felel meg.
	 * @param coordsXY CoordXY lista mely alapján kirajzolódnak a kördiagramok. 
	 */
	public void loadPieCharts(List<CoordXY> coordsXY) {
			Platform.runLater(() -> {
				diagramFP.getChildren().clear();
				for (PieChart chart : ChartHelper.getPieCharts(coordsXY).values()) {
					chart.setPrefWidth(diagramFP.getPrefWidth());
					diagramFP.getChildren().add(chart);
				}
			});
	}
	
	public void loadIncomesBarChart(List<CoordXY> coordsXY) {
		Platform.runLater(() -> loadBarChartDiagram("Year", "Cash flow ($)", yearsCB.getValue() + " - Cash flow ($)", complementsIncomeMonths(coordsXY)));
	}
    
	private void loadBarChartDiagram(String xAxisLabel, String yAxisLabel, String seriesName, List<CoordXY> coordsXY) {
			monthsCB.setVisible(false);
			monthsCB.setManaged(false);
			
			final CategoryAxis xAxis = new CategoryAxis();
			final NumberAxis yAxis = new NumberAxis();
			statisticsBarChart = new BarChart<>(xAxis, yAxis);
			
			statisticsBarChart.getXAxis().setLabel(xAxisLabel);
			statisticsBarChart.getYAxis().setLabel(yAxisLabel);

			XYChart.Series<String, Number> series = new Series<>();
			series.setName(seriesName);

			for(CoordXY coordXY : coordsXY) {
				
				//String monthName = Month.getByNo(Integer.parseInt(coordXY.getyText())).getName();
				Integer intValue = coordXY.getValue().intValue();
				XYChart.Data<String, Number> data = new Data<>(coordXY.getyText(), intValue);
				data.nodeProperty().addListener((ChangeListener<Node>) (observable, oldValue, newValue) -> {
					if (newValue != null) {
						addLabelToBar(data);
					}
				});
				series.getData().add(data);
			}
			
			statisticsBarChart.getData().add(series);
			if (diagramFP.getChildren() != null) {
				diagramFP.getChildren().clear();
			}
			statisticsBarChart.setMinWidth(700);
			diagramFP.getChildren().add(statisticsBarChart);
	}
	
	/**
	 * Az osszlopdiagram adott oszlop tetejére elhelyez egy szövegmezőt az y koordináta értéke alapján.
	 * @param data Diagram oszlopának x és y koordináta adatok, ebből az y koordináta értékét írja az oszlopdiagram oszlopának tetejére.
	 */
	private void addLabelToBar(XYChart.Data<String, Number> data) {
		Node node = data.getNode();
		Text dataText = new Text(data.getYValue() != null ? data.getYValue().toString() : "");
		dataText.setStyle("-fx-font: 12 arial;");
		node.parentProperty().addListener((ChangeListener<Parent>) (ov, oldParent, parent) -> {
			Group parentGroup = (Group) parent;
			parentGroup.getChildren().add(dataText);
		});

		node.boundsInParentProperty().addListener((ChangeListener<Bounds>) (ov, oldBounds, bounds) -> {
			dataText.setLayoutX(Math.round(bounds.getMinX() + bounds.getWidth() / 2 - dataText.prefWidth(-1) / 2));
			dataText.setLayoutY(Math.round(bounds.getMinY() - dataText.prefHeight(-1) * 0.5));
		});
	}
	

    @FXML
    void searchStatistics(ActionEvent event) {
    	sendStatisticsGroupAction();
    }
	
//	@FXML
//    void print(ActionEvent event) {
//        PrinterJob job2 = PrinterJob.createPrinterJob();
//        if(job2 != null) {
//        	PageLayout paisagem = Printer.getDefaultPrinter().createPageLayout(Paper.A4, PageOrientation.PORTRAIT, Printer.MarginType.DEFAULT);	
//        	paisagem.getPrintableWidth();
//        	statisticsBarChart.setMaxWidth(paisagem.getPrintableWidth());
//        	job2.showPrintDialog(diagramFP.getScene().getWindow());
//        	//diagramFP.setPrefSize(400, 220);
//        	diagramFP.setMaxWidth(400);
//        	diagramFP.setMaxHeight(220);
//	        job2.printPage(diagramFP);
//	        job2.printPage(diagramFP);
//	        job2.endJob();
//        }
//    }
}
