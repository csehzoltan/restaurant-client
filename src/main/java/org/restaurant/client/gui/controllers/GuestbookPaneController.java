package org.restaurant.client.gui.controllers;

import java.io.IOException;
import java.util.List;

import org.restaurant.client.gui.helper.DateHelper;
import org.restaurant.client.main.ClientMain;
import org.restaurant.common.model.beans.Guestbook;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class GuestbookPaneController {

    @FXML
    private JFXTextField noFld;

    @FXML
    private VBox guestBookVB;
    
    @FXML
    private JFXComboBox<Integer> yearsCB;

    @FXML
    private JFXComboBox<String> monthsCB;
    
	/**
	 * Feltölti az év és a hónap kereső mezőket.
	 */
	@FXML
	public void initialize() {
		DateHelper.fillDateCheckBoxes(yearsCB, ClientMain.getClientProperty().getFromYear());
		DateHelper.fillMonthCheckBox(monthsCB);
		ClientMain.getClient().populateGuestBookAdministrator(DateHelper.getDateBySearchFields(yearsCB, monthsCB));
	}
    
    /**
     * A vendégkönyv alapra betölti a vendégkönyveket.
     * @param guestbookItems Vendégkönyvek listája.
     */
	public void loadGuestbookItems(List<Guestbook> guestbooks) {
		Platform.runLater(() -> {
			try {
				guestBookVB.getChildren().clear();
				if(guestbooks != null && !guestbooks.isEmpty()) {
					for (Guestbook guestbook : guestbooks) {
						FXMLLoader loader = new FXMLLoader(
								getClass().getResource("/org/restaurant/client/gui/temp/administrator/GuestBookItem.fxml"));
						loader.setControllerFactory(c -> new GuestbookItemController(guestbook));
						Pane guestbookItem = loader.load();

						guestBookVB.getChildren().add(guestbookItem);
					}
				} else {
					Label noResult = new Label("No result found!");
					guestBookVB.getChildren().add(noResult);
				}
				
				noFld.setText(guestbooks != null ? "No.: " + String.valueOf(guestbooks.size()) : null);
				noFld.setEditable(false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
    @FXML
    void search(ActionEvent event) {
    	ClientMain.getClient().populateGuestBookAdministrator(DateHelper.getDateBySearchFields(yearsCB, monthsCB));
    }
}
