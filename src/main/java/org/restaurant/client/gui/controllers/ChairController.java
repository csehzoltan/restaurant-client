package org.restaurant.client.gui.controllers;

import org.restaurant.common.helpers.NumberHelper;
import org.restaurant.common.model.beans.Chair;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class ChairController {

	@FXML
	private Label nameLabel;

	@FXML
	private Label priceLabel;

	private Chair chair;
	private Double price;

	public ChairController(Chair chair, Double price) {
		this.chair = chair;
		this.price = price;
	}

	@FXML
    public void initialize() {
    	nameLabel.setText(chair.getName());
    	priceLabel.setText(price != null ? NumberHelper.scaleDouble(price, 2) + " $" : " - ");
    }
	
	public Chair getChair() {
		return chair;
	}

	public void setChair(Chair chair) {
		this.chair = chair;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Label getNameLabel() {
		return nameLabel;
	}

	public void setNameLabel(Label nameLabel) {
		this.nameLabel = nameLabel;
	}

	public Label getPriceLabel() {
		return priceLabel;
	}

	public void setPriceLabel(Label priceLabel) {
		this.priceLabel = priceLabel;
	}

}
