package org.restaurant.client.gui.controllers;

import org.restaurant.common.model.beans.ProductPrice;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class PorductPriceInfoBoxController {

    @FXML
    private HBox priceInfoHBox;

    @FXML
    private Label priceLabel;

    @FXML
    private JFXButton deletePriceBtn;

    @FXML
    private FontAwesomeIconView deleteIngredient;
    
    private ProductTablePaneController parentController;

	private ProductPrice productPrice;
    
    public PorductPriceInfoBoxController(ProductPrice productPrice) {
    	this.productPrice = productPrice;
    }
    
	/**
	 * Termék árához megjelenítendő szöveg beállítása és egyedi css osztály hozzáadása az objektumhoz
	 * és egyedi id.
	 */
	public void initialize() {
		priceLabel.setText(productPrice.getDisplayText());
		priceInfoHBox.setId("productPrice_" + productPrice.getId());
		priceInfoHBox.getStyleClass().add("productPriceClass");
		if(productPrice.isDeleted()) {
			priceInfoHBox.setVisible(false);
			priceInfoHBox.setManaged(false);
		}
    }

    @FXML
    void deletePrice(ActionEvent event) {
    	if(parentController != null) {
    		parentController.deleteProductPrice(productPrice);
    	}
    }

	public void setParentController(ProductTablePaneController parentController) {
		this.parentController = parentController;
	}

}

