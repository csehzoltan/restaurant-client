package org.restaurant.client.gui.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.restaurant.common.model.beans.Ingredient;

import com.jfoenix.controls.JFXButton;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;

public class IngredientItemController {
	
	@FXML
	private VBox ingredientItem;

	@FXML
    private JFXButton imgBtn;

	@FXML
	private Label ingredientNameLbl;

	@FXML
	private Label ingredientAmountLbl;
	
	private Ingredient ingredient;
	
	private IngredientSurfaceController parentController;
	
    public IngredientItemController(Ingredient ingredient) {
    	this.ingredient = ingredient;
    }
    
    public void initialize() {
		initIngredientValues();
	}
    
    /**
     * A beállított összetevő alapján újratölti a felületen megjelenített értékeket.
     */
    public void redraw() {
    	initIngredientValues();
    }
    
	private void initIngredientValues() {
		try {
			ImageView ingredientIV = new ImageView(byteArrayToImg(ingredient.getIngredientImg(), 50, 50));
			ingredientIV.setFitHeight(50);
			ingredientIV.setFitWidth(50);
			imgBtn.setGraphic(ingredientIV);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ingredientNameLbl.setText(ingredient.getName());
		ingredientAmountLbl.setText(ingredient.getQuantity().toString() + (ingredient.getUnit() != null ? " " + ingredient.getUnit().getValue() : ""));
	}
    
    @FXML
    void selectIngredientAction(ActionEvent event) {
    	if(getParentController() != null) {
    		getParentController().getIdFld().setText(ingredient.getId() != null ? ingredient.getId().toString() : null);
    		getParentController().getNameFld().setText(ingredient.getName());
    		getParentController().getQuantityFld().setText(ingredient.getQuantity() != null ? ingredient.getQuantity().toString() : null);
    		getParentController().getAlertLimitFld().setText(ingredient.getAlertLimit() != null ? ingredient.getAlertLimit().toString() : null);
    		getParentController().getUnitCB().setValue(ingredient.getUnit() != null ? ingredient.getUnit().getValue() : null);
    		try {
    			if(ingredient.getIngredientImg() != null) {
    				getParentController().getIngredientImageView().setImage(byteArrayToImg(ingredient.getIngredientImg(), 50, 50));
    			} else {
    				getParentController().getIngredientImageView().setImage(null);
    			}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    //TODO create common handler
	private static Image byteArrayToImg(byte[] raw, final int width, final int height) throws IOException {
		WritableImage image = new WritableImage(width, height);
		ByteArrayInputStream bis = new ByteArrayInputStream(raw);
		BufferedImage read = ImageIO.read(bis);
		image = SwingFXUtils.toFXImage(read, null);
		return image;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}
	
	public IngredientSurfaceController getParentController() {
		return parentController;
	}

	public void setParentController(IngredientSurfaceController parentController) {
		this.parentController = parentController;
	}
	
    public VBox getIngredientItem() {
		return ingredientItem;
	}
}
