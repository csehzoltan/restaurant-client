package org.restaurant.client.gui.controllers;

import org.apache.commons.lang.SerializationUtils;
import org.restaurant.client.gui.helper.AlertHelper;
import org.restaurant.client.gui.validators.FieldValidator;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.Size;
import org.restaurant.common.model.beans.Unit;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class ProductPriceDialogController {
	
    @FXML
    private VBox inputFieldsVBox;

    @FXML
    private TableView<ProductPrice> priceTable;

    @FXML
    private TableColumn<ProductPrice, Integer> colId;

    @FXML
    private TableColumn<ProductPrice, Double> colAmmount;

    @FXML
    private TableColumn<ProductPrice, Unit> colUnit;

    @FXML
    private TableColumn<ProductPrice, Double> colPrice;

    @FXML
    private TableColumn<ProductPrice, Size> colSize;
    
    @FXML
    private TableColumn<ProductPrice, Boolean> colDeleted;

    @FXML
    private JFXTextField idFld;

    @FXML
    private JFXTextField amountFld;

    @FXML
    private JFXComboBox<String> unitCB;

    @FXML
    private JFXTextField priceFld;

    @FXML
    private JFXComboBox<String> sizeCB;

    @FXML
    private JFXButton deletePriceBtn;

    @FXML
    private JFXButton addBtn;
    
    @FXML
    private JFXCheckBox deletedCB;
    
    private Integer editIndex;
	
	private FieldValidator validator;
    
	Product product;
	
    private ProductTablePaneController parentController;

	/**
	 * @param product termék melynek az árait módosítani kívánjuk.
	 */
	public ProductPriceDialogController(Product product) {
		this.product = product;
	}
    
	/**
	 * Ár azonosító mezőt letiltja.
	 * Rendeleri az árakat tartalmazó táblát.
	 * A méret és a mértékegység legördülőmenüt feltölti a megfelelő értékekkel.
	 */
	@FXML
	public void initialize() {
		validator = new FieldValidator();
		initPriceTable();
		initFields();
	}
	
	private void initFields() {
		idFld.setDisable(true);
		
		ObservableList<String> units = FXCollections.observableArrayList();
		for (Unit unit : Unit.values()) {
			units.add(unit.getValue());
		}
		unitCB.getItems().addAll(units);
		
		ObservableList<String> sizes = FXCollections.observableArrayList();
		for(Size size : Size.values()) {
			sizes.add(size.getDisplay());
		}
		sizeCB.getItems().addAll(sizes);
	}
	
	/**
	 * Rendeleri az árakat tartalmazó táblát.
	 */
	private void initPriceTable() {
		priceTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		idFld.setDisable(true);
		colId.setCellValueFactory(new PropertyValueFactory<ProductPrice, Integer>("id"));
		colPrice.setCellValueFactory(new PropertyValueFactory<ProductPrice, Double>("price"));
		colAmmount.setCellValueFactory(new PropertyValueFactory<ProductPrice, Double>("amount"));
		colUnit.setCellValueFactory(new PropertyValueFactory<ProductPrice, Unit>("unit"));
		colSize.setCellValueFactory(new PropertyValueFactory<ProductPrice, Size>("size"));
		colDeleted.setCellValueFactory(new PropertyValueFactory<ProductPrice, Boolean>("deleted"));
		TableColumn<ProductPrice, Boolean> colEdit = new TableColumn<>("Act.");
		colEdit.setMinWidth(55.0);
		colEdit.setPrefWidth(55.0);
		colEdit.setMaxWidth(55.0);
		priceTable.getColumns().add(colEdit);
		colEdit.setCellValueFactory(p -> new SimpleBooleanProperty(p.getValue() != null));
		colEdit.setCellFactory(p -> new ButtonCell());
		
		ObservableList<ProductPrice> productTableItems = FXCollections.observableArrayList();
		if(product.getPrices() != null) {
			for (ProductPrice productPrice : product.getPrices()) {
				productTableItems.add(productPrice);
			}

			if (productTableItems != null) {
				priceTable.setItems(productTableItems);
			}
		}
		
	}
	
	private class ButtonCell extends TableCell<ProductPrice, Boolean> {
		Button cellButton = new Button("Edit");

		/**
		 * Button text set null because it add dots if not set it.
		 */
		ButtonCell() {
			Image imgEdit = new Image(getClass().getResource("/gui.icons/edit.png").toExternalForm());
			ImageView iv = new ImageView();
			iv.setFitWidth(20);
			iv.setFitHeight(20);
			iv.setImage(imgEdit);
			iv.setPreserveRatio(true);
			iv.setSmooth(true);
			iv.setCache(true);
			cellButton.setText(null);
			cellButton.setStyle("-fx-background-color: transparent;");
			cellButton.setGraphic(iv);
			setAlignment(Pos.CENTER);
			cellButton.setOnAction(t -> {
				editIndex = ButtonCell.this.getIndex();
				priceTable.getSelectionModel().select(editIndex);
				ProductPrice productPrice = ButtonCell.this.getTableView().getItems().get(editIndex);
				
				productPrice = (ProductPrice) SerializationUtils.clone(productPrice);
				if(productPrice.getId() != null) {
					idFld.setText(productPrice.getId().toString());
				}
				priceFld.setText(String.valueOf(productPrice.getPrice()));
				amountFld.setText(productPrice.getAmount() != null ? String.valueOf(productPrice.getAmount()) : null);
				String unit = productPrice.getUnit() != null ? productPrice.getUnit().getValue() : null;
				unitCB.getSelectionModel().select(unit);
				String size = productPrice.getSize() != null ? productPrice.getSize().getDisplay() : null;
				sizeCB.getSelectionModel().select(size);
				deletedCB.setSelected(productPrice.isDeleted());
				
//				drawIngredients(temporaryProductPrice.getIngredients());
//				drawProductPrices(temporaryProductPrice.getPrices());
			});
		}

		@Override
		protected void updateItem(Boolean t, boolean empty) {
			super.updateItem(t, empty);
			if (!empty) {
				setGraphic(cellButton);
			}
		}
	}

	/**
	 * Validálja a bemeneti mezőket, majd a megadott árnak módosítja az értékeit a beviteli mezők alapján.
	 * Végül frissíti a tábla adatait a módosításnak megfelelően.
	 * @param event
	 */
	@FXML
	void addPrice(ActionEvent event) {
		boolean isValid = false;
		if (editIndex != null && priceTable != null) {
			ProductPrice selectedProductPrice = priceTable.getItems().get(editIndex);
			selectedProductPrice.setId(
					(idFld.getText() != null && !idFld.getText().isEmpty()) ? Integer.parseInt(idFld.getText()) : null);
			isValid = setProductPriceValues(selectedProductPrice);
		} else {
			ProductPrice newProductPrice = new ProductPrice();
			isValid = setProductPriceValues(newProductPrice);
			if(isValid) {
				priceTable.getItems().add(newProductPrice);
			}
			
		}

		if (isValid) {
			getParentController().drawProductPrices(priceTable.getItems());
			priceTable.refresh();
			product.setPrices(priceTable.getItems());
		}
	}

	/**
	 * Validálja a bemeneti mezőket, majd a megadott árnak módosítja az értékeit a beviteli mezők alapján.
	 * @param selectedProductPrice termék ára.
	 * @return igazzal tér vissza, amennyiben átment a validáláson.
	 */
	private boolean setProductPriceValues(ProductPrice selectedProductPrice) {
		boolean isValid = false;
		if(validator.validateTf(priceFld, FieldValidator.DECIMALREGEXP, true, 1, 15, FieldValidator.DECIMALRULE) && 
				validator.validateTf(amountFld, FieldValidator.DECIMALREGEXP, false, 1, 15, FieldValidator.DECIMALRULE)) {
			
			boolean isValidFilledAmount = validateAmountFields();

			if (!isValidFilledAmount) {
				AlertHelper.callAlert(AlertType.ERROR,
						"Required filds: price with amount and unit or price with size!");
			} else {
				setSelectedProductPrice(selectedProductPrice);
			}
			if (isValidFilledAmount) {
				isValid = true;
			}

		}
		
		return isValid;
	}

	private void setSelectedProductPrice(ProductPrice selectedProductPrice) {
		selectedProductPrice.setPrice((priceFld.getText() != null && !priceFld.getText().isEmpty())
				? Double.parseDouble(priceFld.getText())
				: 0.0);
		selectedProductPrice.setAmount((amountFld.getText() != null && !amountFld.getText().isEmpty())
				? Double.parseDouble(amountFld.getText())
				: null);
		selectedProductPrice.setUnit(
				unitCB.getValue() != null && !unitCB.getValue().isEmpty() ? Unit.getByValue(unitCB.getValue())
						: null);
		selectedProductPrice.setSize(
				sizeCB.getValue() != null && !sizeCB.getValue().isEmpty() ? Size.getByValue(sizeCB.getValue())
						: null);
		selectedProductPrice.setDeleted(deletedCB.isSelected());
	}

	private boolean validateAmountFields() {
		return ((amountFld.getText() != null && unitCB.getValue() != null && sizeCB.getValue() == null)
				|| (amountFld.getText() == null || amountFld.getText().isEmpty()) && unitCB.getValue() == null && sizeCB.getValue() != null);
	}

    @FXML
    void erasePriceData(ActionEvent event) {
    	idFld.clear();
    	priceFld.clear();
    	amountFld.clear();
    	unitCB.setValue(null);
    	sizeCB.setValue(null);
    	deletedCB.setSelected(false);
    	
    	editIndex = null;
    }
    
	public ProductTablePaneController getParentController() {
		return parentController;
	}

	public void setParentController(ProductTablePaneController parentController) {
		this.parentController = parentController;
	}

}
