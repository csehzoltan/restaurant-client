package org.restaurant.client.gui.logics;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;

/**
 * Contains the gui of the menu item side.
 * @author Zolt�n
 *
 */
public class MenuItems {
	
	private MenuItems(){
		// Exists only to defeat instantiation.
	}
	
	protected static FlowPane addMenus() {
		FlowPane flow = new FlowPane();
	    flow.setPadding(new Insets(5, 0, 5, 0));
	    flow.setVgap(4);
	    flow.setHgap(4);
	    flow.setPrefWrapLength(170); // preferred width allows for two columns
	    flow.setStyle("-fx-background-color: DAE6F3;");

		Button rendButton = new Button("Rending");
		Button statisticsButton = new Button("Statistics");
		Button rendButton2 = new Button("Rending2");
		Button statisticsButton2 = new Button("Statistics2");
		
		flow.getChildren().addAll(rendButton, rendButton2, statisticsButton, statisticsButton2);

		return flow;
	}
}
