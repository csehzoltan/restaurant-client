package org.restaurant.client.gui.logics;

import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Contains the gui of the header side.
 * @author Zolt�n
 *
 */
public class Header {
	private Header(){
		// Exists only to defeat instantiation.
	}
	
	/**
	 * Contains all of the header elements.
	 * @return HBox header elements.
	 */
	protected static HBox addHeader() {
		HBox header = new HBox();
		
		DropShadow shadow = new DropShadow();
		shadow.setOffsetX(5);
		shadow.setOffsetY(5);
		
		Text restName = new Text("Restaurant");
		restName.setFont(Font.font("Courier New", FontWeight.BOLD, 28));
		restName.setEffect(shadow);
		
		header.getChildren().add(restName);
		return header;
	}
}
