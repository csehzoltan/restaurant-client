package org.restaurant.client.gui.logics;

import java.io.IOException;

import org.restaurant.common.model.beans.User;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Contains the gui of the loggin side.
 *
 */
public class Loggin {
	
	private Label message = new Label("");
	private BorderPane bp;
	public User user = new User();
	private Hyperlink registrationLink = new Hyperlink("Registration");
	/**
	 * Contains all of the login elements.
	 * @return GridPane login elements.
	 */
	protected GridPane addLoggin(BorderPane bp) {
		this.bp = bp;
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		
		final Text loginTitle = new Text("Logidfdfn");
		loginTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
		grid.add(loginTitle, 0, 0, 2, 1);

		Label userName = new Label("User Name:");
		grid.add(userName, 0, 1);

		TextField userField = new TextField();
		grid.add(userField, 1, 1);

		Label pw = new Label("Password:");
		grid.add(pw, 0, 2);

		PasswordField pwField = new PasswordField();
		grid.add(pwField, 1, 2);
		
		grid.add(registrationLink, 0, 3);
		grid.add(message, 0, 4);
		
		
		pwField.setOnAction(e -> loginAction(userField, pwField));
		registrationLink.setOnAction(e -> {
			try {
				registrationAction(bp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		return grid;
	}

	private void loginAction(TextField userField, PasswordField pwField) {
		/*UserDao userDAO = new UserDao(RestaurantFactory.getSqlSessionFactory());
		user = userDAO.selectByName(userField.getText());
		if(user!=null && userField.getText().equals(user.getName()) && pwField.getText().equals(user.getPassword())){
			message.setText("Your password has been confirmed!");
			message.setTextFill(Color.rgb(21, 117, 84));
		} else {
			message.setText("Your password is incorrect!");
			message.setTextFill(Color.rgb(210, 39, 30));
		}*/
		
	}

	private static void registrationAction(BorderPane bp) throws IOException {
		/*Pane registration = (Pane) FXMLLoader.load(Loggin.class.getResource("/restaurant/gui/temp/common/Registration.fxml"));
		bp.setCenter(registration);
		bp.setAlignment(registration, Pos.BOTTOM_CENTER);*/
		Pane root = FXMLLoader.load(Loggin.class.getResource("/restaurant/gui/temp/common/Login.fxml"));
		Stage dialog = new Stage();
		dialog.initModality(Modality.APPLICATION_MODAL);
		//dialog.initOwner(parentStage);
		dialog.setTitle("Registration");
		dialog.setScene(new Scene(root));
		dialog.showAndWait();
		
		//FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(name));
		//https://www.youtube.com/watch?v=ZzwvQ6pa_tk
	}
	
}
